﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace HttpParser
{
    [TestClass]
    public class SberbankParserTests
    {
        #region Constants and fields

        private const string InitialUrl = "https://www.avito.ru/sberbank/rossiya";

        private readonly Random random;

        private readonly Regex YearRegex;

        private HttpClient Client;

        private List<JObject> ParsedObjects;

        private JArray ParsedData;
        #endregion

        #region Constructors

        public SberbankParserTests()
        {
            Client = new HttpClient();
            ParsedObjects = new List<JObject>(200);
            random = new Random();
            YearRegex = new Regex("(?:(?:[1]{1}\\d{1}\\d{1}\\d{1})|(?:[2]{1}\\d{3}))", RegexOptions.IgnoreCase | RegexOptions.Singleline);
        }
        #endregion

        #region Test methods

        [TestMethod]
        public void IsSberbankSiteParsedSuccessfully()
        {
            int pagesCount = GetPagesCount();

            for (int index = 1; index <= pagesCount; ++index)
            {
                ProcessSitePage(index);

                Thread.Sleep(random.Next(2000, 3000));
            }

            UpdateSectionInfo(ParsedObjects);

            SaveDataAfterParsing();
        }

        [TestMethod]
        public void IsParsedDataUpdatesSuccessfully()
        {
            ParsedData = GetParsedData("avito_sberbank_data.json");
            
            for (int index = 0; index < ParsedData.Count; ++index)
            {
                UpdateParsedData(index);
            }

            SaveUpdatedData(ParsedData, "avito_sberbank_data_updated.json");
        }

        [TestMethod]
        public async Task IsParsedDataDescriptionUpdatesSuccessfully()
        {
            const string FileName = "avito_sberbank_data_updated.json";

            Random rnd = new Random();
            HttpClient client = new HttpClient();
            
            JArray dataArray = GetParsedData(FileName);
            
            for (int index = 0; index < dataArray.Count; ++index)
            {
                int waitTime = rnd.Next(5000, 10000);
                await Task.Delay(waitTime);

                JObject dataObject = dataArray[index] as JObject;
                string url = (string)dataObject["URL"];

                HttpResponseMessage response = await client.GetAsync(url);
                HttpContent content = response.Content;

                Stream contentStream = await content.ReadAsStreamAsync();
                Encoding contentEncoding = Encoding.GetEncoding(content.Headers.ContentType.CharSet ?? "utf-8");

                HtmlDocument document = new HtmlDocument();
                document.Load(contentStream, contentEncoding);

                HtmlNode descriptionNode = document.DocumentNode.SelectSingleNode("//div[@class='item-description-html']");
                if (descriptionNode != null)
                {
                    string descriptionText = descriptionNode.InnerText.Trim();
                    dataObject["Description"] = descriptionText;
                }
            }

            SaveUpdatedData(dataArray, FileName);
        }
        
        [TestMethod]
        public void UpdateParsedDataSections()
        {
            JArray parsedData = GetParsedData("avito_sberbank_data_updated.json");

            string[] realties = new[] { "дом", "дач", "коттедж", "коммерч", "недвижим", "гараж", "квартир" };
            string[] autos = new[] { "грузовик", "спецтехника", "автомобил" };
            string equipment = "оборудование";
            string business = "бизнес";
            
            for (int index = 0; index < parsedData.Count; ++index)
            {
                JObject current = parsedData[index] as JObject;
                
                string section = null;
                string category = (string)current["Category"];

                if (realties.Any(s => category.IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    section = "Недвижимость";
                }
                else if (autos.Any(s => category.IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    section = "Транспорт";
                }
                else if (category.IndexOf(equipment) >= 0)
                {
                    section = "Оборудование";
                }
                else if (category.IndexOf(business) >= 0)
                {
                    section = "Бизнес";
                }
                else section = "Неизвестный раздел";

                current["Section"] = section;
            }

            SaveUpdatedData(parsedData, "avito_sberbank_data_updated.json");
        }
        #endregion

        #region Private class methods

        private int GetPagesCount()
        {
            Task<int> pagesTask = Client.GetAsync(InitialUrl)
                .ContinueWith(GetDocumentFromResponse)
                .ContinueWith(GetPagesCountFromDocument);

            pagesTask.Wait();

            return pagesTask.Result;
        }

        private int GetPagesCountFromDocument(Task<HtmlDocument> documentTask)
        {
            int pagesCount = 0;

            HtmlDocument document = documentTask.Result;

            HtmlNode paginationNode = document.DocumentNode.SelectSingleNode("//div[@class='pagination-pages clearfix']");
            if (paginationNode != null)
            {
                HtmlNodeCollection childNodes = paginationNode.ChildNodes;
                for (int index = 0; index < childNodes.Count; ++index)
                {
                    string nodeName = childNodes[index].Name;
                    if (string.Equals("span", nodeName) || string.Equals("a", nodeName))
                    {
                        pagesCount++;
                    }
                }
            }

            return pagesCount;
        }

        private void UpdateParsedData(int itemIndex)
        {
            JObject dataObject = ParsedData[itemIndex] as JObject;

            if (dataObject["Price"] == null || dataObject["Images"] == null)
            {
                string itemUrl = (string)dataObject["URL"];

                Task updateTask = Client.GetAsync(itemUrl)
                    .ContinueWith(GetDocumentFromResponse)
                    .ContinueWith(GetDataFromItemDocument, itemIndex);

                updateTask.Wait();
            }
        }

        private void ProcessSitePage(int index)
        {
            string siteUrl = string.Concat(InitialUrl, "?p=", index.ToString());

            Task parsingTask = Client.GetAsync(siteUrl)
                .ContinueWith(GetDocumentFromResponse)
                .ContinueWith(SelectItemNodes)
                .ContinueWith(ProcessItemNodes);

            parsingTask.Wait();
        }

        private HtmlDocument GetDocumentFromResponse(Task<HttpResponseMessage> responseTask)
        {
            HttpResponseMessage response = responseTask.Result;
            HttpContent content = response.Content;

            string charSet = content.Headers.ContentType.CharSet ?? "utf-8";
            Encoding responseEncoding = Encoding.GetEncoding(charSet);

            Stream responseStream = content.ReadAsStreamAsync().Result;

            HtmlDocument document = new HtmlDocument();
            document.Load(responseStream, responseEncoding);
            return document;
        }

        private HtmlNodeCollection SelectItemNodes(Task<HtmlDocument> documentTask)
        {
            const string itemNodesSelector = @"//div[@class='item item_table clearfix js-catalog-item-enum c-b-0']";

            HtmlDocument document = documentTask.Result;

            HtmlNodeCollection itemNodes = document.DocumentNode.SelectNodes(itemNodesSelector);

            return itemNodes;
        }

        private void ProcessItemNodes(Task<HtmlNodeCollection> nodesTask)
        {
            HtmlNodeCollection itemNodes = nodesTask.Result;

            int count = itemNodes.Count;
            
            for (int index = 0; index < count; ++index)
            {
                ProcessSingleNode(itemNodes[index]);

                Thread.Sleep(random.Next(2000, 3000));
            }
        }

        private void ProcessSingleNode(HtmlNode itemNode)
        {
            string itemID = itemNode.Attributes["id"].Value.Substring(1);

            HtmlNode descriptionNode = null;

            HtmlNodeCollection childNodes = itemNode.ChildNodes;
            for (int index = 0; index < childNodes.Count; ++index)
            {
                HtmlNode currentChild = childNodes[index];
                HtmlAttribute classAttribute = currentChild.Attributes["class"];
                if (classAttribute != null && string.Equals(classAttribute.Value, "description", StringComparison.OrdinalIgnoreCase))
                {
                    descriptionNode = currentChild; break;
                }
            }

            if (descriptionNode != null)
            {
                HtmlNode titleNode = descriptionNode.ChildNodes["h3"].ChildNodes["a"];

                JObject itemData = new JObject();

                string itemUrl = string.Concat("https://www.avito.ru", titleNode.Attributes["href"].Value);

                itemData["ID"] = itemID;
                itemData["Name"] = titleNode.InnerText.Trim();
                itemData["URL"] = itemUrl;

                var itemDocumentTask = Client.GetAsync(itemUrl).ContinueWith(GetDocumentFromResponse);
                itemDocumentTask.Wait();

                HtmlDocument itemDocument = itemDocumentTask.Result;

                if (itemData["Images"] == null)
                {
                    HtmlNodeCollection imageNodes = itemDocument.DocumentNode.SelectNodes("//div[@class='gallery-img-wrapper js-gallery-img-wrapper']");
                    if (imageNodes != null)
                    {
                        int count = imageNodes.Count;

                        string[] imageUrls = new string[count];
                        for (int index = 0; index < count; ++index)
                        {
                            HtmlAttribute urlAttribute = imageNodes[index].ChildNodes.FirstOrDefault(node => node.Name == "div")?.Attributes["data-url"];
                            if (urlAttribute != null)
                            {
                                imageUrls[index] = string.Concat("https:", urlAttribute.Value);
                            }
                        }
                        itemData["Images"] = new JArray(imageUrls);
                    }
                }

                if (itemData["Price"] == null)
                {
                    HtmlNode priceNode = itemDocument.DocumentNode.SelectSingleNode("//span[@class='price-value-string']");
                    if (priceNode != null)
                    {
                        string dataText = WebUtility.HtmlDecode(priceNode.InnerText).Trim();

                        StringBuilder priceBuilder = new StringBuilder(dataText.Length);
                        for (int index = 0; index < dataText.Length; ++index)
                        {
                            char current = dataText[index];
                            if (char.IsDigit(current) || (current == '.'))
                            {
                                priceBuilder.Append(current);
                            }
                        }
                        if (priceBuilder[priceBuilder.Length - 1] == '.')
                        {
                            priceBuilder.Append('0');
                        }

                        itemData["Price"] = double.Parse(priceBuilder.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                    }
                }

                if (itemData["Description"] == null)
                {
                    HtmlNode descrNode = itemDocument.DocumentNode.SelectSingleNode("//div[@class='item-description-html']");
                    if (descrNode != null)
                    {
                        string descriptionText = descriptionNode.InnerText.Trim();
                        itemData["Description"] = descriptionText;
                    }
                }

                HtmlNode addressNode = itemDocument.DocumentNode.SelectSingleNode("//div[@class='seller-info js-seller-info']");
                HtmlNode addressPropNode = addressNode.ChildNodes.LastOrDefault(node => string.Equals("seller-info-prop", node.Attributes["class"]?.Value));
                HtmlNode addressValueNode = addressPropNode.ChildNodes.LastOrDefault(node => string.Equals("seller-info-value", node.Attributes["class"]?.Value));

                string addressValue = addressValueNode.InnerText.Trim();
                int commaIndex = addressValue.IndexOf(',');
                string region = addressValue.Substring(0, commaIndex);
                itemData["Region"] = region;

                HtmlNode dataNode = descriptionNode.ChildNodes.FirstOrDefault(IsDataNode);

                itemData["Category"] = dataNode.ChildNodes[1].InnerText;
                itemData["City"] = dataNode.ChildNodes[3].InnerText;
                
                ParsedObjects.Add(itemData);
            }
        }

        private void UpdateSectionInfo(List<JObject> parsedData)
        {
            string[] realties = new[] { "дом", "дач", "коттедж", "коммерч", "недвижим", "гараж", "квартир" };
            string[] autos = new[] { "грузовик", "спецтехника", "автомобил" };
            string equipment = "оборудование";
            string business = "бизнес";

            for (int index = 0; index < parsedData.Count; ++index)
            {
                JObject current = parsedData[index] as JObject;

                string section = null;
                string category = (string)current["Category"];

                if (realties.Any(s => category.IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    section = "Недвижимость";
                }
                else if (autos.Any(s => category.IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    section = "Транспорт";
                }
                else if (category.IndexOf(equipment) >= 0)
                {
                    section = "Оборудование";
                }
                else if (category.IndexOf(business) >= 0)
                {
                    section = "Бизнес";
                }
                else section = "Неизвестный раздел";

                current["Section"] = section;

                if (string.Equals("Транспорт", section, StringComparison.OrdinalIgnoreCase))
                {
                    string description = (string)current["Description"];
                    if (!string.IsNullOrEmpty(description))
                    {
                        Match yearMatch = YearRegex.Match(description);
                        if (yearMatch.Success)
                        {
                            do
                            {
                                int year = int.Parse(yearMatch.Value);
                                if (year >= 1900)
                                {
                                    current["Year"] = year;
                                    break;
                                }

                                yearMatch = yearMatch.NextMatch();
                            }
                            while (yearMatch.Success);
                        }
                    }
                }
            }
        }

        private static bool IsAboutNode(HtmlNode node)
        {
            return (node != null) &&
                string.Equals(node.Name, "div", StringComparison.OrdinalIgnoreCase) &&
                string.Equals(node.Attributes["class"].Value, "about", StringComparison.OrdinalIgnoreCase);
        }

        private static bool IsDataNode(HtmlNode node)
        {
            return (node != null) &&
                string.Equals(node.Name, "div", StringComparison.OrdinalIgnoreCase) &&
                string.Equals(node.Attributes["class"].Value, "data", StringComparison.OrdinalIgnoreCase);
        }

        private static JsonSerializer CreateSerializer()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver(),
                DateFormatString = "dd.MM.yyyy",
                DefaultValueHandling = DefaultValueHandling.Include,
                Formatting = Formatting.Indented,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };

            JsonSerializer serializer = JsonSerializer.Create(settings);

            return serializer;
        }

        private void SaveDataAfterParsing()
        {
            JsonSerializer serializer = CreateSerializer();

            string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string filePath = Path.Combine(desktopDirectory, "avito_sberbank_data.json");

            var parsedObjects = ParsedObjects;

            using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.UTF8, 4096))
            {
                serializer.Serialize(writer, parsedObjects);
                writer.Flush();
            }
        }

        private JArray GetParsedData(string fileName)
        {
            JsonSerializer serializer = CreateSerializer();

            string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string filePath = Path.Combine(desktopDirectory, fileName);

            JArray result = null;

            using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8))
            {
                result = serializer.Deserialize(reader, typeof(JArray)) as JArray;
            }

            return result;
        }

        private void SaveUpdatedData(JArray dataArray, string fileName)
        {
            JsonSerializer serializer = CreateSerializer();

            string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string filePath = Path.Combine(desktopDirectory, fileName);

            using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.UTF8, 4096))
            {
                serializer.Serialize(writer, dataArray);
                writer.Flush();
            }
        }

        private void GetDataFromItemDocument(Task<HtmlDocument> documentTask, object stateObject)
        {
            int itemIndex = (int)stateObject;

            JObject itemObject = ParsedData[itemIndex] as JObject;

            HtmlDocument itemDocument = documentTask.Result;

            if (itemObject["Images"] == null)
            {
                HtmlNodeCollection imageNodes = itemDocument.DocumentNode.SelectNodes("//div[@class='gallery-img-wrapper js-gallery-img-wrapper']");
                if (imageNodes != null)
                {
                    int count = imageNodes.Count;

                    string[] imageUrls = new string[count];
                    for (int index = 0; index < count; ++index)
                    {
                        HtmlAttribute urlAttribute = imageNodes[index].ChildNodes.FirstOrDefault(node => node.Name == "div")?.Attributes["data-url"];
                        if (urlAttribute != null)
                        {
                            imageUrls[index] = string.Concat("https:", urlAttribute.Value);
                        }
                    }
                    itemObject["Images"] = new JArray(imageUrls);
                }
            }

            if (itemObject["Price"] == null)
            {
                HtmlNode priceNode = itemDocument.DocumentNode.SelectSingleNode("//span[@class='price-value-string']");
                if (priceNode != null)
                {
                    string dataText = WebUtility.HtmlDecode(priceNode.InnerText).Trim();

                    StringBuilder priceBuilder = new StringBuilder(dataText.Length);
                    for (int index = 0; index < dataText.Length; ++index)
                    {
                        char current = dataText[index];
                        if (char.IsDigit(current) || char.IsPunctuation(current))
                        {
                            priceBuilder.Append(current);
                        }
                    }

                    itemObject["Price"] = decimal.Parse(priceBuilder.ToString());
                }
            }
        }
        #endregion
    }
}