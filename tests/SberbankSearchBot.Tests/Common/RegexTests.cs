﻿using System;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SberbankSearchBot.Tests
{
    [TestClass]
    public class RegexTests
    {
        #region Test methods

        [TestMethod]
        public void IsDecimalRegexAppliesSuccessfully()
        {
            Regex decimalRegex = new Regex("\\d+", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            Match numberMatch = decimalRegex.Match("some text without decimals...");
        }
        #endregion
    }
}