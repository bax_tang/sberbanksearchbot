﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace SberbankSearchBot.Common.Tests
{
    [TestClass]
    public class JsonTests
    {
        #region Constants and fields

        private const string DataFilePath = @"F:\Source\Projects\SberbankSearchBot\src\SberbankSearchBot\App_Data\ObjectsData.json";
        #endregion

        #region Test methods

        [TestMethod]
        public void IsDecimalValueSuccessfullySerializesToJson()
        {
            RealtyInfo[] infoArray = new RealtyInfo[]
            {
                new RealtyInfo("App_Data\\first.png", 100500M, "дом", "дача"),
                new RealtyInfo("App_Data\\second.png", 90000000M, "коттедж")
            };

            string jsonInfo = JsonConvert.SerializeObject(infoArray, Formatting.Indented);

            Console.WriteLine(jsonInfo);
        }

        [TestMethod]
        public void IsTokenSelectionExecutesSuccessfully()
        {
            JArray dataArray = GetDataArray();

            string expression = "$..[?(@.Section=='Недвижимость')&&(@.Category=='Земельные участки'||@.Category=='Гаражи и машиноместа')]";

            IEnumerable<JToken> selectedTokens = dataArray.SelectTokens(expression, false);

            Assert.IsNotNull(selectedTokens);

            Console.WriteLine(string.Join("\r\n", selectedTokens));
        }
        #endregion

        #region Private class methods

        private JArray GetDataArray()
        {
            JArray dataArray = null;

            StreamReader reader = new StreamReader(DataFilePath, Encoding.UTF8);

            try
            {
                dataArray = JArray.Load(new JsonTextReader(reader));
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
                throw;
            }
            finally
            {
                reader.Dispose();
            }

            return dataArray;
        }
        #endregion

        #region Nested types

        private class RealtyInfo
        {
            public string ImagePath;
            public decimal Price;
            public string[] Keywords;

            public RealtyInfo(string imagePath, decimal price, params string[] keywords)
            {
                ImagePath = imagePath;
                Price = price;
                Keywords = keywords;
            }
        }
        #endregion
    }
}