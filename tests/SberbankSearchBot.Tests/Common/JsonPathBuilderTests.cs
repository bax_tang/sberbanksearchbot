﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot.Tests
{
    [TestClass]
    public class JsonPathBuilderTests
    {
        #region Constants and fields

        private const string ConfigFilePath = @"F:\Source\Projects\SberbankSearchBot\src\SberbankSearchBot\App_Data\SearchConfiguration.json";

        private const string DataFilePath = @"F:\Source\Projects\SberbankSearchBot\src\SberbankSearchBot\App_Data\ObjectsData.json";
        #endregion

        #region Test methods
        
        #endregion

        #region Private class methods

        private static SearchConfigurationRepository GetConfigurationRepository()
        {
            return new SearchConfigurationRepository(ConfigFilePath);
        }

        private static ObjectsDataRepository GetDataRepository()
        {
            ObjectsJsonDataSource dataSource = new ObjectsJsonDataSource(DataFilePath);

            return new ObjectsDataRepository(dataSource);
        }
        #endregion
    }
}