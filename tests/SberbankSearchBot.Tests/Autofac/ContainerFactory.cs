﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Autofac;

namespace SberbankSearchBot.Tests
{
    public static class ContainerFactory
    {
        #region Constants and fields

        private const string TestConnectionString = 
            "Data Source=DESKTOP-D4CDD5E;Initial Catalog=AvitoObjects;Integrated Security=True;Connect Timeout=30;" + 
            "Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        #endregion

        #region Public class methods

        public static IContainer CreateContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterInstance(BotConfiguration.Instance);

            builder.Register(GetDatabaseConnection).InstancePerDependency();

            builder.RegisterType<MessagesRepository>().InstancePerDependency();

            return builder.Build();
        }
        #endregion

        #region Private class methods

        internal static SqlConnection GetDatabaseConnection(IComponentContext context)
        {
            SqlConnection connection = new SqlConnection(TestConnectionString);
            connection.Open();
            return connection;
        }
        #endregion
    }
}