﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot.Tests
{
    [TestClass]
    public class SearchConfigurationRepositoryTests
    {
        #region Constants and fields

        private const string ConfigFilePath = @"F:\Source\Projects\SberbankSearchBot\src\SberbankSearchBot\App_Data\SearchConfiguration.json";
        #endregion

        #region Test methods
        
        [TestMethod]
        public void IsSearchConfigurationCreatesSuccessfully()
        {
            var repository = GetConfigurationRepository();

            string activityKey = Guid.NewGuid().ToString("B").ToUpperInvariant();
            string sectionName = "Недвижимость";

            SearchConfiguration configuration = repository.GetOrCreateConfiguration(activityKey, sectionName);

            Assert.IsNotNull(configuration);
        }
        #endregion

        #region Private class methods

        private static SearchConfigurationRepository GetConfigurationRepository()
        {
            return new SearchConfigurationRepository(ConfigFilePath);
        }
        #endregion
    }
}