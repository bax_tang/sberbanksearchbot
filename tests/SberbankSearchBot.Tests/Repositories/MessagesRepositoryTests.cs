﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Autofac;

namespace SberbankSearchBot.Tests
{
    [TestClass]
    public class MessagesRepositoryTests
    {
        #region Test methods

        [TestMethod]
        public void IsMessagesLoadsSuccessfully()
        {
            BotConfiguration configuration = TestContainer.Resolve<BotConfiguration>();

            foreach (Guid departmentID in new[]
            {
                Guid.Parse("55BF4517-D217-4169-A462-3509A0F43677"),
                Guid.Parse("452F48D1-FEB0-4B02-8463-A9AF0BF6FC67")
            })
            {
                configuration.Department = departmentID;

                MessagesRepository repository = TestContainer.Resolve<MessagesRepository>();

                repository.PreloadMessages();
            }
        }
        #endregion

        #region Fields and properties

        private static IContainer TestContainer;
        #endregion

        #region Initialize / cleanup methods

        [ClassInitialize]
        public static void InitializeClass(TestContext testContext)
        {
            TestContainer = ContainerFactory.CreateContainer();
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            TestContainer?.Dispose();
        }
        #endregion
    }
}