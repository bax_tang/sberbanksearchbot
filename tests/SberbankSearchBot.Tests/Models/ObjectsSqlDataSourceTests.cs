﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SberbankSearchBot.Tests
{
    [TestClass]
    public class ObjectsSqlDataSourceTests
    {
        #region Constants and fields

        private const string AvitoConnectionString = "Server=tcp:avitoobjects.database.windows.net,1433;Initial Catalog=AvitoObjects;Persist Security Info=False;" + 
            "User ID=dvmaslennikov;Password=Fuckoff!1977;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        #endregion

        #region Initialize / cleanup methods

        [TestInitialize]
        public void Initialize()
        {
        }

        [TestCleanup]
        public void Cleanup()
        {
        }
        #endregion

        #region Test methods

        [TestMethod]
        public void IsGetObjectIdentifiersExecutesSuccessfully()
        {
            ObjectsSqlDataSource dataSource = GetDataSource();

            string sectionName = "Недвижимость";
            IList<string> identifiers = dataSource.GetObjectIdentifiers(sectionName);

            Assert.IsNotNull(identifiers);
            Assert.IsTrue(identifiers.Count > 0);

            Console.WriteLine(string.Join("\r\n", identifiers));
        }

        [TestMethod]
        public void IsGetSectionsExecutesSuccessfully()
        {
            ObjectsSqlDataSource dataSource = GetDataSource();

            IList<string> sections = dataSource.GetSections();

            Assert.IsNotNull(sections);
            Assert.IsTrue(sections.Count > 0);

            Console.WriteLine(string.Join("\r\n", sections));
        }

        [TestMethod]
        public void IsGetCategoriesExecutesSuccessfully()
        {
            ObjectsSqlDataSource dataSource = GetDataSource();

            string sectionName = "Недвижимость";
            IList<string> categories = dataSource.GetCategories(sectionName);

            Assert.IsNotNull(categories);
            Assert.IsTrue(categories.Count > 0);

            Console.WriteLine(string.Join("\r\n", categories));
        }

        [TestMethod]
        public void IsGetRegionsExecutesSuccessfully()
        {
            ObjectsSqlDataSource dataSource = GetDataSource();

            string sectionName = "Недвижимость";
            IList<string> regions = dataSource.GetRegions(sectionName);

            Assert.IsNotNull(regions);
            Assert.IsTrue(regions.Count > 0);

            Console.WriteLine(string.Join("\r\n", regions));
        }

        [TestMethod]
        public void IsGetCitiesExecutesSuccessfully()
        {
            ObjectsSqlDataSource dataSource = GetDataSource();

            string sectionName = "Недвижимость";
            string regionName = "Ханты-Мансийский АО";
            IList<string> cities = dataSource.GetCities(sectionName, regionName);

            Assert.IsNotNull(cities);
            Assert.IsTrue(cities.Count > 0);

            Console.WriteLine(string.Join("\r\n", cities));
        }

        [TestMethod]
        public void IsGetTransportIssueYearsExecutesSuccessfully()
        {
            ObjectsSqlDataSource dataSource = GetDataSource();

            IList<int> issueYears = dataSource.GetTransportIssueYears();

            Assert.IsNotNull(issueYears);
            Assert.IsTrue(issueYears.Count > 0);

            Console.WriteLine(string.Join("\r\n", issueYears));
        }

        [TestMethod]
        public void IsGetByIDExecutesSuccessfully()
        {
            ObjectsSqlDataSource dataSource = GetDataSource();
            
            string sectionName = "Недвижимость";
            IList<string> identifiers = dataSource.GetObjectIdentifiers(sectionName);

            for (int index = 0; index < identifiers.Count; ++index)
            {
                string objectID = identifiers[index];

                ObjectData current = dataSource.GetByID(objectID);

                Assert.IsNotNull(current);
                Assert.IsNotNull(current.ID);
                Assert.IsNotNull(current.Images);
                Assert.IsNotNull(current.Name);

                Console.WriteLine(current);
            }
        }
        #endregion

        #region Private class methods

        private SqlConnection GetConnection()
        {
            return CreateConnection(AvitoConnectionString);
        }

        private ObjectsSqlDataSource GetDataSource()
        {
            return new ObjectsSqlDataSource(GetConnection, BotConfiguration.Instance);
        }

        private static SqlConnection CreateConnection(string connectionString)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }
        #endregion
    }
}