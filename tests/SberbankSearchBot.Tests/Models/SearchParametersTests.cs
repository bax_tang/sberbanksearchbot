﻿using System;
using System.Globalization;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SberbankSearchBot.Tests
{
    [TestClass]
    public class SearchParametersTests
    {
        #region Test methods

        [TestMethod]
        public void IsSearchParametersSuccessfullyConvertsToString()
        {
            /*
            RealtySearchParameters parameters = new RealtySearchParameters(1000, 10000, "Сургут");

            string result = parameters.ToString();

            Console.WriteLine(result);
            */
        }

        [TestMethod]
        public void IsDecimalSuccessfullyConvertsToString()
        {
            decimal price = 26500000M;

            string result = string.Format(CultureInfo.InvariantCulture, "{0:N} руб.", price);

            Console.WriteLine(result);
        }
        #endregion
    }
}