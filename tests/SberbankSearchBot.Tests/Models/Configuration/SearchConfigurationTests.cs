﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SberbankSearchBot.Tests
{
    [TestClass]
    public class SearchConfigurationTests
    {
        #region Test methods

        [TestMethod]
        public void IsSearchConfigurationCreatesSuccessfully()
        {
            string sectionName = "Недвижимость";

            SearchConfiguration configuration = new SearchConfiguration(sectionName);

            Assert.IsNotNull(configuration);
            Assert.AreNotEqual(0, configuration.Count);
            
            for (int index = 0; index < configuration.Count; ++index)
            {
                Assert.IsNotNull(configuration[index]);
            }
        }
        #endregion
    }
}