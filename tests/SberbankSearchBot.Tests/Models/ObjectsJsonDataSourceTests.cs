﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot.Tests
{
    [TestClass]
    public class ObjectsJsonDataSourceTests
    {
        #region Constants and fields

        private const string DataFilePath = @"F:\Source\Projects\SberbankSearchBot\src\SberbankSearchBot\App_Data\ObjectsData.json";

        private const string ConfigFilePath = @"F:\Source\Projects\SberbankSearchBot\src\SberbankSearchBot\App_Data\SearchConfiguration.json";
        #endregion

        #region Test methods

        [TestMethod]
        public void IsSectionsQueryExecutesSuccessfully()
        {
            ObjectsJsonDataSource dataSource = GetDataSource();

            IList<string> sections = dataSource.GetSections();

            Assert.IsNotNull(sections);
            Assert.IsTrue(sections.Count > 0);

            Console.WriteLine(string.Join("\r\n", sections));
        }

        [TestMethod]
        public void IsObjectDataExtractsSuccessfully()
        {
            ObjectsJsonDataSource dataSource = GetDataSource();

            ObjectData dataItem = dataSource.GetByID("826950776");

            Assert.IsNotNull(dataItem);
            Assert.IsNotNull(dataItem.Name);

            Console.WriteLine(dataItem.ToString(Formatting.Indented));
        }

        [TestMethod]
        public void IsRegionsDataExtractsSuccessfully()
        {
            ObjectsJsonDataSource dataSource = GetDataSource();

            string[] sections = new[] { "Недвижимость", "Транспорт", "Оборудование", "Бизнес" };
            for (int index = 0; index < sections.Length; ++index)
            {
                IList<string> regions = dataSource.GetRegions(sections[index]);

                Assert.IsNotNull(regions);
                Assert.IsTrue(regions.Count > 0);

                Console.WriteLine("Regions for section \"{0}\":", sections[index]);
                Console.WriteLine(string.Join("\r\n", regions));
                Console.WriteLine();
            }
        }

        [TestMethod]
        public void IsCitiesDataExtractsSuccessfully()
        {
            ObjectsJsonDataSource dataSource = GetDataSource();

            string sectionName = "Недвижимость";
            string regionName = "Ямало-Ненецкий АО";
            IList<string> cities = dataSource.GetCities(sectionName, regionName);

            Assert.IsNotNull(cities);
            Assert.IsTrue(cities.Count > 0);

            Console.WriteLine(string.Join("\r\n", cities));
        }

        [TestMethod]
        public void IsTransportIssueYearsExtractsSuccessfully()
        {
            ObjectsJsonDataSource dataSource = GetDataSource();

            IList<int> years = dataSource.GetTransportIssueYears();

            Assert.IsNotNull(years);
            Assert.IsTrue(years.Count > 0);

            Console.WriteLine(string.Join("\r\n", years));
        }

        [TestMethod]
        public void IsObjectsSearchExecutesSuccessfully()
        {
            var configRepository = GetConfigurationRepository();
            var dataRepository = GetDataRepository();

            string sectionName = "Недвижимость";
            string regionName = "Ханты-Мансийский АО";
            string category = "Дома, дачи, коттеджи";

            string uniqueKey = Guid.NewGuid().ToString("B").ToUpperInvariant();

            SearchConfiguration configuration = configRepository.GetOrCreateConfiguration(uniqueKey, sectionName);

            configuration.RegionItem.RegionName = regionName;
            configuration.CategoriesItem.CategoriesSet.Add(category);

            ObjectsDataSearchResults searchResults = dataRepository.SearchObjects(configuration);
            Assert.IsNotNull(searchResults);

            Console.WriteLine(string.Join("\r\n", searchResults));
        }
        #endregion

        #region Private class methods

        private static SearchConfigurationRepository GetConfigurationRepository()
        {
            return new SearchConfigurationRepository(ConfigFilePath);
        }

        private static ObjectsJsonDataSource GetDataSource()
        {
            return new ObjectsJsonDataSource(DataFilePath);
        }

        private static ObjectsDataRepository GetDataRepository()
        {
            ObjectsJsonDataSource dataSource = GetDataSource();

            return new ObjectsDataRepository(dataSource);
        }
        #endregion
    }
}