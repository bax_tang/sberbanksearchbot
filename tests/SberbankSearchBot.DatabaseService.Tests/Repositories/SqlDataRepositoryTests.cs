﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Autofac;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SberbankSearchBot.DatabaseService.Tests
{
    [TestClass]
    public class SqlDataRepositoryTests
    {
        #region Test methods

        [TestMethod]
        public void IsObjectsCreationWorksSuccessfully()
        {
            SqlDataRepository dataRepository = TestContainer.Resolve<SqlDataRepository>();

            Guid departmentID = Guid.Parse("55BF4517-D217-4169-A462-3509A0F43677");
            List<ObjectInfo> newObjects = GetObjects();

            dataRepository.CreateObjects(departmentID, newObjects);
        }
        #endregion

        #region Private class methods

        private static List<ObjectInfo> GetObjects()
        {
            string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string filePath = Path.Combine(desktopDirectory, "AvitoObjects_pao_sberbank_rossiya.json");

            List<ObjectInfo> result = null;
            if (File.Exists(filePath))
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    Formatting = Formatting.Indented
                };
                JsonSerializer serializer = JsonSerializer.Create();

                using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8, true, 8192))
                {
                    object value = serializer.Deserialize(reader, typeof(List<ObjectInfo>));

                    result = value as List<ObjectInfo>;
                }
            }
            return result ?? new List<ObjectInfo>();
        }
        #endregion

        #region Fields and properties

        private static IContainer TestContainer;
        #endregion

        #region Initialize / cleanup methods

        [ClassInitialize]
        public static void InitializeClass(TestContext context)
        {
            TestContainer = ContainerFactory.CreateContainer();
        }

        [ClassCleanup]
        public static void CleanupClass()
        {
            TestContainer?.Dispose();
        }
        #endregion
    }
}