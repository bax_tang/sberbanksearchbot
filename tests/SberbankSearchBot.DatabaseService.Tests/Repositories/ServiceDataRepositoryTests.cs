﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SberbankSearchBot.DatabaseService.Tests
{
    [TestClass]
    public class ServiceDataRepositoryTests
    {
        #region Fields and properties

        private StaticDataRepository Repository;
        #endregion

        #region Test methods

        [TestMethod]
        public void IsObjectMethodsWorksSuccessfully()
        {
            Guid departmentID = Guid.NewGuid();

            Assert.IsTrue(Repository.AddObject(departmentID, 100500L));
            Assert.IsTrue(Repository.ObjectExists(departmentID, 100500L));

            Assert.IsFalse(Repository.ObjectExists(departmentID, 9000L));
            Assert.IsFalse(Repository.ObjectExists(Guid.Empty, 42L));
        }
        #endregion

        #region Initialize / cleanup methods

        [TestInitialize]
        public void Initialize()
        {
            Repository = new StaticDataRepository();
        }
        #endregion
    }
}