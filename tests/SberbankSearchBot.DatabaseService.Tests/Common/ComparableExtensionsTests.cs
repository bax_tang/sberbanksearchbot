﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SberbankSearchBot.DatabaseService
{
    [TestClass]
    public class ComparableExtensionsTests
    {
        #region Test methods

        [TestMethod]
        public void IsBetweenMethodExecutesSuccessfully()
        {
            Assert.IsTrue(10.IsBetween(9, 11));
            Assert.IsTrue(9000L.IsBetween(1L, 100500L));
        }
        #endregion
    }
}