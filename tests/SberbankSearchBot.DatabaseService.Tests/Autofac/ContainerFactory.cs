﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;

using Autofac;
using NLog;

namespace SberbankSearchBot.DatabaseService.Tests
{
    internal static class ContainerFactory
    {
        #region Constants and fields

        private const string TestConnectionString =
            "Data Source=DESKTOP-D4CDD5E;Initial Catalog=AvitoObjects;Integrated Security=True;Connect Timeout=60;" + 
            "Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        #endregion

        #region Public class methods

        public static IContainer CreateContainer(string connectionString = null)
        {
            ContainerBuilder builder = new ContainerBuilder();

            ServiceConfiguration.Instance.Database.ConnectionString = connectionString ?? TestConnectionString;

            builder.RegisterInstance(ServiceConfiguration.Instance);
            builder.Register(GetLogger).As<ILoggerBase, ILogger, Logger>().InstancePerDependency();
            builder.Register(CreateAvitoHttpClient).Keyed<HttpClient>("avito").InstancePerDependency();
            builder.Register(CreateBotClient).InstancePerDependency();
            builder.Register(CreateDatabaseConnection).InstancePerDependency();
            builder.Register(CreateDatabaseContext).InstancePerDependency();
            builder.RegisterType<AvitoParserFactory>().InstancePerDependency();
            builder.RegisterType<StaticDataRepository>().SingleInstance();
            builder.RegisterType<SqlDataRepository>().InstancePerDependency();
            builder.RegisterType<DatabaseUpdatingService>().SingleInstance();
            builder.RegisterType<DatabaseUpdatingWorker>().InstancePerDependency();

            return builder.Build();
        }
        #endregion

        #region Private class methods

        private static Logger GetLogger(IComponentContext context)
        {
            return LogManager.GetCurrentClassLogger();
        }

        private static HttpClient CreateAvitoHttpClient(IComponentContext context)
        {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue
            {
                MaxAge = TimeSpan.FromSeconds(0.0)
            };
            client.DefaultRequestHeaders.Connection.TryParseAdd("keep-alive");
            client.DefaultRequestHeaders.UserAgent.TryParseAdd("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
            client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");

            return client;
        }

        private static BotMaintenanceClient CreateBotClient(IComponentContext context)
        {
            ILogger logger = context.Resolve<ILogger>();

            return new BotMaintenanceClient(logger, new HttpClient());
        }

        private static SqlConnection CreateDatabaseConnection(IComponentContext context)
        {
            ServiceConfiguration configuration = context.Resolve<ServiceConfiguration>();

            SqlConnection connection = new SqlConnection(configuration.Database.ConnectionString);
            connection.Open();
            return connection;
        }

        private static AvitoDbContext CreateDatabaseContext(IComponentContext context)
        {
            ServiceConfiguration configuration = context.Resolve<ServiceConfiguration>();
            SqlConnection connection = context.Resolve<SqlConnection>();

            return new AvitoDbContext(connection, configuration.Database.ExecutionTimeout);
        }
        #endregion
    }
}