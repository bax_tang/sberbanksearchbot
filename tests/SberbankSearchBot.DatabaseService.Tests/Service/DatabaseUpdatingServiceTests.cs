﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Autofac;
using NLog;

namespace SberbankSearchBot.DatabaseService.Tests
{
    [TestClass]
    public class DatabaseUpdatingServiceTests
    {
        #region Test methods

        [TestMethod]
        public void IsDatabaseUpdatesSuccessfully()
        {
            IContainer container = ContainerFactory.CreateContainer();

            var service = container.Resolve<DatabaseUpdatingService>();

            service.UpdateDatabase(true);
        }
        #endregion
    }
}