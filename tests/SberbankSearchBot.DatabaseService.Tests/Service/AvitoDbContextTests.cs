﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SberbankSearchBot.DatabaseService.Tests
{
    [TestClass]
    public class AvitoDbContextTests
    {
        #region Test methods

        [TestMethod]
        public void IsObjectIdentifiersLoadsSuccessfully()
        {
            AvitoDbContext context = CreateContext();

            IList<long> identifiers = context.GetObjectIdentifiers();

            Assert.IsNotNull(identifiers);
            Assert.IsTrue(identifiers.Count >= 0);

            context.Dispose();

            Console.WriteLine("Identifiers:\r\n{0}", string.Join("\r\n", identifiers));
        }
        #endregion

        #region Private class methods

        private static AvitoDbContext CreateContext()
        {
            ServiceConfiguration configuration = ServiceConfiguration.Instance;

            SqlConnection dbConnection = new SqlConnection(configuration.Database.ConnectionString);
            dbConnection.Open();

            return new AvitoDbContext(dbConnection, configuration.Database.ExecutionTimeout);
        }
        #endregion
    }
}