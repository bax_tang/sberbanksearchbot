﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SberbankSearchBot.DatabaseService.Tests
{
    [TestClass]
    public class AvitoWebParserTests
    {
        #region Test methods

        [TestMethod]
        public void IsWebParserWorksSuccessfully()
        {
            string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            Random generator = new Random();
            Func<int, int, int> delayFunc = new Func<int, int, int>(generator.Next);

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver(),
                DateFormatString = "dd.MM.yyyy HH:mm:ss",
                DefaultValueHandling = DefaultValueHandling.Populate,
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Include,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            JsonSerializer serializer = JsonSerializer.Create(settings);

            foreach (string baseUri in new[] { "http://avito.ru/pao_sberbank/rossiya", "http://avito.ru/sberbank/rossiya" })
            {
                HttpClient client = CreateAvitoClient();

                string filePath = CombineResultsFilePath(desktopDirectory, new Uri(baseUri));

                using (AvitoWebParser parser = new AvitoWebParser(ServiceConfiguration.Instance, client, baseUri))
                {
                    ParseAndWriteSiteData(parser, serializer, filePath, delayFunc);
                }
            }
        }

		[TestMethod]
		public void IsObjectsByCustomUrlsParsedSuccessfully()
		{
			string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			string filePath = Path.Combine(desktopDirectory, "AvitoObjects.txt");

			Random rnd = new Random();
			HttpClient client = CreateAvitoClient();

			IEnumerable<string> objectUrls = File.ReadLines(filePath);
			List<ObjectInfo> objects = new List<ObjectInfo>(50);

			using (AvitoWebParser parser = new AvitoWebParser(ServiceConfiguration.Instance, client, "..."))
			{
				foreach (string objectUrl in objectUrls)
				{
					ObjectInfo currentObject = parser.GetObjectFromUrl(objectUrl);
					if (currentObject != null)
					{
						objects.Add(currentObject);

						Thread.Sleep(rnd.Next(3000, 5000));
					}
				}
			}

			string resultFile = Path.Combine(desktopDirectory, "AvitoObjects.json");
			string objectsText = JsonConvert.SerializeObject(objects, Formatting.Indented);

			File.WriteAllText(resultFile, objectsText, Encoding.UTF8);
		}

		[TestMethod]
		public void IsObjectsByCustomUrlsCreatesSuccessfully()
		{
			string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			string filePath = Path.Combine(desktopDirectory, "AvitoObjects.txt");

			Random rnd = new Random();
			HttpClient client = CreateAvitoClient();

			var objectUrls = File.ReadLines(filePath).ToList();
			List<ObjectInfo> objects = new List<ObjectInfo>(objectUrls.Count);

			Guid departmentID = Guid.Parse("55BF4517-D217-4169-A462-3509A0F43677"); // Западно-Сибирский банк ПАО Сбербанк

			DatabaseConfiguration dbConfig = ServiceConfiguration.Instance.Database;
			dbConfig.ConnectionString = "Data Source=(local);Initial Catalog=AvitoObjects;Integrated Security=False;User ID=sa;Password=saionara;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

			string connectionString = dbConfig.ConnectionString;
			SqlConnection connection = new SqlConnection(connectionString);
			connection.Open();

			AvitoDbContext context = new AvitoDbContext(connection, 300);
			SqlDataRepository repository = new SqlDataRepository(context, new StaticDataRepository());

			using (AvitoWebParser parser = new AvitoWebParser(ServiceConfiguration.Instance, client, "..."))
			{
				foreach (string objectUrl in objectUrls)
				{
					ObjectInfo currentObject = parser.GetObjectFromUrl(objectUrl);
					if (currentObject != null)
					{
						if (context.ObjectExists(currentObject.ObjectID)) continue;

						repository.CreateObject(departmentID, currentObject);

						Thread.Sleep(rnd.Next(3000, 5000));
					}
				}
			}
		}

        [TestMethod]
        public void IsAdditionalInfoFillsSuccessfully()
        {
            ObjectInfo testObject = new ObjectInfo(
                890925798L,
                "Светильник SBL 12",
                "https://www.avito.ru/novosibirsk/mebel_i_interer/svetilnik_sbl_12_890925798");

            string baseUri = "http://avito.ru/pao_sberbank/rossiya";
            HttpClient client = CreateAvitoClient();

            using (AvitoWebParser parser = new AvitoWebParser(ServiceConfiguration.Instance, client, baseUri))
            {
                Assert.IsTrue(parser.FillAdditionalInfo(testObject));
            }
        }
        #endregion

        #region Private class methods

        private static HttpClient CreateAvitoClient()
        {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue
            {
                MaxAge = TimeSpan.FromSeconds(0.0)
            };
            client.DefaultRequestHeaders.Connection.TryParseAdd("keep-alive");
            client.DefaultRequestHeaders.UserAgent.TryParseAdd("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
            client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");

            return client;
        }

        private static string CombineResultsFilePath(string targetDirectory, Uri siteUrl)
        {
            string appender = siteUrl.PathAndQuery.Substring(1).Replace("/", "_");

            string filePath = Path.Combine(targetDirectory, $"AvitoObjects_{appender}.json");
            return filePath;
        }

        private static void ParseAndWriteSiteData(AvitoWebParser parser, JsonSerializer serializer, string filePath, Func<int, int, int> delayFunc)
        {
            int pagesCount = parser.GetPagesCount();

            List<ObjectInfo> parsedObjects = new List<ObjectInfo>(50 * pagesCount);

            for (int pageIndex = 1; pageIndex <= pagesCount; ++pageIndex)
            {
                foreach (ObjectInfo parsedObject in parser.GetObjectsFromPage(pageIndex))
                {
                    if (parsedObject.ObjectID <= 0L) continue;

                    if (parser.FillAdditionalInfo(parsedObject))
                    {
                        parsedObjects.Add(parsedObject);
                    }

                    Thread.Sleep(delayFunc.Invoke(3000, 5000));
                }
            }

            using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.UTF8, 8192))
            {
                serializer.Serialize(writer, parsedObjects);
                writer.Flush();
            }
        }
        #endregion
    }
}