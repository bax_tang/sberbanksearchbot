﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace SberbankSearchBot.DatabaseService.Tests
{
	[TestClass]
	public class SqlQueryGenerationTests
	{
		#region Test methods

		[TestMethod]
		public void IsCreateObjectsQueryGeneratesSuccessfully()
		{
			string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			string dataFile = Path.Combine(desktopDirectory, "AvitoObjects.json");

			if (File.Exists(dataFile))
			{
				JsonSerializer serializer = JsonSerializer.Create(new JsonSerializerSettings
				{
					ContractResolver = new DefaultContractResolver(),
					Formatting = Formatting.Indented,
					NullValueHandling = NullValueHandling.Include,
					ReferenceLoopHandling = ReferenceLoopHandling.Ignore
				});

				List<ObjectInfo> objectsInfo = null;

				using (JsonTextReader reader = new JsonTextReader(new StreamReader(dataFile, Encoding.UTF8)))
				{
					objectsInfo = serializer.Deserialize<List<ObjectInfo>>(reader);
				}

				if (objectsInfo != null && objectsInfo.Count > 0)
				{
					string resultFile = Path.Combine(desktopDirectory, "CreateObjects.sql");
					StreamWriter writer = new StreamWriter(resultFile, false, Encoding.UTF8, 1024);

					foreach (ObjectInfo objectInfo in objectsInfo)
					{
						string objectQuery = string.Format(CreateObjectQueryTemplate,
							objectInfo.ObjectID,
							objectInfo.Section,
							objectInfo.Category,
							objectInfo.Region,
							objectInfo.City);

						writer.WriteLine(objectQuery);
						writer.Flush();
					}
				}
			}
		}
		#endregion

		#region Constants and fields

		private const string CreateObjectQueryTemplate = @"EXEC [dbo].[sp_object_create_from_strings]
	@ObjectID = {0},
	@DepartmentID = '55BF4517-D217-4169-A462-3509A0F43677', -- Западно-Сибирский банк ПАО Сбербанк
	@SectionName = N'{1}',
	@CategoryName = N'{2}',
	@RegionName = N'{3}',
	@CityName = N'{4}';";
		#endregion
	}
}