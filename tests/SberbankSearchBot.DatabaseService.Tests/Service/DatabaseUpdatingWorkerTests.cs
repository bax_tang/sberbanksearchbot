﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Autofac;

namespace SberbankSearchBot.DatabaseService.Tests
{
    [TestClass]
    public class DatabaseUpdatingWorkerTests
    {
        #region Test methods

        [TestMethod]
        public void IsLocalDatabaseUpdatesSuccessfully()
        {
            IContainer container = ContainerFactory.CreateContainer();

            DatabaseUpdatingWorker worker = container.Resolve<DatabaseUpdatingWorker>();

            worker.UpdateDatabase(true);
        }

        [TestMethod]
        public void IsAzureDatabaseUpdatesSuccessfully()
        {
            string connectionString = 
                "Data Source=tcp:avitoobjects.database.windows.net,1433;Initial Catalog=AvitoObjects;Integrated Security=False;" + 
                "User ID=dvmaslennikov;Password=Fuckoff!1977;Connect Timeout=30;" + 
                "Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            IContainer container = ContainerFactory.CreateContainer(connectionString);

            DatabaseUpdatingWorker worker = container.Resolve<DatabaseUpdatingWorker>();

            worker.UpdateDatabase(true);
        }
        #endregion
    }
}