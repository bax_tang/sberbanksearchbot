﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot.Tests
{
    [TestClass]
    public class JArrayTests
    {
        #region Test methods

        [TestMethod]
        public void IsJArrayItemAddingExecutesSuccessfully()
        {
            JArray jsonArray = new JArray();

            jsonArray.Add(new JValue("section"));
            jsonArray.Add(new JValue("category"));
            jsonArray.Add(new JArray("city"));

            Console.WriteLine(jsonArray.ToString(Formatting.Indented, new JsonConverter[0]));
        }
        #endregion
    }
}