﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace JsonProcessing.Tests
{
    [TestClass]
    public class JsonPathTests
    {
        #region Test methods

        [TestMethod]
        public void IsTokenSelectionExecutesSuccessfully()
        {
            JArray dataArray = GetTestData("avito_sberbank_data_updated.json");

            string pathExpression = "$..[?(@.Section=='Недвижимость')].Category";

            var tokens = dataArray.SelectTokens(pathExpression, false);
            Assert.IsNotNull(tokens);

            var tokenStrings = tokens.Select(TokenToString);

            HashSet<string> citiesSet = new HashSet<string>(tokenStrings, StringComparer.OrdinalIgnoreCase);

            Console.WriteLine(string.Join("\r\n", citiesSet));
        }

        [TestMethod]
        public void IsMulticonditionalTokenSelectionExecutesSuccessfully()
        {
            JArray dataArray = GetTestData("avito_sberbank_data_updated.json");

            string pathExpression = "$..[?(@.Section=='Недвижимость' && @.Price>20000000.0 && @.Price<30000000.0)]";

            var tokens = dataArray.SelectTokens(pathExpression, false);
            Assert.IsNotNull(tokens);
            
            var tokenStrings = tokens.Take(3).Select(TokenToString);
            
            Console.WriteLine(string.Join("\r\n", tokens));
        }

        [TestMethod]
        public void IsCombinedConditionalTokenSelectionExecutesSuccessfully()
        {
            JArray dataArray = GetTestData("avito_sberbank_data_updated.json");

            string pathExpression = "$..[?(@.Section=='Недвижимость')].[?(@.Category=='Коммерческая' || @.Category=='Земельные участки')]";

            var tokens = dataArray.SelectTokens(pathExpression, false);
            Assert.IsNotNull(tokens);

            var tokenStrings = tokens.Take(3).Select(TokenToString);

            Console.WriteLine(string.Join("\r\n", tokens));
        }

        [TestMethod]
        public void IsYearProductionExtractsSuccessfully()
        {
            string filePath = @"F:\Source\Projects\SberbankSearchBot\src\SberbankSearchBot\App_Data\ObjectsData.json";
            JArray objectsData = GetTestData(filePath);

            Regex yearRegex = new Regex("(?:(?:[1]{1}\\d{1}\\d{1}\\d{1})|(?:[2]{1}\\d{3}))", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            var transportTokens = objectsData.SelectTokens("$..[?(@.Section=='Транспорт')]", false);
            foreach (JToken token in transportTokens)
            {
                try
                {
                    string description = (string)token["Description"];
                    Match yearMatch = yearRegex.Match(description);
                    token["Year"] = int.Parse(yearMatch.Value);
                }
                catch (Exception exc)
                {
                    Console.WriteLine("Can't extract year from token '{0}: {1}':\r\n{2}",
                        token["ID"],
                        token["Name"],
                        exc);
                }
            }

            SaveData(objectsData, filePath);
        }

        [TestMethod]
        public void IsYearExtractsSuccessfully()
        {
            string filePath = @"F:\Source\Projects\SberbankSearchBot\src\SberbankSearchBot\App_Data\ObjectsData.json";
            JArray objectsData = GetTestData(filePath);

            var yearTokens = objectsData.SelectTokens("$..[?(@.Section=='Транспорт')].Year", false);
            var years = yearTokens.Select(t => (int)t);

            HashSet<int> yearsSet = new HashSet<int>(years);

            Console.WriteLine(string.Join("\r\n", yearsSet));
        }
        #endregion

        #region Private class methods

        private static string TokenToString(JToken token)
        {
            string result = null;

            JValue valueToken = token as JValue;
            if (valueToken != null)
            {
                result = valueToken.ToString();
            }
            else
            {
                JProperty propertyToken = token as JProperty;
                if (propertyToken != null)
                {
                    valueToken = propertyToken.Value as JValue;
                    if (valueToken != null)
                    {
                        result = valueToken.ToString();
                    }
                }
                else
                {
                    JObject objectToken = token as JObject;
                    if (objectToken != null)
                    {
                        result = objectToken.ToString(Formatting.Indented, new JsonConverter[0]);
                    }
                }
            }

            return result ?? string.Empty;
        }

        private static JArray GetTestData(string fileName)
        {
            string filePath = null;

            if (Path.IsPathRooted(fileName))
            {
                filePath = fileName;
            }
            else
            {
                string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                filePath = Path.Combine(desktopDirectory, fileName);
            }
            
            JArray result = null;

            using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8))
            {
                result = JArray.Load(new JsonTextReader(reader));
            }

            return result;
        }

        private static void SaveData(JArray objectsData, string fileName)
        {
            string filePath = null;

            if (Path.IsPathRooted(fileName))
            {
                filePath = fileName;
            }
            else
            {
                string desktopDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                filePath = Path.Combine(desktopDirectory, fileName);
            }

            JsonReader dataReader = objectsData.CreateReader();

            using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.UTF8))
            {
                using (JsonTextWriter jsonWriter = new JsonTextWriter(writer))
                {
                    jsonWriter.WriteToken(dataReader);
                    jsonWriter.Flush();
                }
            }
        }
        #endregion
    }
}