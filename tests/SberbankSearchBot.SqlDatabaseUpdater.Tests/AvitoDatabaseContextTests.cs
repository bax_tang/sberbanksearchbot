﻿using System;
using System.Data;
using System.Data.SqlClient;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SberbankSearchBot.Tests
{
    [TestClass]
    public class AvitoDatabaseContextTests
    {
        #region Constants and fields

        public const string AvitoDBConnectionString = "Data Source=DKALINOV-PC;Initial Catalog=AvitoDB;Integrated Security=True";

        private AvitoDatabaseContext Context = null;
        #endregion

        #region Initialize / cleanup methods

        [TestInitialize]
        public void Initialize()
        {
            SqlConnection connection = CreateConnection();

            Context = new AvitoDatabaseContext(connection);
        }

        [TestCleanup]
        public void Cleanup()
        {
            Context.Dispose();
        }
        #endregion

        #region Test methods

        [TestMethod]
        public void IsRegionCreatesSuccessfully()
        {
            string regionName = "Ханты-Мансийский АО";

            Guid regionID = Context.GetOrCreateRegion(regionName);

            Assert.AreNotEqual(Guid.Empty, regionID);
        }
        #endregion

        #region Private class methods

        private static SqlConnection CreateConnection()
        {
            SqlConnection connection = new SqlConnection(AvitoDBConnectionString);
            connection.Open();
            return connection;
        }
        #endregion
    }
}