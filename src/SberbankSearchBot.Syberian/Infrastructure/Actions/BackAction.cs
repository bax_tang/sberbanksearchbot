﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
	public class BackAction : BotAction
	{
		#region Constructors

		public BackAction() : base(BotConstants.BackActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			string actionCode = GetTargetActionCode(userActivity.Text);

			BotAction targetAction = ParentRepository.GetAction(actionCode);

			userActivity.Text = userActivity.Text.Substring(5);

			return targetAction.ProcessActivityAsync(userActivity);
		}
		#endregion

		#region Private class methods

		private static string GetTargetActionCode(string activityText)
		{
			string result = null;

			int startIndex = activityText.IndexOf(' ');
			int endIndex = activityText.IndexOf(' ', 1 + startIndex);

			if (endIndex == -1)
			{
				result = activityText.Substring(1 + startIndex);
			}
			else
			{
				result = activityText.Substring(1 + startIndex, endIndex - startIndex - 1);
			}

			return result;
		}
		#endregion
	}
}