﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
	public class SectionAction : BotAction
	{
		#region Constructors

		public SectionAction() : base(BotConstants.SectionActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			Activity replyActivity = userActivity.CreateReply(null, "ru");

			HeroCard sectionCard = CreateSectionCard(userActivity);

			replyActivity.Attachments.Add(sectionCard.ToAttachment());

			ConnectorClient connector = GetConnector(userActivity);

			return connector.Conversations.ReplyToActivityAsync(replyActivity);
		}
		#endregion

		#region Private class methods

		private HeroCard CreateSectionCard(KeyedActivity userActivity)
		{
			List<CardAction> cardButtons = new List<CardAction>(10);

			cardButtons.Add(ActionSource.CreateBackAction("search"));

			string activityKey = userActivity.GetActivityKey();
			string activityText = userActivity.Text;
			int spaceIndex = activityText.IndexOf(' ');
			string sectionName = activityText.Substring(1 + spaceIndex);

			SearchConfiguration configuration = ConfigurationRepository.GetOrCreateConfiguration(activityKey, sectionName);
			for (int index = 0; index < configuration.Count; ++index)
			{
				SearchConfigurationItem configItem = configuration[index];
				if (configItem.Enabled)
				{
					string actionCode = configItem.ActionCode;
					string actionTitle = configItem.ToString();

					cardButtons.Add(new CardAction(ActionTypes.PostBack, actionTitle, null, actionCode));
				}
			}

			cardButtons.Add(ActionSource.CreateDoSearchAction());

			HeroCard sectionCard = new HeroCard
			{
				Title = string.Format(BotConstants.SearchCriteriaCardTitle, sectionName),
				Text = BotConstants.SearchCriteriaCardText,
				Images = new CardImage[0],
			};

			sectionCard.Buttons = cardButtons;

			return sectionCard;
		}
		#endregion
	}
}