﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
	public class GreetingAction : BotAction
	{
		#region Constants and fields
		#endregion

		#region Constructors

		public GreetingAction() : base(BotConstants.GreetingActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			Activity replyActivity = userActivity.CreateReply(null, "ru");

			int hIndex = userActivity.Text.LastIndexOf("-h");

			HeroCard greetingCard = CreateGreetingCard(hIndex == -1);
			Attachment cardAttachment = greetingCard.ToAttachment();
			replyActivity.Attachments.Add(cardAttachment);

			ConnectorClient connector = GetConnector(userActivity);

			return connector.Conversations.ReplyToActivityAsync(replyActivity);
		}
		#endregion

		#region Private class methods

		private HeroCard CreateGreetingCard(bool showHeader)
		{
			List<CardAction> cardButtons = new List<CardAction>(4);

			cardButtons.Add(ActionSource.CreateSearchAction());
			cardButtons.Add(ActionSource.CreateManageFavoritesAction());
			cardButtons.Add(ActionSource.CreateManageSubscriptionsAction());
			cardButtons.Add(ActionSource.CreateSettingsAction());

			HeroCard greetingCard = new HeroCard
			{
				Buttons = cardButtons,
				Images = new List<CardImage>()
			};

			if (showHeader)
			{
				greetingCard.Title = BotConstants.GreetingCardTitle;
				greetingCard.Text = Messages.GetMessage("GreetingCardText"); //BotConstants.GreetingCardText;
			}
			else
			{
				greetingCard.Text = Messages.GetMessage("GreetingCardTextNoHeader"); //BotConstants.GreetingCardTextNoHeader;
			}

			return greetingCard;
		}
		#endregion
	}
}