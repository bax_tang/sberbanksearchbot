﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
	public class PriceAction : BotAction
	{
		#region Constants and fields
		#endregion

		#region Constructors

		public PriceAction() : base(BotConstants.PriceActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			Activity replyActivity = ProcessUserActivity(userActivity);

			ConnectorClient connector = GetConnector(userActivity);

			return connector.Conversations.ReplyToActivityAsync(replyActivity);
		}
		#endregion

		#region Private class methods

		private Activity ProcessUserActivity(KeyedActivity userActivity)
		{
			Activity replyActivity = userActivity.CreateReply(null, "ru");

			HeroCard replyCard = null;

			string activityText = userActivity.Text;
			int setIndex = activityText.IndexOf("set");
			int minIndex = activityText.IndexOf("min");
			int maxIndex = activityText.IndexOf("max");
			int ownIndex = activityText.IndexOf("own");

			if (setIndex != -1)
			{
				ProcessPriceSetting(userActivity, replyActivity, ownIndex, minIndex, maxIndex);

				if (ownIndex == -1)
				{
					replyCard = CreatePricesCard(userActivity);
				}
			}
			else
			{
				if (minIndex != -1)
				{
					replyCard = CreateMinPricesCard(userActivity);
				}
				else if (maxIndex != -1)
				{
					replyCard = CreateMaxPricesCard(userActivity);
				}
				else
				{
					replyCard = CreatePricesCard(userActivity);
				}
			}

			if (replyCard != null)
			{
				replyActivity.Attachments.Add(replyCard.ToAttachment());
			}

			return replyActivity;
		}

		private void ProcessPriceSetting(KeyedActivity userActivity, Activity replyActivity, int ownIndex, int minIndex, int maxIndex)
		{
			if (ownIndex != -1)
			{
				HeroCard replyCard = new HeroCard
				{
					Images = new CardImage[0]
				};

				string activityKey = userActivity.GetActivityKey();

				CardAction[] cardButtons = new CardAction[1];

				if (minIndex != -1)
				{
					Executor.SetActionState(activityKey, CurrentActionState.WaitForMinPrice);

					cardButtons[0] = ActionSource.CreateBackAction("price min");
					replyCard.Text = BotConstants.PriceMinOwnText;
				}
				else if (maxIndex != -1)
				{
					Executor.SetActionState(activityKey, CurrentActionState.WaitForMaxPrice);

					cardButtons[0] = ActionSource.CreateBackAction("price max");
					replyCard.Text = BotConstants.PriceMaxOwnText;
				}

				replyCard.Buttons = cardButtons;

				replyActivity.Attachments.Add(replyCard.ToAttachment());
			}
			else
			{
				string activityKey = userActivity.GetActivityKey();
				SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);

				Regex decimalRegex = RegularExpressions.GetDecimalRegex();
				Match indexMatch = decimalRegex.Match(userActivity.Text);
				int priceIndex = int.Parse(indexMatch.Value, NumberStyles.Any, CultureInfo.InvariantCulture);
				var prices = (minIndex != -1) ? PriceSource.MinPrices : PriceSource.MaxPrices;
				string priceText = prices[priceIndex];
				Match priceMatch = decimalRegex.Match(priceText);

				double price = double.Parse(priceMatch.Value, NumberStyles.Any, CultureInfo.InvariantCulture);

				int thousandIndex = priceText.IndexOf("тыс");
				int millionIndex = priceText.IndexOf("млн");

				if (thousandIndex != -1) price *= 1000.0;
				else if (millionIndex != -1) price *= 1000000.0;

				PricesConfigurationItem pricesItem = configuration.PricesItem;

				if (minIndex != -1)
				{
					pricesItem.ChangePrice(price, prices[priceIndex], PriceType.MinPrice);
				}
				else if (maxIndex != -1)
				{
					pricesItem.ChangePrice(price, prices[priceIndex], PriceType.MaxPrice);
				}
				else throw new InvalidOperationException("Unknown price type.");
			}
		}

		private HeroCard CreatePricesCard(KeyedActivity userActivity)
		{
			string activityKey = userActivity.GetActivityKey();

			SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);

			List<CardAction> cardButtons = new List<CardAction>(5);

			cardButtons.Add(ActionSource.CreateBackAction(string.Concat("section", " ", configuration.SectionName)));

			PricesConfigurationItem pricesItem = configuration.PricesItem;
			if (pricesItem.MinPriceExists || pricesItem.MaxPriceExists)
			{
				if (pricesItem.MinPriceExists)
				{
					cardButtons.Add(ActionSource.CreatePriceAction(string.Concat(BotConstants.PriceMinActionTitle, ": ", pricesItem.MinPriceHeader), true));
				}
				else
				{
					cardButtons.Add(ActionSource.CreatePriceAction(BotConstants.PriceAnyMinActionTitle, true));
				}

				if (pricesItem.MaxPriceExists)
				{
					cardButtons.Add(ActionSource.CreatePriceAction(string.Concat(BotConstants.PriceMaxActionTitle, ": ", pricesItem.MaxPriceHeader), false));
				}
				else
				{
					cardButtons.Add(ActionSource.CreatePriceAction(BotConstants.PriceAnyMaxActionTitle, false));
				}
			}
			else
			{
				cardButtons.Add(ActionSource.CreatePriceAction(BotConstants.PriceAnyMinActionTitle, true));
				cardButtons.Add(ActionSource.CreatePriceAction(BotConstants.PriceAnyMaxActionTitle, false));
			}

			cardButtons.Add(ActionSource.CreateDoSearchAction());

			HeroCard pricesCard = new HeroCard
			{
				Buttons = cardButtons,
				Images = new CardImage[0],
				Title = BotConstants.PriceActionTitle,
				Text = BotConstants.PriceActionText
			};

			return pricesCard;
		}

		private HeroCard CreateMinPricesCard(KeyedActivity userActivity)
		{
			List<CardAction> cardButtons = new List<CardAction>(12);

			cardButtons.Add(ActionSource.CreateBackAction("price"));

			var minPrices = PriceSource.MinPrices;
			for (int index = 0; index < minPrices.Count; ++index)
			{
				cardButtons.Add(ActionSource.CreateSetPriceAction(minPrices[index], index, true));
			}

			cardButtons.Add(ActionSource.CreateCustomPriceAction("Своя", "price set min own"));

			HeroCard pricesCard = new HeroCard
			{
				Buttons = cardButtons,
				Images = new CardImage[0],
				Title = BotConstants.PriceMinActionTitle,
				Text = BotConstants.PriceMinActionText
			};

			return pricesCard;
		}

		private HeroCard CreateMaxPricesCard(KeyedActivity userActivity)
		{
			List<CardAction> cardButtons = new List<CardAction>(12);

			cardButtons.Add(ActionSource.CreateBackAction("price"));

			var maxPrices = PriceSource.MaxPrices;
			for (int index = 0; index < maxPrices.Count; ++index)
			{
				cardButtons.Add(ActionSource.CreateSetPriceAction(maxPrices[index], index, false));
			}

			cardButtons.Add(ActionSource.CreateCustomPriceAction("Своя", "price set max own"));

			HeroCard pricesCard = new HeroCard
			{
				Buttons = cardButtons,
				Images = new CardImage[0],
				Title = BotConstants.PriceMaxActionTitle,
				Text = BotConstants.PriceMaxActionText
			};

			return pricesCard;
		}
		#endregion
	}
}