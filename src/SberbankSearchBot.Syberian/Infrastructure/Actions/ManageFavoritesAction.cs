﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
	public class ManageFavoritesAction : BotAction
	{
		#region Constructors

		public ManageFavoritesAction() : base(BotConstants.ManageFavoritesActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			Activity replyActivity = ProcessUserActivity(userActivity);

			ConnectorClient connector = GetConnector(userActivity);

			return connector.Conversations.ReplyToActivityAsync(replyActivity);
		}
		#endregion

		#region Private class methods

		private Activity ProcessUserActivity(KeyedActivity userActivity)
		{
			Activity replyActivity = userActivity.CreateReply(null, "ru");

			string activityKey = userActivity.GetActivityKey();
			var favorites = FavoritesRepository.GetFavorites(activityKey);
			if (favorites.Count > 0)
			{
				BuildFavoritesCards(favorites, replyActivity);
			}
			else
			{
				BuildEmptyFavoritesReply(replyActivity);
			}

			return replyActivity;
		}

		private void BuildFavoritesCards(ICollection<string> favorites, Activity replyActivity)
		{
			replyActivity.Text = BotConstants.FavoritesReplyMessageText;

			var dataRepository = DataRepository;

			foreach (string objectID in favorites)
			{
				ObjectData current = dataRepository.GetByID(objectID);

				HeroCard objectCard = HeroCardSource.CreateObjectCardForFavorites(current);

				replyActivity.Attachments.Add(objectCard.ToAttachment());
			}
		}

		private void BuildEmptyFavoritesReply(Activity replyActivity)
		{
			CardAction[] cardButtons = new CardAction[2];

			cardButtons[0] = ActionSource.CreateSearchAction();
			cardButtons[1] = ActionSource.CreateBackAction("start -h");

			HeroCard emptyFavoritesCard = new HeroCard
			{
				Buttons = cardButtons,
				Images = new CardImage[0],
				Text = BotConstants.NoFavoritesMessageText
			};

			replyActivity.Attachments.Add(emptyFavoritesCard.ToAttachment());
		}
		#endregion
	}
}