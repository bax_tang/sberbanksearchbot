﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
	public class SearchAction : BotAction
	{
		#region Constants and fields
		#endregion

		#region Constructors

		public SearchAction() : base(BotConstants.SearchActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			Activity replyActivity = userActivity.CreateReply(null, "ru");

			string activityKey = userActivity.GetActivityKey();
			ConfigurationRepository.RemoveConfigurationIfExists(activityKey);
			SubscriptionsRepository.RemoveResultsIfExists(activityKey);

			HeroCard searchCard = CreateSearchCard();
			replyActivity.Attachments.Add(searchCard.ToAttachment());

			ConnectorClient connector = GetConnector(userActivity);

			return connector.Conversations.ReplyToActivityAsync(replyActivity);
		}
		#endregion

		#region Private class methods

		private HeroCard CreateSearchCard()
		{
			HeroCard searchCard = new HeroCard
			{
				Images = new List<CardImage>(),
				Title = BotConstants.SearchCardTitle,
				Text = BotConstants.SearchCardText
			};

			List<CardAction> searchButtons = new List<CardAction>(6);

			searchButtons.Add(ActionSource.CreateBackAction("start -h"));

			IList<string> sections = DataRepository.GetSections();
			for (int index = 0; index < sections.Count; ++index)
			{
				CardAction sectionAction = ActionSource.CreateSectionAction(sections[index]);

				searchButtons.Add(sectionAction);
			}

			searchCard.Buttons = searchButtons;

			return searchCard;
		}
		#endregion
	}
}