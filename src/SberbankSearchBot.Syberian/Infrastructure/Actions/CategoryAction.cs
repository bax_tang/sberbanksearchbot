﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
	public class CategoryAction : BotAction
	{
		#region Constructors

		public CategoryAction() : base(BotConstants.CategoryActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			Activity replyActivity = ProcessUserActivity(userActivity);

			ConnectorClient connector = GetConnector(userActivity);

			return connector.Conversations.ReplyToActivityAsync(replyActivity);
		}
		#endregion

		#region Private class methods

		private Activity ProcessUserActivity(KeyedActivity userActivity)
		{
			Activity replyActivity = userActivity.CreateReply(null, "ru");

			string activityKey = userActivity.GetActivityKey();
			string activityText = userActivity.Text;
			int setIndex = activityText.IndexOf("set");
			if (setIndex != -1)
			{
				ProcessCategorySetting(activityKey, activityText, setIndex);
			}

			HeroCard replyCard = CreateCategoriesCard(activityKey);

			replyActivity.Attachments.Add(replyCard.ToAttachment());

			return replyActivity;
		}

		private HeroCard CreateCategoriesCard(string activityKey)
		{
			SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);

			string sectionName = configuration.SectionName;
			IList<string> categories = DataRepository.GetCategories(sectionName);

			HashSet<string> categoriesSet = configuration.CategoriesItem.CategoriesSet;

			List<CardAction> cardButtons = new List<CardAction>(2 + categories.Count);

			cardButtons.Add(ActionSource.CreateBackAction(string.Concat("section", " ", sectionName)));

			for (int index = 0; index < categories.Count; ++index)
			{
				string categoryName = categories[index];
				bool isEnabled = categoriesSet.Contains(categoryName);

				cardButtons.Add(ActionSource.CreateCategoryAction(categoryName, isEnabled));
			}

			cardButtons.Add(ActionSource.CreateDoSearchAction());

			HeroCard categoriesCard = new HeroCard
			{
				Buttons = cardButtons,
				Images = new CardImage[0],
				Title = BotConstants.CategoryCardTitle,
				Text = BotConstants.CategoryCardText
			};

			return categoriesCard;
		}

		private void ProcessCategorySetting(string activityKey, string activityText, int setIndex)
		{
			SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);

			string categoryName = activityText.Substring(4 + setIndex);

			HashSet<string> categories = configuration.CategoriesItem.CategoriesSet;

			if (categories.Contains(categoryName))
			{
				categories.Remove(categoryName);
			}
			else
			{
				categories.Add(categoryName);
			}
		}
		#endregion
	}
}