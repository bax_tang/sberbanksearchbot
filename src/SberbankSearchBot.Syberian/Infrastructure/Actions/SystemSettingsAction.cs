﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
	public class SystemSettingsAction : BotAction
	{
		#region Constructors

		public SystemSettingsAction() : base(BotConstants.SystemSettingsActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			Activity replyActivity = ProcessUserActivity(userActivity);

			ConnectorClient connector = GetConnector(userActivity);

			return connector.Conversations.ReplyToActivityAsync(replyActivity);
		}
		#endregion

		#region Private class methods

		private Activity ProcessUserActivity(KeyedActivity userActivity)
		{
			Activity replyActivity = userActivity.CreateReply(null, "ru");

			HeroCard replyCard = BuildSystemConfigurationCard(userActivity.GetActivityKey());

			replyActivity.Attachments.Add(replyCard.ToAttachment());

			return replyActivity;
		}

		private HeroCard BuildSystemConfigurationCard(string activityKey)
		{
			SystemConfiguration configuration = SystemRepository.GetConfiguration(activityKey);

			CardAction[] cardButtons = new CardAction[2];

			cardButtons[0] = ActionSource.CreateBackAction("start -h");
			cardButtons[1] = ActionSource.CreateSearchResultsAction(configuration.SearchResultsCount);

			HeroCard replyCard = new HeroCard
			{
				Buttons = cardButtons,
				Images = new CardImage[0],
				Title = BotConstants.SystemSettingsCardTitle,
				Text = BotConstants.SystemSettingsCardText
			};

			return replyCard;
		}
		#endregion
	}
}