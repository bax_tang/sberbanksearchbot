﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
	public class ForwardAction : BotAction
	{
		#region Constructors

		public ForwardAction() : base(BotConstants.ForwardActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			Activity replyActivity = userActivity.CreateReply(null, "ru");

			ConnectorClient connector = GetConnector(userActivity);

			return connector.Conversations.ReplyToActivityAsync(replyActivity);
		}
		#endregion

		#region Private class methods
		#endregion
	}
}