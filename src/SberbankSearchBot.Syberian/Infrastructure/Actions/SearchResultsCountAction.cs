﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
	public class SearchResultsCountAction : BotAction
	{
		#region Constructors

		public SearchResultsCountAction() : base(BotConstants.ResultsCountActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			Activity replyActivity = ProcessUserActivity(userActivity);

			ConnectorClient connector = GetConnector(userActivity);

			return connector.Conversations.ReplyToActivityAsync(replyActivity);
		}
		#endregion

		#region Private class methods

		private Activity ProcessUserActivity(KeyedActivity userActivity)
		{
			string activityKey = userActivity.GetActivityKey();

			Executor.SetActionState(activityKey, CurrentActionState.WaitForSearchResultsCount);

			Activity replyActivity = userActivity.CreateReply(null, "ru");

			HeroCard replyCard = CreateReplyCard();

			replyActivity.Attachments.Add(replyCard.ToAttachment());

			return replyActivity;
		}

		private HeroCard CreateReplyCard()
		{
			CardAction[] cardButtons = new CardAction[1];
			cardButtons[0] = ActionSource.CreateBackAction(BotConstants.SystemSettingsActionCode);

			HeroCard replyCard = new HeroCard
			{
				Buttons = cardButtons,
				Images = new CardImage[0],
				Text = BotConstants.ResultsCountMessageText
			};

			return replyCard;
		}
		#endregion
	}
}