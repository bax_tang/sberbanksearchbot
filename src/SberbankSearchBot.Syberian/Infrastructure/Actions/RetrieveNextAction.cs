﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
	public class RetrieveNextAction : BotActionWithSubscriptionCard
	{
		#region Constructors

		public RetrieveNextAction() : base(BotConstants.RetrieveNextActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			bool nextAvailable = false;
			Activity replyActivity = ProcessUserActivity(userActivity, out nextAvailable);

			ConnectorClient connector = GetConnector(userActivity);

			Task<ResourceResponse> objectsTask = connector.Conversations.ReplyToActivityAsync(replyActivity);

			return ContinueWithSubscriptionCardSending(objectsTask, new SubscriptionActivityState(userActivity, nextAvailable));
		}
		#endregion

		#region Private class methods

		private Activity ProcessUserActivity(KeyedActivity userActivity, out bool retrievingNextAvailable)
		{
			Activity replyActivity = userActivity.CreateReply(null, "ru");

			string activityKey = userActivity.GetActivityKey();
			SystemConfiguration systemConfiguration = SystemRepository.GetConfiguration(activityKey);
			HashSet<string> foundedItems = SubscriptionsRepository.GetFoundedItems(activityKey);
			ObjectsDataSearchResults searchResult = SubscriptionsRepository.GetSearchResult(activityKey);

			int count = 0;
			int maxCount = systemConfiguration.SearchResultsCount;

			IEnumerator<ObjectData> resultsEnumerator = searchResult.GetEnumerator();
			if (resultsEnumerator.MoveNext())
			{
				do
				{
					ObjectData foundedObject = resultsEnumerator.Current;
					if (foundedItems.Contains(foundedObject.ID))
					{
						if (resultsEnumerator.MoveNext()) continue;
					}
					else
					{
						foundedItems.Add(foundedObject.ID);

						HeroCard objectCard = HeroCardSource.CreateObjectCard(foundedObject);
						replyActivity.Attachments.Add(objectCard.ToAttachment());

						if (++count == maxCount) break;
						if (!resultsEnumerator.MoveNext()) break;
					}
				}
				while (true);
			}
			else
			{
				replyActivity.Text = BotConstants.NoResultsMessageText;
			}

			bool nextAvailable = resultsEnumerator.MoveNext();
			retrievingNextAvailable = nextAvailable;
			if (nextAvailable)
			{
				SubscriptionsRepository.SetSearchResults(activityKey, foundedItems, searchResult);
			}
			else
			{
				SubscriptionsRepository.SetSearchResults(activityKey, foundedItems);
			}

			return replyActivity;
		}
		#endregion
	}
}