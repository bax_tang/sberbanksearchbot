﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
	public class DoSearchAction : BotActionWithSubscriptionCard
	{
		#region Constructors

		public DoSearchAction() : base(BotConstants.DoSearchActionCode) { }
		#endregion

		#region BotAction methods overriding

		public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
		{
			bool nextAvailable = false;
			Activity replyActivity = ProcessUserActivity(userActivity, out nextAvailable);

			ConnectorClient connector = GetConnector(userActivity);

			Task<ResourceResponse> objectsTask = connector.Conversations.ReplyToActivityAsync(replyActivity);

			return ContinueWithSubscriptionCardSending(objectsTask, new SubscriptionActivityState(userActivity, nextAvailable));
		}
		#endregion

		#region Private class methods

		private Activity ProcessUserActivity(KeyedActivity userActivity, out bool retrievingNextAvailable)
		{
			Activity replyActivity = userActivity.CreateReply(null, "ru");

			string activityKey = userActivity.GetActivityKey();
			SystemConfiguration systemConfiguration = SystemRepository.GetConfiguration(activityKey);
			SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);
			ObjectsDataSearchResults searchResult = DataRepository.SearchObjects(configuration);

			int resultsCount = searchResult.Count();

			HashSet<string> foundedItems = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

			if (resultsCount > 0)
			{
				replyActivity.Text = string.Format(BotConstants.SearchResultsMessageText, resultsCount);

				int count = 0;
				int maxCount = systemConfiguration.SearchResultsCount;

				IEnumerator<ObjectData> resultsEnumerator = searchResult.GetEnumerator();
				if (resultsEnumerator.MoveNext())
				{
					do
					{
						ObjectData foundedObject = resultsEnumerator.Current;
						foundedItems.Add(foundedObject.ID);

						HeroCard objectCard = HeroCardSource.CreateObjectCard(foundedObject);
						replyActivity.Attachments.Add(objectCard.ToAttachment());

						if (++count == maxCount) break;
						if (!resultsEnumerator.MoveNext()) break;
					}
					while (true);
				}
			}
			else
			{
				replyActivity.Text = BotConstants.NoResultsMessageText;
			}

			bool nextAvailable = (resultsCount > systemConfiguration.SearchResultsCount);
			retrievingNextAvailable = nextAvailable;
			if (nextAvailable)
			{
				SubscriptionsRepository.SetSearchResults(activityKey, foundedItems, searchResult);
			}
			else
			{
				SubscriptionsRepository.SetSearchResults(activityKey, foundedItems);
			}

			replyActivity.AttachmentLayout = AttachmentLayoutTypes.Carousel;

			return replyActivity;
		}
		#endregion
	}
}