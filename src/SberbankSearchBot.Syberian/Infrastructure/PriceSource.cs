﻿using System;
using System.Collections.Generic;

namespace SberbankSearchBot
{
    public static class PriceSource
    {
        #region Constants and fields

        public static readonly IReadOnlyList<string> MinPrices;

        public static readonly IReadOnlyList<string> MaxPrices;
        #endregion

        #region Constructors

        static PriceSource()
        {
            MinPrices = new[]
            {
                "50 тыс. руб.",
                "100 тыс. руб",
                "1 млн. руб.",
                "3 млн. руб.",
                "5 млн. руб.",
                "10 млн. руб.",
                "25 млн. руб.",
                "50 млн. руб."
            };
            MaxPrices = new[]
            {
                "50 тыс. руб.",
                "100 тыс. руб",
                "1 млн. руб.",
                "3 млн. руб.",
                "5 млн. руб.",
                "10 млн. руб.",
                "25 млн. руб.",
                "50 млн. руб."
            };
        }
        #endregion
    }
}