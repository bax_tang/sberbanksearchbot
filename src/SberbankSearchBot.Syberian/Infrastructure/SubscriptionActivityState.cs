﻿using System;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class SubscriptionActivityState
    {
        public KeyedActivity UserActivity;

        public bool RetrievingNextAvailable;

        public SubscriptionActivityState(KeyedActivity userActivity, bool nextAvailable)
        {
            UserActivity = userActivity;
            RetrievingNextAvailable = nextAvailable;
        }
    }
}