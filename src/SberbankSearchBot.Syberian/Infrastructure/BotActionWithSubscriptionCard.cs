﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
	public abstract class BotActionWithSubscriptionCard : BotAction
	{
		#region Constructors

		protected internal BotActionWithSubscriptionCard(string actionCode) : base(actionCode) { }
		#endregion

		#region Protected class methods

		protected Task<ResourceResponse> ContinueWithSubscriptionCardSending(Task<ResourceResponse> sourceTask, SubscriptionActivityState state)
		{
			return sourceTask.ContinueWith(SendSubscriptionCard, state);
		}
		#endregion

		#region Private class methods

		private ResourceResponse SendSubscriptionCard(Task<ResourceResponse> sourceTask, object state)
		{
			SubscriptionActivityState subscriptionState = state as SubscriptionActivityState;

			KeyedActivity userActivity = subscriptionState.UserActivity;
			bool nextAvailable = subscriptionState.RetrievingNextAvailable;

			Activity replyActivity = userActivity.CreateReply(null, "ru");

			ResourceResponse response = null;

			SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(userActivity.GetActivityKey());
			if (configuration != null)
			{
				HeroCard subscriptionCard = HeroCardSource.CreateSubscriptionCard(configuration.SectionName, nextAvailable);
				replyActivity.Attachments.Add(subscriptionCard.ToAttachment());

				ConnectorClient connector = GetConnector(userActivity);
				response = connector.Conversations.ReplyToActivity(replyActivity);
			}
			else
			{
				response = new ResourceResponse("Success");
			}

			return response;
		}
		#endregion
	}
}