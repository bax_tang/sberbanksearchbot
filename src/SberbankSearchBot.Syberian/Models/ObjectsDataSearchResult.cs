﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class ObjectsDataSearchResults : IEnumerable<ObjectData>
    {
        #region Constants and fields

        private readonly ObjectsDataRepository DataRepository;

        private readonly IList<string> Identifiers;

        private readonly SearchConfiguration Configuration;

        private IEnumerable<ObjectData> InnerEnumerable;

        private HashSet<string> CategoriesSet;

        private string Region;

        private HashSet<string> CitiesSet;

        private HashSet<int> YearsSet;

        private double MinPrice = -1.0;

        private double MaxPrice = -1.0;

        private Regex KeywordsRegex;
        #endregion

        #region Constructors

        internal ObjectsDataSearchResults()
        {
            InnerEnumerable = new ObjectData[0];
        }

        public ObjectsDataSearchResults(ObjectsDataRepository dataRepository, IList<string> identifiers, SearchConfiguration configuration)
        {
            if (dataRepository == null)
            {
                throw new ArgumentNullException(nameof(dataRepository), "Data repository can't be null.");
            }
            if (identifiers == null)
            {
                throw new ArgumentNullException(nameof(identifiers), "List of object identifiers can't be null.");
            }
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration), "Search configuration can't be null.");
            }

            DataRepository = dataRepository;
            Identifiers = identifiers;
            Configuration = configuration;
            InnerEnumerable = identifiers.Select(dataRepository.GetByID);
        }
        #endregion

        #region IEnumerable implementation

        IEnumerator IEnumerable.GetEnumerator()
        {
            return InnerEnumerable.GetEnumerator();
        }

        public IEnumerator<ObjectData> GetEnumerator()
        {
            return InnerEnumerable.GetEnumerator();
        }
        #endregion

        #region Internal class methods

        internal void Prepare()
        {
            SearchConfiguration configuration = Configuration;

            CreateCategoriesFilterIfPresent(configuration.CategoriesItem);
            CreateRegionFilterIfPresent(configuration.RegionItem);
            CreateCitiesFilterIfPresent(configuration.CitiesItem);
            CreatePricesFilterIfPresent(configuration.PricesItem);
            CreateIssueYearFilterIfPresent(configuration.IssueYearsItem);
            CreateKeywordsFilterIfPresent(configuration.KeywordsItem);
        }
        #endregion

        #region Private class methods

        private void CreateCategoriesFilterIfPresent(CategoriesConfigurationItem categoriesItem)
        {
            if (categoriesItem.Enabled && categoriesItem.CategoriesSet.Count > 0)
            {
                HashSet<string> categoriesSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                categoriesSet.UnionWith(categoriesItem.CategoriesSet);
                CategoriesSet = categoriesSet;

                InnerEnumerable = InnerEnumerable.Where(ApplyCategoryFilter);
            }
        }

        private void CreateRegionFilterIfPresent(RegionConfigurationItem regionItem)
        {
            if (regionItem.Enabled)
            {
                if (string.IsNullOrEmpty(regionItem.RegionName)) return;

                Region = string.Copy(regionItem.RegionName);

                InnerEnumerable = InnerEnumerable.Where(ApplyRegionFilter);
            }
        }

        private void CreateCitiesFilterIfPresent(CitiesConfigurationItem citiesItem)
        {
            if (citiesItem.Enabled && citiesItem.CitiesSet.Count > 0)
            {
                HashSet<string> citiesSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                citiesSet.UnionWith(citiesItem.CitiesSet);
                CitiesSet = citiesSet;

                InnerEnumerable = InnerEnumerable.Where(ApplyCityFilter);
            }
        }

        private void CreatePricesFilterIfPresent(PricesConfigurationItem pricesItem)
        {
            if (pricesItem.Enabled)
            {
                if (pricesItem.MinPriceExists)
                {
                    MinPrice = pricesItem.MinPrice;
                }
                if (pricesItem.MaxPriceExists)
                {
                    MaxPrice = pricesItem.MaxPrice;
                }

                InnerEnumerable = InnerEnumerable.Where(ApplyPricesFilter);
            }
        }

        private void CreateIssueYearFilterIfPresent(IssueYearsConfigurationItem issueYearItem)
        {
            if (issueYearItem.Enabled)
            {
                HashSet<int> yearsSet = new HashSet<int>();
                yearsSet.UnionWith(issueYearItem.YearsSet);
                YearsSet = yearsSet;

                InnerEnumerable = InnerEnumerable.Where(ApplyIssueYearFilter);
            }
        }

        private void CreateKeywordsFilterIfPresent(KeywordsConfigurationItem keywordsItem)
        {
            if (keywordsItem.Enabled && keywordsItem.Keywords.Length > 0)
            {
                StringBuilder expressionBuilder = new StringBuilder("(?:", 64);

                string[] keywordsArray = keywordsItem.Keywords;

                for (int index = 0; index < keywordsArray.Length; ++index)
                {
                    expressionBuilder.Append(keywordsArray[index]).Append('|');
                }

                expressionBuilder[expressionBuilder.Length - 1] = ')';

                Regex keywordsRegex = new Regex(expressionBuilder.ToString(), RegexOptions.IgnoreCase | RegexOptions.Singleline);
                KeywordsRegex = keywordsRegex;

                InnerEnumerable = InnerEnumerable.Where(ApplyKeywordsFilter);
            }
        }
        #endregion

        #region Filtering methods

        private bool ApplyCategoryFilter(ObjectData currentObject)
        {
            var categories = CategoriesSet;
            return (categories == null) || (categories.Count == 0) || categories.Contains(currentObject.Category);
        }

        private bool ApplyRegionFilter(ObjectData currentObject)
        {
            return string.IsNullOrEmpty(Region) || string.Equals(Region, currentObject.Region, StringComparison.OrdinalIgnoreCase);
        }

        private bool ApplyCityFilter(ObjectData currentObject)
        {
            var cities = CitiesSet;

            return (cities == null) || (cities.Count == 0) || cities.Contains(currentObject.City);
        }

        private bool ApplyPricesFilter(ObjectData currentObject)
        {
            double minPrice = MinPrice, maxPrice = MaxPrice;

            return
                (((minPrice == -1.0) || (currentObject.Price >= minPrice)) && 
                ((maxPrice == -1.0) || (currentObject.Price <= maxPrice)));
        }

        private bool ApplyIssueYearFilter(ObjectData currentObject)
        {
            var years = YearsSet;
            return (years == null) || (years.Count == 0) || years.Contains(currentObject.Year ?? -1);
        }

        private bool ApplyKeywordsFilter(ObjectData currentObject)
        {
            var keywords = KeywordsRegex;
            return (keywords == null) || (keywords.IsMatch(currentObject.Name) || keywords.IsMatch(currentObject.Description));
        }
        #endregion
    }
}