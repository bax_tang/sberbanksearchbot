﻿using System;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class KeyedActivity : Activity
    {
        #region Fields and properties

        private string ActivityKey;
        #endregion

        #region Constructors

        public KeyedActivity() : base() { }
        #endregion

        #region Public class methods

        public string GetActivityKey()
        {
            if (string.IsNullOrEmpty(ActivityKey))
            {
                ActivityKey = string.Concat(From.Id, "|", Conversation.Id);
            }
            return ActivityKey;
        }
        #endregion
    }
}