﻿using System;
using System.Collections.Generic;

namespace SberbankSearchBot
{
    public class IssueYearsConfigurationItem : SearchConfigurationItem
    {
        #region Constants and fields

        public HashSet<int> YearsSet;
        #endregion

        #region Constructors

        public IssueYearsConfigurationItem() : base(BotConstants.YearActionCode, "Год выпуска", "любой")
        {
            YearsSet = new HashSet<int>();
        }
        #endregion

        #region SearchConfigurationItem methods overriding

        public override void Reset()
        {
            base.Reset();

            YearsSet.Clear();
        }

        public override string ToString()
        {
            string result = string.Empty;

            HashSet<int> yearsSet = YearsSet;
            if (yearsSet.Count > 0)
            {
                result = string.Concat(ItemName, ": ", string.Join("; ", YearsSet));
            }
            else
            {
                result = base.ToString();
            }

            return result;
        }
        #endregion
    }
}