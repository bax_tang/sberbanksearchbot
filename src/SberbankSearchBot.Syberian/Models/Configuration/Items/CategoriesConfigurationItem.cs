﻿using System;
using System.Collections.Generic;

namespace SberbankSearchBot
{
    public class CategoriesConfigurationItem : SearchConfigurationItem
    {
        #region Constants and fields

        public readonly HashSet<string> CategoriesSet;
        #endregion

        #region Constructors

        public CategoriesConfigurationItem() : base(BotConstants.CategoryActionCode, "Категория", "любая")
        {
            CategoriesSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        }
        #endregion

        #region SearchConfigurationItem methods overriding

        public override void Reset()
        {
            base.Reset();

            CategoriesSet.Clear();
        }

        public override string ToString()
        {
            string result = string.Empty;

            HashSet<string> categories = CategoriesSet;
            if (categories.Count > 0)
            {
                result = string.Concat(ItemName, ": ", string.Join("; ", CategoriesSet));
            }
            else
            {
                result = base.ToString();
            }

            return result;
        }
        #endregion
    }
}