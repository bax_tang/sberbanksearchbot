﻿using System;
using System.Collections.Generic;

namespace SberbankSearchBot
{
    public class CitiesConfigurationItem : SearchConfigurationItem
    {
        #region Constants and fields

        public HashSet<string> CitiesSet;
        #endregion

        #region Constructors

        public CitiesConfigurationItem() : base(BotConstants.CityActionCode, "Город", "любой")
        {
            CitiesSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        }
        #endregion

        #region SearchConfigurationItem methods overriding

        public override void Reset()
        {
            base.Reset();

            CitiesSet.Clear();
        }

        public override string ToString()
        {
            string result = string.Empty;

            HashSet<string> citiesSet = CitiesSet;
            if (citiesSet.Count > 0)
            {
                result = string.Concat(ItemName, ": ", string.Join("; ", CitiesSet));
            }
            else
            {
                result = base.ToString();
            }

            return result;
        }
        #endregion

        #region Public class methods

        public void Clear(bool newStatus)
        {
            Enabled = newStatus;
            CitiesSet.Clear();
        }
        #endregion
    }
}