﻿using System;

namespace SberbankSearchBot
{
    public abstract class SearchConfigurationItem
    {
        #region Constants and fields

        public readonly string ActionCode;

        public readonly string ItemName;

        public readonly string AnyHeader;

        public bool Enabled;
        #endregion

        #region Constructors

        private SearchConfigurationItem()
        {
            Enabled = false;
        }

        protected internal SearchConfigurationItem(string actionCode, string itemName, string anyHeader) : this()
        {
            ActionCode = actionCode;
            AnyHeader = anyHeader;
            ItemName = itemName;
        }
        #endregion

        #region Public class methods

        public virtual void Reset()
        {
            Enabled = false;
        }

        public override string ToString()
        {
            return string.Concat(ItemName, ": ", AnyHeader);
        }
        #endregion
    }
}