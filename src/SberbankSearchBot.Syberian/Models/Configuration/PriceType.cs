﻿using System;

namespace SberbankSearchBot
{
    public enum PriceType
    {
        Unknown = 0,
        MinPrice = 1,
        MaxPrice = 2
    }
}