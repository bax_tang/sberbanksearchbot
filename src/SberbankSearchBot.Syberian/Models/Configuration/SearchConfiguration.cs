﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SberbankSearchBot
{
    public class SearchConfiguration : IEnumerable<SearchConfigurationItem>
    {
        #region Constants, fields and properties

        private const int InitialCapacity = 7;

        private const string DisplayFormat = "Параметры поиска по разделу \"{0}\"";

        private List<SearchConfigurationItem> ItemsList;

        private Dictionary<string, SearchConfigurationItem> ItemsDictionary;

        public string SectionName = string.Empty;

        public readonly CategoriesConfigurationItem CategoriesItem;

        public readonly RegionConfigurationItem RegionItem;

        public readonly CitiesConfigurationItem CitiesItem;

        public readonly IssueYearsConfigurationItem IssueYearsItem;

        public readonly PricesConfigurationItem PricesItem;

        public readonly KeywordsConfigurationItem KeywordsItem;

        public int Count => ItemsList.Count;

        public SearchConfigurationItem this[int itemIndex] => ItemsList[itemIndex];

        public SearchConfigurationItem this[string itemCode]
        {
            get
            {
                SearchConfigurationItem item = null;
                ItemsDictionary.TryGetValue(itemCode, out item);
                return item;
            }
        }
        #endregion

        #region Constructors

        private SearchConfiguration(int capacity)
        {
            ItemsDictionary = new Dictionary<string, SearchConfigurationItem>(capacity, StringComparer.OrdinalIgnoreCase);
            ItemsList = new List<SearchConfigurationItem>(capacity);
        }

        public SearchConfiguration(string sectionName) : this(InitialCapacity)
        {
            SectionName = sectionName;

            CategoriesItem = AddItem(new CategoriesConfigurationItem());
            RegionItem = AddItem(new RegionConfigurationItem());
            CitiesItem = AddItem(new CitiesConfigurationItem());
            IssueYearsItem = AddItem(new IssueYearsConfigurationItem());
            PricesItem = AddItem(new PricesConfigurationItem());
            KeywordsItem = AddItem(new KeywordsConfigurationItem());
        }
        #endregion

        #region IEnumerable implementation

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ItemsList.GetEnumerator();
        }

        public IEnumerator<SearchConfigurationItem> GetEnumerator()
        {
            return ItemsList.GetEnumerator();
        }
        #endregion

        #region object methods overriding

        public override string ToString()
        {
            return string.Format(DisplayFormat, SectionName);
        }
        #endregion

        #region Public class methods

        public string GetParametersText()
        {
            StringBuilder parametersBuilder = new StringBuilder(128);

            List<SearchConfigurationItem> parameters = ItemsList;
            for (int index = 0; index < parameters.Count; ++index)
            {
                SearchConfigurationItem currentItem = parameters[index];
                if (currentItem.Enabled)
                {
                    parametersBuilder.AppendLine(currentItem.ToString());
                }
            }

            return parametersBuilder.ToString();
        }

        public void Reset(string sectionName)
        {
            SectionName = sectionName;

            ResetItems();
        }

        public void ResetItems()
        {
            for (int index = 0; index < ItemsList.Count; ++index)
            {
                ItemsList[index].Reset();
            }
        }

        public void ChangeItemStatus(string itemCode, bool newStatus)
        {
            SearchConfigurationItem item = this[itemCode];
            if (item != null)
            {
                item.Enabled = newStatus;
            }
        }
        #endregion

        #region Private class methods

        private TConfigurationItem AddItem<TConfigurationItem>(TConfigurationItem configurationItem) where TConfigurationItem : SearchConfigurationItem
        {
            ItemsDictionary[configurationItem.ActionCode] = configurationItem;
            ItemsList.Add(configurationItem);

            return configurationItem;
        }
        #endregion
    }
}