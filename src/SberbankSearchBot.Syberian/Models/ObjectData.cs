﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using Newtonsoft.Json;

namespace SberbankSearchBot
{
    public class ObjectData : IEquatable<ObjectData>
    {
        #region Constants and fields

        private static readonly Random rnd;

        public static readonly ObjectData Empty;

        public string ID { get; internal set; }

        public string Name { get; internal set; }

        public string Description { get; internal set; }

        public string URL { get; internal set; }

        public string Section { get; internal set; }

        public string Category { get; internal set; }

        public string Region { get; internal set; }

        public string City { get; internal set; }

        public double Price { get; internal set; }

        public int? Year { get; internal set; }

        public IList<string> Images { get; internal set; }
        #endregion

        #region Constructors

        static ObjectData()
        {
            ObjectData empty = new ObjectData
            {
                ID = string.Empty,
                Section = string.Empty,
                Category = string.Empty,
                Region = string.Empty,
                City = string.Empty,
                Name = string.Empty,
                Description = string.Empty,
                Price = 0.0D,
                Year = null,
                Images = new string[0]
            };
            Empty = empty;

            rnd = new Random();
        }

        internal ObjectData() { }
        #endregion

        #region object methods overriding

        public override bool Equals(object obj)
        {
            return Equals(obj as ObjectData);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.None);
        }
        #endregion

        #region Public class methods

        public static bool Equals(ObjectData left, ObjectData right)
        {
            return (left == right) || ((left != null) && (right != null) && left.Equals(right));
        }

        public bool Equals(ObjectData other)
        {
            return (other != null) && string.Equals(ID, other.ID, StringComparison.OrdinalIgnoreCase);
        }

        public string GetFirstImageUrl()
        {
            string imageUrl = Images?.FirstOrDefault();
            return imageUrl;
        }

        public string GetRandomImageUrl()
        {
            string imageUrl = null;

            IList<string> images = Images;
            if (images != null && images.Count > 0)
            {
                int imageIndex = rnd.Next(0, images.Count);
                imageUrl = images[imageIndex];
            }

            return imageUrl;
        }

        public string GetPriceText()
        {
            string priceText = null;

            double price = Price;
            if (price >= 1000000.0)
            {
                priceText = GetPriceText((price / 1000000.0), " млн. руб.");
            }
            else if (price >= 1000.0)
            {
                priceText = GetPriceText((price / 1000.0), " тыс. руб.");
            }
            else
            {
                priceText = GetPriceText(price, " руб.");
            }

            return priceText;
        }

        public string ToString(Formatting formatting)
        {
            return JsonConvert.SerializeObject(this, formatting);
        }
        #endregion

        #region Private class methods

        private static string GetPriceText(double priceValue, string priceAppender)
        {
            return string.Concat(priceValue.ToString("F0", CultureInfo.InvariantCulture), priceAppender);
        }
        #endregion
    }
}