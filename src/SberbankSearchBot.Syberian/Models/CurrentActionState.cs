﻿using System;

namespace SberbankSearchBot
{
    public enum CurrentActionState
    {
        Unknown = 0,
        WaitForMinPrice,
        WaitForMaxPrice,
        WaitForKeywords,
        WaitForSearchResultsCount
    }
}