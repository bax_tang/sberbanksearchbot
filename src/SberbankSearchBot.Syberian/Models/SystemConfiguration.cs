﻿using System;
using System.Text;

namespace SberbankSearchBot
{
    public class SystemConfiguration
    {
        #region Constants and fields

        private readonly string UserKey;

        /// <summary>
        /// Содержит значение, определяющее максимальное количество результатов выдачи при выполнении поиска объектов.
        /// Значение по умолчанию - 5.
        /// </summary>
        public int SearchResultsCount = BotConstants.MaxResultsCount;
        #endregion

        #region Constructors

        public SystemConfiguration(string userKey)
        {
            UserKey = userKey;
        }
        #endregion

        #region object methods overriding

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder(32);

            builder.Append("Параметры конфигурации для пользователя ");
            builder.AppendLine(UserKey);
            builder.Append("Количество результатов: ");
            builder.AppendLine(SearchResultsCount.ToString());

            return builder.ToString();
        }
        #endregion
    }
}