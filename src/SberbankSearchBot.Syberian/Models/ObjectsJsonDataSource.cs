﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace SberbankSearchBot
{
    public class ObjectsJsonDataSource : IObjectsDataSource
    {
        #region Constants and fields

        private readonly string DataFilePath;

        private Lazy<JArray> ObjectsStore = null;
        #endregion

        #region Constructors

        public ObjectsJsonDataSource(string dataFilePath)
        {
            if (string.IsNullOrEmpty(dataFilePath))
            {
                throw new ArgumentNullException(nameof(dataFilePath), "Objects data file path can't be null or empty.");
            }
            if (!File.Exists(dataFilePath))
            {
                throw new InvalidOperationException("Objects data file not exists.");
            }

            DataFilePath = dataFilePath;
            ObjectsStore = new Lazy<JArray>(LoadObjectsData, true);
        }
        #endregion

        #region IObjectsDataSource implementation

        public IList<string> GetObjectIdentifiers(string sectionName)
        {
            JArray objectsStore = ObjectsStore.Value;

            string expression = string.Format("$..[?(@.Section=='{0}')].ID", sectionName);

            var identifierTokens = objectsStore.SelectTokens(expression, false);
            var identifiers = identifierTokens.Select(TokenToString);

            return new List<string>(identifiers);
        }

        public ObjectData GetByID(string objectID)
        {
            JArray objectsStore = ObjectsStore.Value;
            
            string expression = string.Format("$..[?(@.ID=='{0}')]", objectID);
            JObject dataObject = objectsStore.SelectToken(expression, false) as JObject;

            ObjectData result = (dataObject != null) ? BuildFromJObject(dataObject) : ObjectData.Empty;
            return result;
        }

        public IList<string> GetSections()
        {
            JArray objectsStore = ObjectsStore.Value;

            var sectionTokens = objectsStore.SelectTokens("$..Section", false);
            var sections = sectionTokens.Select(TokenToString);

            HashSet<string> sectionsSet = new HashSet<string>(sections, StringComparer.OrdinalIgnoreCase);

            string[] sectionsArray = new string[sectionsSet.Count];
            sectionsSet.CopyTo(sectionsArray, 0, sectionsArray.Length);

            return sectionsArray;
        }

        public IList<string> GetCategories(string sectionName)
        {
            JArray objectsStore = ObjectsStore.Value;

            string expression = string.Format("$..[?(@.Section=='{0}')].Category", sectionName);

            var categoryTokens = objectsStore.SelectTokens(expression, false);
            var categories = categoryTokens.Select(TokenToString);

            HashSet<string> categoriesSet = new HashSet<string>(categories, StringComparer.OrdinalIgnoreCase);

            string[] categoriesArray = new string[categoriesSet.Count];
            categoriesSet.CopyTo(categoriesArray, 0, categoriesSet.Count);
            Array.Sort(categoriesArray, StringComparer.OrdinalIgnoreCase);

            return categoriesArray;
        }

        public IList<string> GetRegions(string sectionName)
        {
            JArray objectsStore = ObjectsStore.Value;

            string expression = string.Format("$..[?(@.Section=='{0}')].Region", sectionName);

            var regionTokens = objectsStore.SelectTokens(expression, false);
            var regions = regionTokens.Select(TokenToString);

            HashSet<string> regionsSet = new HashSet<string>(regions, StringComparer.OrdinalIgnoreCase);

            string[] regionsArray = new string[regionsSet.Count];
            regionsSet.CopyTo(regionsArray, 0, regionsSet.Count);
            Array.Sort(regionsArray, StringComparer.OrdinalIgnoreCase);

            return regionsArray;
        }

        public IList<string> GetCities(string sectionName, string regionName)
        {
            JArray objectsStore = ObjectsStore.Value;

            string expression = string.Format("$..[?(@.Section=='{0}')]", sectionName);

            var objectTokens = objectsStore.SelectTokens(expression, false);

            HashSet<string> citiesSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            
            foreach (JObject item in objectTokens.OfType<JObject>())
            {
                string itemRegion = (string)item["Region"];
                if (string.Equals(regionName, itemRegion, StringComparison.OrdinalIgnoreCase))
                {
                    citiesSet.Add((string)item["City"]);
                }
            }

            string[] citiesArray = new string[citiesSet.Count];
            citiesSet.CopyTo(citiesArray, 0, citiesSet.Count);
            Array.Sort(citiesArray, StringComparer.OrdinalIgnoreCase);

            return citiesArray;
        }

        public IList<int> GetTransportIssueYears()
        {
            JArray objectsStore = ObjectsStore.Value;

            string expression = "$..Year";

            var fieldTokens = objectsStore.SelectTokens(expression, false);

            HashSet<int> yearsSet = new HashSet<int>();
            foreach (JToken fieldToken in fieldTokens)
            {
                int year = (int)fieldToken;
                yearsSet.Add(year);
            }

            int[] yearsArray = new int[yearsSet.Count];
            yearsSet.CopyTo(yearsArray, 0, yearsSet.Count);
            Array.Sort(yearsArray);

            return yearsArray;
        }
        #endregion

        #region Private class methods

        private static ObjectData BuildFromJObject(JObject source)
        {
            ObjectData result = new ObjectData
            {
                ID = (string)source["ID"],
                Section = (string)source["Section"],
                Category = (string)source["Category"] ?? string.Empty,
                Region = (string)source["Region"] ?? string.Empty,
                City = (string)source["City"] ?? string.Empty,
                Name = (string)source["Name"],
                Description = (string)source["Description"],
                URL = (string)source["URL"],
                Price = ((double?)source["Price"]).GetValueOrDefault(),
                Year = (int?)source["Year"]
            };

            JArray imagesArray = source["Images"] as JArray;
            if (imagesArray != null)
            {
                int count = imagesArray.Count;
                string[] images = new string[count];
                for (int index = 0; index < count; ++index)
                {
                    images[index] = (string)imagesArray[index];
                }
                result.Images = images;
            }

            return result;
        }

        private static string TokenToString(JToken token)
        {
            string result = null;

            JValue valueToken = token as JValue;
            if (valueToken != null)
            {
                result = valueToken.ToString();
            }
            else
            {
                JProperty propertyToken = token as JProperty;
                if (propertyToken != null)
                {
                    valueToken = propertyToken.Value as JValue;
                    if (valueToken != null)
                    {
                        result = valueToken.ToString();
                    }
                }
            }

            return result;
        }

        private JArray LoadObjectsData()
        {
            JArray result = null;

            using (StreamReader reader = new StreamReader(DataFilePath, Encoding.UTF8))
            {
                result = JArray.Load(new JsonTextReader(reader));
            }

            return result;
        }
        #endregion
    }
}