﻿using System;

namespace SberbankSearchBot
{
    public static class BotConstants
    {
        #region Constants and fields

        public const int MaxResultsCount = 5; // 5 search results by default

        public const int MinDelayInterval = 300000; // 300 seconds <==> 5 minutes

        public const int MaxDelayInterval = 600000; // 600 seconds <==> 10 minutes

        public const string BackActionCode = "back";

        public const string BackActionTitle = "Назад";

        public const string ForwardActionCode = "forward";

        public const string ForwardActionTitle = "Далее";

        public const string BookActionCode = "book";

        public const string BookActionTitle = "Забронировать";

        public const string BookActionText = "Лот забронирован, наш оператор свяжется с Вами в течение 10 минут";

        public const string GreetingActionCode = "start";

        public const string GreetingActionCodeWithoutHeader = "start -h";

        public const string GreetingActionTitle = "Главное меню";

        public const string GreetingCardTitle = "Здравствуйте!";

        public const string GreetingCardText = @"Вас приветствует чат-бот уведомления о поступлении реализации недвижимости Западно-Сибирского банка ПАО ""Сбербанк"".

Вы можете управлять этим ботом с помощью следующих команд:";

        public const string GreetingCardTextNoHeader = @"Чат-бот уведомления о поступлении реализации недвижимости Западно-Сибирского банка ПАО ""Сбербанк"".

Вы можете управлять этим ботом с помощью следующих команд:";

        public const string SearchActionCode = "search";

        public const string SearchActionTitle = "Поиск";

        public const string SearchCardTitle = "Поиск";

        public const string SearchCardText = "Пожалуйста, выберите интересующий Вас раздел или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string SectionActionCode = "section";

        public const string SectionCardTitle = "Категории раздела \"{0}\"";

        public const string SectionCardText = "Пожалуйста, выберите интересующие Вас категории или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string CategoryActionCode = "category";

        public const string CategoryCardTitle = "Выбор категорий";

        public const string CategoryCardText = "Пожалуйста, выберите интересующие Вас категории или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string RegionActionCode = "region";

        public const string RegionActionAnyValue = "region set any";

        public const string RegionActionTitle = "Выбор региона";

        public const string RegionCardTitleFormat = "Регионы по разделу \"{0}\"";

        public const string RegionCardText = "Пожалуйста, выберите интересующий Вас регион или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string FavoritesReplyMessageText = "Объекты, добавленные в избранное";

        public const string NoFavoritesMessageText = @"Вы ещё не добавили ни одного объекта в избранное.
Пожалуйста, воспользуйтесь кнопкой ""Поиск"" для поиска предложений или кнопкой ""Назад"" для возврата в главное меню.";

        public const string AddToFavoritesActionCode = "add_to_favorites";

        public const string AddToFavoritesActionTitle = "В избранное";

        public const string AddToFavoritesSucceeded = @"Объект успешно добавлен в избранное.
Вы можете воспользоваться кнопкой ""Перейти в избранное"" для просмотра добавленных объектов.";

        public const string AddToFavoritesFailed = @"Объект уже добавлен в избранное.
Вы можете воспользоваться кнопкой ""Перейти в избранное"" для просмотра добавленных объектов.";

        public const string RemoveFromFavoritesActionCode = "remove_from_favorites";

        public const string RemoveFromFavoritesActionTitle = "Удалить";

        public const string RemoveFromFavoritesSucceeded = @"Объект успешно удалён из избранного.
Вы можете воспользоваться кнопкой ""Назад"" для возврата в предыдущее меню или кнопкой ""Главное меню"" для выхода.";

        public const string RemoveFromFavoritesFailed = @"Объект уже удалён из избранного.
Вы можете воспользоваться кнопкой ""Назад"" для возврата в предыдущее меню или кнопкой ""Главное меню"" для выхода.";

        public const string ManageFavoritesActionCode = "manage_favorites";

        public const string ManageFavoritesActionTitle = "Избранное";

        public const string ManageFavoritesActionTitleAfterAdding = "В избранное";

        public const string ManageSubscriptionsActionCode = "manage_subscriptions";

        public const string ManageSubscriptionsActionTitle = "Подписки";

        public const string ManageSubscriptionsCardText = "Чтобы отказаться от подписки, нажмите на соответствующую ей кнопку или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string ClearSubscriptionsActionCode = "clear_subscriptions";

        public const string ClearSubscriptionsActionTitle = "Удалить все";

        public const string ClearSubscriptionsCardText = "Все подписки были удалены. Пожалуйста, нажмите кнопку \"Назад\" для возврата в главное меню.";

        public const string AddSubscriptionActionCode = "add_subscription";

        public const string AddSubscriptionActionTitle = "Подписаться";

        public const string AddSubscriptionSucceeded = "Подписка успешно создана. Пожалуйста, нажмите кнопку \"Назад\" для возврата в главное меню.";

        public const string AddSubscriptionFailed = "При создании подписки произошла ошибка. Пожалуйста, нажмите кнопку \"Назад\" для возврата в главное меню.";

        public const string ViewSubscriptionsActionCode = "view_subscriptions";

        public const string ViewSubscriptionsActionTitle = "Посмотреть подписки";

        public const string NoSubscriptionsMessageText = @"Вы ещё не создали ни одной подписки.
Пожалуйста, воспользуйтесь кнопкой ""Поиск"" для поиска предложений или кнопкой ""Назад"" для возврата в главное меню.";

        public const string RemoveSubscriptionActionCode = "remove_subscription";

        public const string RemoveSubscriptionActionTitle = "Отказаться от подписки";

        public const string RemoveSubscriptionSucceeded = "Подписка успешно удалена. Для возврата в главное меню воспользуйтесь кнопкой \"Назад\".";

        public const string RemoveSubscriptionFailed = "Подписка не удалена. Для возврата в главное меню воспользуйтесь кнопкой \"Назад\".";

        public const string AreaActionCode = "area";

        public const string KeywordsActionCode = "keywords";

        public const string KeywordsActionText = "Пожалуйста, укажите ключевые слова для поиска, разделяя их пробелами или запятыми, или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string SearchCriteriaCardTitle = "Критерии поиска по разделу \"{0}\"";

        public const string SearchCriteriaCardText = "Пожалуйста, укажите интересующие Вас критерии или нажмите кнопку \"Выполнить поиск\" для поиска предложений по текущим критериям.";
        
        public const string CityActionCode = "city";

        public const string CityActionTitleFormat = "Города региона {0}";

        public const string CityActionText = "Пожалуйста, выберите интересующие Вас города или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string PriceActionCode = "price";

        public const string PriceActionTitle = "Диапазон цен";

        public const string PriceActionText = "Пожалуйста, укажите интересующий Вас диапазон цен или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string PriceAnyMinActionTitle = "Минимальная цена: любая";

        public const string PriceAnyMaxActionTitle = "Максимальная цена: любая";

        public const string PriceMinActionTitle = "Минимальная цена";

        public const string PriceMinActionText = "Пожалуйста, выберите значение минимальной цены из списка или нажмите кнопку \"Своя\" для ввода собственного значения.";

        public const string PriceMaxActionTitle = "Максимальная цена";

        public const string PriceMaxActionText = "Пожалуйста, выберите значение максимальной цены из списка или нажмите кнопку \"Своя\" для ввода собственного значения.";

        public const string PriceMinOwnText = "Пожалуйста, укажите в ответном сообщении собственное значение минимальной цены в рублях или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string PriceMaxOwnText = "Пожалуйста, укажите в ответном сообщении собственное значение максимальной цены в рублях или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string PriceIncorrectOwnValue = "Вы ввели некорректное значение цены. Пожалуйста, повторите ввод.";

        public const string YearActionCode = "year";

        public const string YearCardTitle = "Год выпуска";

        public const string YearCardText = "Пожалуйста, выберите интересующие Вас годы выпуска или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string DoSearchActionCode = "do_search";

        public const string DoSearchActionTitle = "Выполнить поиск";

        public const string RetrieveNextActionCode = "retrieve_next";

        public const string RetrieveNextActionTitle = "Показать ещё";

        public const string SearchResultsMessageText = "Результаты поиска (найдено {0}):";

        public const string OpenUrlActionTitle = "Перейти к лоту";

        public const string NewResultsFoundedMessageText = "Найдены новые поступления";

        public const string SubscriptionCardTitle = "Доступные действия";

        public const string SubscriptionCardTitleFormat = "Подписка по разделу \"{0}\"";

        public const string CreatedSubscriptionsMessageText = "Созданные подписки";

        public const string NoResultsMessageText = "Отсутствуют лоты, удовлетворяющие указанным условиям поиска.";

        public const string SystemSettingsActionCode = "manage_system_settings";

        public const string SystemSettingsActionTitle = "Настройки";

        public const string SystemSettingsCardTitle = "Системные настройки";

        public const string SystemSettingsCardText = "Пожалуйста, выберите соответствующую настройку для изменения или нажмите кнопку \"Назад\" для возврата в главное меню.";

        public const string ResultsCountActionCode = "search_results_count";

        public const string ResultsCountActionTitle = "Количество результатов";

        public const string ResultsCountMessageText = "Пожалуйста, укажите в ответном сообщении собственное значение количества результатов поиска или нажмите кнопку \"Назад\" для возврата в предыдущее меню.";

        public const string ResultsCountChangingSucceeded = "Новое значение количества результатов поиска принято.";

        public const string ResultsCountChangingFailed = "Вы ввели некорректное значение количества результатов поиска. Пожалуйста, повторите ввод.";
        #endregion

        #region Constructors

        static BotConstants() { }
        #endregion
    }
}