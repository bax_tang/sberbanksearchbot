﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace SberbankSearchBot
{
    public static class RegularExpressions
    {
        #region Constants and fields

        [ThreadStatic]
        private static Regex DecimalRegex;
        #endregion

        #region Public class methods

        public static Regex GetDecimalRegex()
        {
            Regex decimalRegex = DecimalRegex;
            if (decimalRegex == null)
            {
                decimalRegex = new Regex("\\d+", RegexOptions.IgnoreCase | RegexOptions.Singleline);

                Interlocked.CompareExchange(ref DecimalRegex, decimalRegex, null);
            }
            return decimalRegex;
        }
        #endregion
    }
}