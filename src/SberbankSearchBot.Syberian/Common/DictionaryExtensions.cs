﻿using System;
using System.Collections.Generic;

namespace SberbankSearchBot
{
    public static class DictionaryExtensions
    {
        #region Public class methods

        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue addValue)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException(nameof(dictionary), "Dictionary can't be null.");
            }
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key), "Key can't be null.");
            }

            TValue result = default(TValue);
            bool exists = dictionary.TryGetValue(key, out result);
            if (!exists)
            {
                dictionary.Add(key, addValue);
                result = addValue;
            }
            return result;
        }

        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TKey, TValue> addValueFactory)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException(nameof(dictionary), "Dictionary can't be null.");
            }
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key), "Key can't be null.");
            }
            if (addValueFactory == null)
            {
                throw new ArgumentNullException(nameof(addValueFactory), "Value factory can't be null.");
            }

            TValue result = default(TValue);
            bool exists = dictionary.TryGetValue(key, out result);
            if (!exists)
            {
                result = addValueFactory.Invoke(key);
                dictionary.Add(key, result);
            }
            return result;
        }

        public static TValue AddOrUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException(nameof(dictionary), "Dictionary can't be null.");
            }
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key), "Key can't be null.");
            }
            if (updateValueFactory == null)
            {
                throw new ArgumentNullException(nameof(updateValueFactory), "Update factory can't be null.");
            }

            TValue result = default(TValue);
            bool exists = dictionary.TryGetValue(key, out result);
            if (exists)
            {
                TValue newValue = updateValueFactory.Invoke(key, result);
                dictionary[key] = newValue;
                result = newValue;
            }
            else
            {
                dictionary.Add(key, addValue);
                result = addValue;
            }
            return result;
        }

        public static bool TryRemove<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, out TValue value)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException(nameof(dictionary), "Dictionary can't be null.");
            }
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key), "Key can't be null.");
            }

            TValue result = default(TValue);
            bool success = dictionary.TryGetValue(key, out result) && dictionary.Remove(key);
            value = result;
            return success;
        }

        public static TValue TryRemove<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException(nameof(dictionary), "Dictionary can't be null.");
            }
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key), "Key can't be null.");
            }

            TValue result = default(TValue);
            dictionary.TryRemove(key, out result);
            return result;
        }
        #endregion
    }
}