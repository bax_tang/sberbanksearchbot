﻿using System;
using System.Web.Http;

namespace SberbankSearchBot
{
    public class MaintenanceController : ApiController
    {
        #region Constants and fields

        private ObjectsDataRepository DataRepository;
        #endregion

        #region Constructors

        public MaintenanceController(ObjectsDataRepository dataRepository)
        {
            if (dataRepository == null)
            {
                throw new ArgumentNullException(nameof(dataRepository), "Objects data repository can't be null.");
            }

            DataRepository = dataRepository;
        }
        #endregion

        #region Public class methods

        [HttpPost]
        public IHttpActionResult Clear()
        {
            DataRepository.Clear();
            return Ok();
        }
        #endregion
    }
}