﻿using System;
using System.Web.Http;

using Autofac;
using Autofac.Integration.WebApi;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace SberbankSearchBot
{
	public static class WebApiConfig
	{
		public static void Configure(HttpConfiguration config, IContainer container)
		{
			// Json settings
			JsonSerializerSettings settings = config.Formatters.JsonFormatter.SerializerSettings;
			settings.NullValueHandling = NullValueHandling.Ignore;
			settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			settings.Formatting = Formatting.Indented;

			JsonConvert.DefaultSettings = GetDefaultSettings;

			// Web API routes
			config.MapHttpAttributeRoutes();
			config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{action}/{id}", new { id = RouteParameter.Optional });

			container.Resolve<MessagesRepository>().PreloadMessages();
		}

		internal static JsonSerializerSettings GetDefaultSettings()
		{
			IContractResolver resolver = new CamelCasePropertyNamesContractResolver();

			JsonSerializerSettings settings = new JsonSerializerSettings()
			{
				ContractResolver = resolver,
				Formatting = Formatting.Indented,
				NullValueHandling = NullValueHandling.Ignore,
			};

			return settings;
		}
	}
}
