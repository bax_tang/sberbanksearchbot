﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace SberbankSearchBot
{
    public class SystemConfigurationRepository
    {
        #region Constants and fields

        private readonly ConcurrentDictionary<string, SystemConfiguration> ConfigurationStore;
        #endregion

        #region Constructors

        public SystemConfigurationRepository()
        {
            ConfigurationStore = new ConcurrentDictionary<string, SystemConfiguration>(StringComparer.OrdinalIgnoreCase);
        }
        #endregion

        #region Public class methods

        public SystemConfiguration GetConfiguration(string activityKey)
        {
            return ConfigurationStore.GetOrAdd(activityKey, CreateNewConfiguration);
        }
        #endregion

        #region Private class methods

        private static SystemConfiguration CreateNewConfiguration(string activityKey)
        {
            return new SystemConfiguration(activityKey);
        }
        #endregion
    }
}