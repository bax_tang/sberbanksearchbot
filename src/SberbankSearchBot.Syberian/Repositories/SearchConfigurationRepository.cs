﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Microsoft.Bot.Connector;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class SearchConfigurationRepository
    {
        #region Constants and fields

        private readonly string ConfigurationFilePath;

        private Lazy<JObject> ConfigurationStore;

        private ConcurrentDictionary<string, SearchConfiguration> SearchConfigurations;
        #endregion

        #region Constructors

        private SearchConfigurationRepository()
        {
            SearchConfigurations = new ConcurrentDictionary<string, SearchConfiguration>(StringComparer.OrdinalIgnoreCase);
        }

        public SearchConfigurationRepository(string configurationFilePath) : this()
        {
            if (string.IsNullOrEmpty(configurationFilePath))
            {
                throw new ArgumentNullException(nameof(configurationFilePath), "The configuration file path can't be null or empty.");
            }
            if (!File.Exists(configurationFilePath))
            {
                throw new InvalidOperationException("Configuration file doesn't exists.");
            }

            ConfigurationFilePath = configurationFilePath;
            ConfigurationStore = new Lazy<JObject>(GetInnerStore, true);
        }
        #endregion

        #region Public class methods

        public void SetConfiguration(string activityKey, SearchConfiguration configuration)
        {
            SearchConfigurations[activityKey] = configuration;
        }

        public SearchConfiguration GetOrCreateConfiguration(string activityKey, string sectionName)
        {
            SearchConfiguration configuration = null;

            bool exists = SearchConfigurations.TryGetValue(activityKey, out configuration);
            if (!exists || (0 != string.Compare(sectionName, configuration.SectionName, StringComparison.OrdinalIgnoreCase)))
            {
                JObject configStore = ConfigurationStore.Value;
                JArray configArray = configStore[sectionName] as JArray;
                
                if (configuration != null)
                {
                    configuration.Reset(sectionName);
                }
                else
                {
                    configuration = new SearchConfiguration(sectionName);
                }

                for (int index = 0; index < configArray.Count; ++index)
                {
                    string itemCode = (string)configArray[index];

                    configuration.ChangeItemStatus(itemCode, true);
                }
                   
                SearchConfigurations[activityKey] = configuration;
            }

            return configuration;
        }

        public SearchConfiguration GetConfiguration(string activityKey)
        {
            SearchConfiguration configuration = null;
            SearchConfigurations.TryGetValue(activityKey, out configuration);
            return configuration;
        }

        public void RemoveConfigurationIfExists(string activityKey)
        {
            SearchConfiguration configuration = null;
            if (SearchConfigurations.TryRemove(activityKey, out configuration))
            {
                configuration.ResetItems();
                configuration = null;
            }
        }

        public bool TryRemoveConfiguration(string activityKey, out SearchConfiguration configuration)
        {
            return SearchConfigurations.TryRemove(activityKey, out configuration);
        }
        #endregion

        #region Private class methods

        private JObject GetInnerStore()
        {
            JObject result = null;

            JsonLoadSettings loadSettings = new JsonLoadSettings
            {
                CommentHandling = CommentHandling.Ignore,
                LineInfoHandling = LineInfoHandling.Ignore
            };

            using (StreamReader reader = new StreamReader(ConfigurationFilePath, Encoding.UTF8))
            {
                result = JObject.Load(new JsonTextReader(reader), loadSettings);
            }

            return result;
        }
        #endregion
    }
}