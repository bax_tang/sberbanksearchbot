﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SberbankSearchBot
{
    public class MessagesRepository
    {
        #region Fields and properties

        private readonly BotConfiguration Configuration;

        private readonly SqlConnection Connection;

        private ConcurrentDictionary<string, string> MessagesStore;
        #endregion

        #region Constructors

        public MessagesRepository(BotConfiguration configuration, SqlConnection connection)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration), "Configuration can't be null.");
            }
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection), "Database connection can't be null.");
            }

            Configuration = configuration;
            Connection = connection;
            MessagesStore = new ConcurrentDictionary<string, string>();
        }
        #endregion

        #region Public class methods

        public void PreloadMessages()
        {
            PreloadMessagesToStore(MessagesStore);
        }

        public string GetMessage(string messageKey)
        {
            if (string.IsNullOrEmpty(messageKey))
            {
                throw new ArgumentNullException(nameof(messageKey), "Message key can't be null or empty.");
            }

            return MessagesStore.GetOrAdd(messageKey, GetMessageFromDatabase);
        }
        #endregion

        #region Private class methods

        private void PreloadMessagesToStore(IDictionary<string, string> store)
        {
            Guid departmentID = Configuration.Department;

            using (SqlCommand selectCommand = new SqlCommand("[dbo].[sp_bot_message_get_by_dep]", Connection)
            {
                CommandTimeout = 300,
                CommandType = CommandType.StoredProcedure
            })
            {
                selectCommand.Parameters.AddWithValue("DepartmentID", departmentID);

                using (SqlDataReader reader = selectCommand.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        int keyOrdinal = reader.GetOrdinal("Key");
                        int textOrdinal = reader.GetOrdinal("Text");

                        do
                        {
                            string key = reader.GetString(keyOrdinal);
                            string text = reader.GetString(textOrdinal);

                            store[key] = text;
                        }
                        while (reader.Read());
                    }
                }
            }
        }

        private string GetMessageFromDatabase(string messageKey)
        {
            Guid departmentID = Configuration.Department;

            string result = string.Empty;

            using (SqlCommand selectCommand = new SqlCommand("[dbo].[fn_bot_message_get_by_key]", Connection)
            {
                CommandTimeout = 300,
                CommandType = CommandType.StoredProcedure
            })
            {
                selectCommand.Parameters.AddWithValue("DepartmentID", departmentID);
                selectCommand.Parameters.AddWithValue("MessageKey", messageKey);

                SqlParameter textParameter = selectCommand.Parameters.Add("MessageText", SqlDbType.NVarChar);
                textParameter.Direction = ParameterDirection.ReturnValue;

                selectCommand.ExecuteNonQuery();

                result = Convert.ToString(textParameter.Value);
            }

            return result;
        }
        #endregion
    }
}