﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

using Microsoft.Bot.Connector;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class SearchSubscriptionsRepository
    {
        #region Constants and fields

        private readonly BotActionsRepository ParentRepository;

        private ConcurrentDictionary<string, List<SearchSubscription>> SubscriptionStore;

        private ConcurrentDictionary<string, HashSet<string>> FoundedItemsStore;

        private ConcurrentDictionary<string, ObjectsDataSearchResults> SearchResultsStore;
        #endregion

        #region Constructors

        private SearchSubscriptionsRepository()
        {
            FoundedItemsStore = new ConcurrentDictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);
            SearchResultsStore = new ConcurrentDictionary<string, ObjectsDataSearchResults>(StringComparer.OrdinalIgnoreCase);
            SubscriptionStore = new ConcurrentDictionary<string, List<SearchSubscription>>(StringComparer.OrdinalIgnoreCase);
        }

        public SearchSubscriptionsRepository(BotActionsRepository parentRepository) : this()
        {
            if (parentRepository == null)
            {
                throw new ArgumentNullException(nameof(parentRepository), "Parent repository can't be null.");
            }

            ParentRepository = parentRepository;
        }
        #endregion

        #region Public class methods

        public HashSet<string> GetFoundedItems(string activityKey)
        {
            HashSet<string> foundedItems = null;
            bool exists = FoundedItemsStore.TryGetValue(activityKey, out foundedItems);
            return (exists) ? foundedItems : new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        }

        public ObjectsDataSearchResults GetSearchResult(string activityKey)
        {
            ObjectsDataSearchResults searchResult = null;
            bool exists = SearchResultsStore.TryGetValue(activityKey, out searchResult);
            return (exists) ? searchResult : new ObjectsDataSearchResults();
        }

        public void SetSearchResults(string activityKey, HashSet<string> foundedItems, ObjectsDataSearchResults searchResult)
        {
            FoundedItemsStore[activityKey] = foundedItems;
            SearchResultsStore[activityKey] = searchResult;
        }

        public void SetSearchResults(string activityKey, HashSet<string> foundedItems)
        {
            FoundedItemsStore[activityKey] = foundedItems;
        }

        public void RemoveResultsIfExists(string activityKey)
        {
            HashSet<string> foundedItems = null;
            if (FoundedItemsStore.TryRemove(activityKey, out foundedItems))
            {
                foundedItems.Clear();
            }

            ObjectsDataSearchResults searchResult = null;
            SearchResultsStore.TryRemove(activityKey, out searchResult);
        }

        public void AddSubscription(string activityKey, SearchSubscription subscription)
        {
            List<SearchSubscription> subscriptions = SubscriptionStore.GetOrAdd(activityKey, CreateEmptySubscriptionsList);

            HashSet<string> foundedItems;
            bool isRemoved = FoundedItemsStore.TryRemove(activityKey, out foundedItems);
            subscription.SearchResults = (isRemoved) ? foundedItems : new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            subscription.Execute += new EventHandler(ExecuteSubscription);
            subscription.Start();

            subscriptions.Add(subscription);
        }

        public IReadOnlyList<SearchSubscription> GetSubscriptions(string activityKey)
        {
            List<SearchSubscription> subscriptions = SubscriptionStore.GetOrAdd(activityKey, CreateEmptySubscriptionsList);

            return subscriptions;
        }

        public IReadOnlyDictionary<string, SubscriptionInfo[]> GetSubscriptionInfos()
        {
            Dictionary<string, SubscriptionInfo[]> subscriptionInfos = new Dictionary<string, SubscriptionInfo[]>(4);

            FillSubscriptionInfos(subscriptionInfos);

            return subscriptionInfos;
        }

        public IReadOnlyList<SubscriptionInfo> GetSubscriptionInfos(string subscriberKey)
        {
            SubscriptionInfo[] subscriptionInfos = null;

            FillSubscriptionInfos(subscriberKey, out subscriptionInfos);

            return subscriptionInfos;
        }

        public void RemoveSubscription(string activityKey, int subscriptionIndex)
        {
            List<SearchSubscription> subscriptions = SubscriptionStore.GetOrAdd(activityKey, CreateEmptySubscriptionsList);

            SearchSubscription current = subscriptions[subscriptionIndex];

            current.Cancel();

            subscriptions.RemoveAt(subscriptionIndex);
        }

        public void ClearSubscriptions(string activityKey)
        {
            List<SearchSubscription> subscriptions = SubscriptionStore.GetOrAdd(activityKey, CreateEmptySubscriptionsList);
            if (subscriptions.Count > 0)
            {
                for (int index = 0; index < subscriptions.Count; ++index)
                {
                    subscriptions[index].Cancel();
                }
                subscriptions.Clear();
            }
        }
        #endregion

        #region Private class methods

        private void ExecuteSubscription(object sender, EventArgs args)
        {
            SearchSubscription subscription = sender as SearchSubscription;

            HashSet<string> searchResults = subscription.SearchResults;
            SearchConfiguration configuration = subscription.Configuration;
            SystemConfiguration systemConfig = ParentRepository.SystemConfiguration.GetConfiguration(subscription.ActivityKey);

            int count = 0, maxCount = systemConfig.SearchResultsCount;
            HashSet<string> newResults = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            var foundedObjects = ParentRepository.ObjectsData.SearchObjects(configuration);

            Activity replyActivity = null;

            foreach (ObjectData foundedObject in foundedObjects)
            {
                if (searchResults.Contains(foundedObject.ID)) continue;
                else
                {
                    if (replyActivity == null)
                    {
                        replyActivity = subscription.InitialActivity.CreateReply(BotConstants.NewResultsFoundedMessageText, "ru");
                    }

                    newResults.Add(foundedObject.ID);

                    HeroCard objectCard = HeroCardSource.CreateObjectCard(foundedObject);
                    replyActivity.Attachments.Add(objectCard.ToAttachment());

                    if (++count == maxCount) break;
                }
            }

            if (newResults.Count > 0)
            {
                searchResults.UnionWith(newResults);
            }

            if (replyActivity != null)
            {
                subscription.Connector.Conversations.ReplyToActivity(replyActivity);
            }
        }

        private void FillSubscriptionInfos(Dictionary<string, SubscriptionInfo[]> subscriptionInfos)
        {
            foreach (var item in SubscriptionStore)
            {
                string subscriberKey = item.Key;
                List<SearchSubscription> subscriptions = item.Value;

                SubscriptionInfo[] subscriptionInfoArray = null;
                if (subscriptions.Count > 0)
                {
                    subscriptionInfoArray = new SubscriptionInfo[subscriptions.Count];
                    for (int index = 0; index < subscriptions.Count; ++index)
                    {
                        SearchSubscription current = subscriptions[index];

                        SubscriptionInfo info = new SubscriptionInfo
                        {
                            SectionName = current.Configuration.SectionName,
                            Parameters = current.Configuration.GetParametersText()
                        };

                        subscriptionInfoArray[index] = info;
                    }
                }
                else subscriptionInfoArray = new SubscriptionInfo[0];

                subscriptionInfos.Add(subscriberKey, subscriptionInfoArray);
            }
        }

        private void FillSubscriptionInfos(string subscriberKey, out SubscriptionInfo[] subscriptionInfos)
        {
            List<SearchSubscription> subscriptions = null;
            bool exists = SubscriptionStore.TryGetValue(subscriberKey, out subscriptions);
            if (exists)
            {
                if (subscriptions.Count > 0)
                {
                    var subscriptionInfosArray = new SubscriptionInfo[subscriptions.Count];

                    for (int index = 0; index < subscriptions.Count; ++index)
                    {
                        SearchSubscription current = subscriptions[index];

                        SubscriptionInfo info = new SubscriptionInfo
                        {
                            SectionName = current.Configuration.SectionName,
                            Parameters = current.Configuration.GetParametersText()
                        };

                        subscriptionInfosArray[index] = info;
                    }

                    subscriptionInfos = subscriptionInfosArray;
                    return;
                }
            }

            subscriptionInfos = new SubscriptionInfo[0];
        }

        private static List<SearchSubscription> CreateEmptySubscriptionsList(string activityKey)
        {
            return new List<SearchSubscription>(4);
        }
        #endregion
    }
}