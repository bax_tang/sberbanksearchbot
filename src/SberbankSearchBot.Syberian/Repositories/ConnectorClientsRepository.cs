﻿using System;
using System.Collections.Concurrent;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class ConnectorClientsRepository
    {
        #region Constants and fields

        private readonly ConcurrentDictionary<string, ConnectorClient> Connectors;
        #endregion

        #region Constructors

        public ConnectorClientsRepository()
        {
            Connectors = new ConcurrentDictionary<string, ConnectorClient>(StringComparer.OrdinalIgnoreCase);
        }
        #endregion

        #region Public class methods

        public ConnectorClient GetConnector(KeyedActivity userActivity)
        {
            if (userActivity == null)
            {
                throw new ArgumentNullException(nameof(userActivity), "User activity can't be null.");
            }

            string activityKey = userActivity.GetActivityKey();
            return GetOrCreateConnector(activityKey, userActivity);
        }
        #endregion

        #region Private class methods

        private ConnectorClient GetOrCreateConnector(string activityKey, Activity userActivity)
        {
            ConnectorClient connector = null;
            bool exists = Connectors.TryGetValue(activityKey, out connector);
            if (!exists)
            {
                Uri serviceUri = new Uri(userActivity.ServiceUrl, UriKind.RelativeOrAbsolute);
                connector = new ConnectorClient(serviceUri);
                Connectors.TryAdd(activityKey, connector);
            }
            return connector;
        }
        #endregion
    }
}