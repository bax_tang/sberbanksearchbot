﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace SberbankSearchBot
{
    public class UserFavoritesRepository
    {
        #region Constants and fields

        private readonly ConcurrentDictionary<string, HashSet<string>> FavoritesStore;

        private ObjectsDataRepository DataRepository;
        #endregion

        #region Constructors
        
        public UserFavoritesRepository()
        {
            FavoritesStore = new ConcurrentDictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);
        }
        #endregion

        #region Public class methods

        public bool AddToFavorites(string activityKey, string objectID)
        {
            HashSet<string> favorites = FavoritesStore.GetOrAdd(activityKey, CreateEmptyFavoritesSet);
            
            return favorites.Add(objectID);
        }

        public bool RemoveFromFavorites(string activityKey, string objectID)
        {
            HashSet<string> favorites = FavoritesStore.GetOrAdd(activityKey, CreateEmptyFavoritesSet);

            return favorites.Remove(objectID);
        }

        public ICollection<string> GetFavorites(string activityKey)
        {
            HashSet<string> favorites = FavoritesStore.GetOrAdd(activityKey, CreateEmptyFavoritesSet);

            return favorites;
        }
        #endregion

        #region Private class methods

        private static HashSet<string> CreateEmptyFavoritesSet(string activityKey)
        {
            return new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        }
        #endregion
    }
}