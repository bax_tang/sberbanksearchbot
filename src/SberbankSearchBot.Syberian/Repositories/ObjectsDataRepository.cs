﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace SberbankSearchBot
{
    public class ObjectsDataRepository
    {
        #region Constants and fields

        private readonly ConcurrentDictionary<string, IList<string>> CategoriesStore;

        private readonly ConcurrentDictionary<string, IList<string>> RegionsStore;

        private readonly ConcurrentDictionary<Tuple<string, string>, IList<string>> CitiesStore;

        private readonly ConcurrentDictionary<string, ObjectData> ObjectStore;

        public readonly IObjectsDataSource DataSource;
        #endregion

        #region Constructors
        
        public ObjectsDataRepository(IObjectsDataSource dataSource)
        {
            if (dataSource == null)
            {
                throw new ArgumentNullException(nameof(dataSource), "Objects data source can't be null.");
            }

            DataSource = dataSource;

            CategoriesStore = new ConcurrentDictionary<string, IList<string>>(StringComparer.OrdinalIgnoreCase);
            RegionsStore = new ConcurrentDictionary<string, IList<string>>(StringComparer.OrdinalIgnoreCase);
            CitiesStore = new ConcurrentDictionary<Tuple<string, string>, IList<string>>();
            ObjectStore = new ConcurrentDictionary<string, ObjectData>(StringComparer.OrdinalIgnoreCase);
        }
        #endregion

        #region Public class methods

        public void Clear()
        {
            CategoriesStore.Clear();
            RegionsStore.Clear();
            CitiesStore.Clear();
            ObjectStore.Clear();
        }

        public ObjectData GetByID(string objectID)
        {
            return ObjectStore.GetOrAdd(objectID, InternalGetByID);
        }

        public IList<string> GetSections()
        {
            return DataSource.GetSections();
        }

        public IList<string> GetCategories(string sectionName)
        {
            return CategoriesStore.GetOrAdd(sectionName, InternalGetCategories);
        }

        public IList<string> GetRegions(string sectionName)
        {
            return RegionsStore.GetOrAdd(sectionName, InternalGetRegions);
        }
        
        public IList<string> GetCities(string sectionName, string regionName)
        {
            var citiesKey = Tuple.Create(sectionName, regionName);

            return CitiesStore.GetOrAdd(citiesKey, InternalGetCities);
        }

        public IList<int> GetTransportIssueYears()
        {
            return DataSource.GetTransportIssueYears();
        }

        public ObjectsDataSearchResults SearchObjects(SearchConfiguration configuration)
        {
            var identifiers = DataSource.GetObjectIdentifiers(configuration.SectionName);

            var results = new ObjectsDataSearchResults(this, identifiers, configuration);
            results.Prepare();
            return results;
        }
        #endregion

        #region Private class methods

        private ObjectData InternalGetByID(string objectID)
        {
            return DataSource.GetByID(objectID);
        }

        private IList<string> InternalGetCategories(string sectionName)
        {
            return DataSource.GetCategories(sectionName);
        }

        private IList<string> InternalGetRegions(string sectionName)
        {
            return DataSource.GetRegions(sectionName);
        }

        private IList<string> InternalGetCities(Tuple<string, string> key)
        {
            string sectionName = key.Item1;
            string regionName = key.Item2;

            return DataSource.GetCities(sectionName, regionName);
        }
        #endregion
    }
}