﻿CREATE TABLE [dbo].[avito_section_categories]
(
	[CategoryID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [avito_section_categories_def_categoryid] DEFAULT (NEWSEQUENTIALID()),
	[SectionID] UNIQUEIDENTIFIER NOT NULL,
	[Name] NVARCHAR(32) NOT NULL,
	
	CONSTRAINT [avito_section_categories_pk_categoryid] PRIMARY KEY NONCLUSTERED ([CategoryID]),

	CONSTRAINT [avito_section_categories_uni_sectionid_categoryname] UNIQUE NONCLUSTERED ([SectionID], [Name]),

	CONSTRAINT [avito_section_categories_fk_sectionid] FOREIGN KEY ([SectionID])
	REFERENCES [dbo].[avito_sections] ([SectionID]) ON DELETE CASCADE ON UPDATE NO ACTION
)
ON [PRIMARY];
GO