﻿CREATE TABLE [dbo].[avito_regions]
(
	[RegionID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [avito_regions_def_regionid] DEFAULT (NEWSEQUENTIALID()),
	[Name] NVARCHAR(64) NOT NULL,
	
	CONSTRAINT [avito_regions_pk_regionid] PRIMARY KEY NONCLUSTERED ([RegionID]),

	CONSTRAINT [avito_regions_uni_regionname] UNIQUE NONCLUSTERED ([Name])
)
ON [PRIMARY];
GO
