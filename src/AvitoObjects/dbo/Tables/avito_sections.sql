﻿CREATE TABLE [dbo].[avito_sections]
(
	[SectionID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [avito_sections_def_sectionid] DEFAULT (NEWSEQUENTIALID()),
	[Name] NVARCHAR(32) NOT NULL,
	
	CONSTRAINT [avito_sections_pk_sectionid] PRIMARY KEY NONCLUSTERED ([SectionID]),

	CONSTRAINT [avito_sections_uni_sectionname] UNIQUE NONCLUSTERED ([Name])
)
ON [PRIMARY];
GO