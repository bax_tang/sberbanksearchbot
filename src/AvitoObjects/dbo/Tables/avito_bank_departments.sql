﻿
CREATE TABLE [dbo].[avito_bank_departments]
(
	[DepartmentID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [avito_bank_departments_def_departmentid] DEFAULT (NEWSEQUENTIALID()),
	[Name] NVARCHAR(128) NOT NULL,
	[AvitoPageUrl] NVARCHAR(1024) NOT NULL,
	[BotServiceUrl] NVARCHAR(1024) NOT NULL,

	CONSTRAINT [avito_bank_departments_pk_departmentid] PRIMARY KEY NONCLUSTERED ([DepartmentID] ASC),

	CONSTRAINT [avito_bank_departments_uni_name] UNIQUE NONCLUSTERED ([Name] ASC)
)
ON [PRIMARY];
GO
