﻿CREATE TABLE [dbo].[avito_region_cities]
(
	[CityID] UNIQUEIDENTIFIER NOT NULL ROWGUIDCOL CONSTRAINT [avito_region_cities_def_cityid] DEFAULT (NEWSEQUENTIALID()),
	[RegionID] UNIQUEIDENTIFIER NOT NULL,
	[Name] NVARCHAR(64) NOT NULL,
	
	CONSTRAINT [avito_region_cities_pk_cityid] PRIMARY KEY NONCLUSTERED ([CityID]),

	CONSTRAINT [avito_region_cities_uni_regionid_cityname] UNIQUE NONCLUSTERED ([RegionID], [Name]),

	CONSTRAINT [avito_region_cities_fk_regionid] FOREIGN KEY ([RegionID])
	REFERENCES [dbo].[avito_regions] ([RegionID]) ON DELETE CASCADE ON UPDATE NO ACTION
)
ON [PRIMARY];
GO
