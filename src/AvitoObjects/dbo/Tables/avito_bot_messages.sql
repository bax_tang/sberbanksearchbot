﻿CREATE TABLE [dbo].[avito_bot_messages]
(
	[MessageID] BIGINT NOT NULL IDENTITY(1, 1),
	[DepartmentID] UNIQUEIDENTIFIER NOT NULL,
	[Key] NVARCHAR(64) NOT NULL,
	[Text] NVARCHAR(MAX) NULL, 

	CONSTRAINT [avito_bot_messages_pk_messageid] PRIMARY KEY NONCLUSTERED ([MessageID] ASC),

	CONSTRAINT [avito_bot_messages_uni_departmentid_key] UNIQUE NONCLUSTERED ([DepartmentID] ASC, [Key] ASC),

	CONSTRAINT [avito_bot_messages_fk_departmentid] FOREIGN KEY ([DepartmentID])
	REFERENCES [dbo].[avito_bank_departments] ([DepartmentID]) ON DELETE CASCADE ON UPDATE NO ACTION
)
ON [PRIMARY];
GO
