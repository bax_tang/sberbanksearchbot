﻿CREATE TABLE [dbo].[avito_objects]
(
	[ObjectID] BIGINT NOT NULL,

	[DepartmentID] UNIQUEIDENTIFIER NOT NULL,
	[SectionID] UNIQUEIDENTIFIER NOT NULL,
	[CategoryID] UNIQUEIDENTIFIER NOT NULL,
	[RegionID] UNIQUEIDENTIFIER NOT NULL,
	[CityID] UNIQUEIDENTIFIER NOT NULL,
	
	CONSTRAINT [avito_objects_pk_objectid] PRIMARY KEY NONCLUSTERED ([ObjectID]),
	
	CONSTRAINT [avito_objects_fk_departmentid] FOREIGN KEY ([DepartmentID])
	REFERENCES [dbo].[avito_bank_departments] ([DepartmentID]) ON DELETE CASCADE ON UPDATE NO ACTION,

	CONSTRAINT [avito_objects_fk_sectionid] FOREIGN KEY ([SectionID])
	REFERENCES [dbo].[avito_sections] ([SectionID]) ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	CONSTRAINT [avito_objects_fk_categoryid] FOREIGN KEY ([CategoryID])
	REFERENCES [dbo].[avito_section_categories] ([CategoryID]) ON DELETE CASCADE ON UPDATE NO ACTION,

	CONSTRAINT [avito_objects_fk_regionid] FOREIGN KEY ([RegionID])
	REFERENCES [dbo].[avito_regions] ([RegionID]) ON DELETE NO ACTION ON UPDATE NO ACTION,

	CONSTRAINT [avito_objects_fk_cityid] FOREIGN KEY ([CityID])
	REFERENCES [dbo].[avito_region_cities] ([CityID]) ON DELETE CASCADE ON UPDATE NO ACTION
)
ON [PRIMARY];
GO