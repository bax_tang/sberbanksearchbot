﻿CREATE TABLE [dbo].[avito_object_maininfo]
(
	[ObjectID] BIGINT NOT NULL,
	
	[Name] NVARCHAR(128) NOT NULL,
	[Description] NVARCHAR(MAX) NOT NULL,
	[Price] MONEY NULL,
	[Url] NVARCHAR(1024) NOT NULL,
	[IssueYear] INT NULL,

	CONSTRAINT [avito_object_maininfo_pk_objectid] PRIMARY KEY NONCLUSTERED ([ObjectID] ASC),
	
	CONSTRAINT [avito_object_maininfo_fk_objectid] FOREIGN KEY ([ObjectID])
	REFERENCES [dbo].[avito_objects] ([ObjectID]) ON DELETE CASCADE ON UPDATE NO ACTION
)
ON [PRIMARY];
GO
