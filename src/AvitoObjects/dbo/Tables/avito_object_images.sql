﻿CREATE TABLE [dbo].[avito_object_images]
(
	[ImageID] BIGINT NOT NULL,
	[ObjectID] BIGINT NOT NULL,
	[ImageUrl] NVARCHAR(1024) NOT NULL,

	CONSTRAINT [avito_object_images_pk_imageid] PRIMARY KEY NONCLUSTERED ([ImageID]),

	CONSTRAINT [avito_object_images_fk_objectid] FOREIGN KEY ([ObjectID])
	REFERENCES [dbo].[avito_objects] ([ObjectID]) ON DELETE CASCADE ON UPDATE NO ACTION
)
ON [PRIMARY];
GO