﻿
CREATE FUNCTION [dbo].[fn_bot_message_get_by_key]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL,
	@MessageKey NVARCHAR(64) = NULL
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @MessageText NVARCHAR(MAX) = NULL;

	SELECT @MessageText = [Text]
	FROM [dbo].[avito_bot_messages] WITH(NOLOCK)
	WHERE (([DepartmentID] IS NULL) OR ([DepartmentID] = @DepartmentID)) AND ([Key] = @MessageKey);

	RETURN @MessageText;
END;
GO