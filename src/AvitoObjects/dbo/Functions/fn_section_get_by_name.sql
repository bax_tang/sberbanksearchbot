﻿
CREATE FUNCTION [dbo].[fn_section_get_by_name]
(
	@SectionName NVARCHAR(32) = N''
)
RETURNS UNIQUEIDENTIFIER
WITH EXECUTE AS N'dbo'
AS
BEGIN
	DECLARE @SectionID UNIQUEIDENTIFIER = NULL;

	SELECT @SectionID = Sections.[SectionID]
	FROM [dbo].[avito_sections] AS Sections WITH(NOLOCK)
	WHERE Sections.[Name] = @SectionName;

	RETURN ISNULL(@SectionID, '00000000-0000-0000-0000-000000000000');
END;
GO