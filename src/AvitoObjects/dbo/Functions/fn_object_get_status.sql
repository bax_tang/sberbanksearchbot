﻿
CREATE FUNCTION [dbo].[fn_object_get_status]
(
	@ObjectID BIGINT = NULL
)
RETURNS INT
WITH EXECUTE AS N'dbo'
AS
BEGIN
	DECLARE @Exists INT = NULL;

	SELECT @Exists = 1
	FROM [dbo].[avito_objects] WITH(NOLOCK)
	WHERE [ObjectID] = @ObjectID;

	RETURN ISNULL(@Exists, 0);
END;
GO