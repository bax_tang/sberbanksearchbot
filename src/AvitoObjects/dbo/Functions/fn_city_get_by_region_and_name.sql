﻿
CREATE FUNCTION [dbo].[fn_city_get_by_region_and_name]
(
	@RegionID UNIQUEIDENTIFIER,
	@CityName NVARCHAR(64) = N''
)
RETURNS UNIQUEIDENTIFIER
WITH EXECUTE AS N'dbo'
AS
BEGIN
	DECLARE @CityID UNIQUEIDENTIFIER = NULL;

	SELECT @CityID = Cities.[CityID]
	FROM [dbo].[avito_region_cities] AS Cities WITH(NOLOCK)
	WHERE Cities.[RegionID] = @RegionID AND Cities.[Name] = @CityName;

	RETURN ISNULL(@CityID, '00000000-0000-0000-0000-000000000000');
END;
GO
