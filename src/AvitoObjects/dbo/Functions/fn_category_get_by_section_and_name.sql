﻿
CREATE FUNCTION [dbo].[fn_category_get_by_section_and_name]
(
	@SectionID UNIQUEIDENTIFIER,
	@CategoryName NVARCHAR(32) = N''
)
RETURNS UNIQUEIDENTIFIER
WITH EXECUTE AS N'dbo'
AS
BEGIN
	DECLARE @CategoryID UNIQUEIDENTIFIER = NULL;

	SELECT @CategoryID = Categories.[CategoryID]
	FROM dbo.avito_section_categories AS Categories WITH(NOLOCK)
	WHERE Categories.[SectionID] = @SectionID AND Categories.[Name] = @CategoryName;

	RETURN ISNULL(@CategoryID, '00000000-0000-0000-0000-000000000000');
END;
GO