﻿
CREATE FUNCTION [dbo].[fn_region_get_by_name]
(
	@RegionName NVARCHAR(64) = N''
)
RETURNS UNIQUEIDENTIFIER
WITH EXECUTE AS N'dbo'
AS
BEGIN
	DECLARE @RegionID UNIQUEIDENTIFIER = NULL;

	SELECT @RegionID = Regions.[RegionID]
	FROM [dbo].[avito_regions] AS Regions WITH(NOLOCK)
	WHERE Regions.[Name] = @RegionName;

	RETURN ISNULL(@RegionID, '00000000-0000-0000-0000-000000000000');
END;
GO