﻿
CREATE PROCEDURE [dbo].[sp_city_get_by_section_and_region]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL,
	@SectionName NVARCHAR(32) = N'',
	@RegionName NVARCHAR(64) = N''
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SectionID UNIQUEIDENTIFIER = [dbo].[fn_section_get_by_name](@SectionName);
	DECLARE @RegionID UNIQUEIDENTIFIER = [dbo].[fn_region_get_by_name](@RegionName);

	SELECT DISTINCT Cities.[Name]
	FROM [dbo].[avito_region_cities] AS Cities WITH(NOLOCK)
	INNER JOIN [dbo].[avito_objects] AS Instances WITH(NOLOCK) ON Instances.[CityID] = Cities.[CityID]
	WHERE
		Instances.[DepartmentID] = @DepartmentID AND
		Instances.[SectionID] = @SectionID AND
		Instances.[RegionID] = @RegionID
	ORDER BY Cities.[Name];
END;
GO