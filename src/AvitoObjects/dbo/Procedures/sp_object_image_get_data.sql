﻿
CREATE PROCEDURE [dbo].[sp_object_image_get_data]
(
	@ObjectID BIGINT = NULL
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF ((@ObjectID IS NULL) OR (@ObjectID = 0))
	BEGIN
		RAISERROR (N'Object ID can''t be null or equals zero.', 16, 1);
	END;

	SELECT [ImageUrl]
	FROM [dbo].[avito_object_images] WITH(NOLOCK)
	WHERE [ObjectID] = @ObjectID;
END;
GO