﻿
CREATE PROCEDURE [dbo].[sp_object_delete_by_dep]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF (@DepartmentID IS NULL) RETURN;

	DELETE FROM [dbo].[avito_objects] WITH(ROWLOCK)
	WHERE [DepartmentID] = @DepartmentID;
END;
GO
