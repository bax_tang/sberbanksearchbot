﻿
CREATE PROCEDURE [dbo].[sp_object_delete_all]
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM [dbo].[avito_objects] WITH(TABLOCK);
END;
GO