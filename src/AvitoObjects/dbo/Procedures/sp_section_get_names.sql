﻿
CREATE PROCEDURE [dbo].[sp_section_get_names]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT Sections.[Name]
	FROM [dbo].[avito_sections] AS Sections WITH(NOLOCK)
	INNER JOIN [dbo].[avito_objects] AS Instances WITH(NOLOCK) ON (Instances.[SectionID] = Sections.[SectionID])
	WHERE Instances.[DepartmentID] = @DepartmentID
	ORDER BY Sections.[Name] ASC;
END;
GO