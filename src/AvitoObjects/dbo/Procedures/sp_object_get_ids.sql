﻿
CREATE PROCEDURE [dbo].[sp_object_get_ids]
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [ObjectID] AS 'ID'
	FROM [dbo].[avito_objects] WITH(NOLOCK);
END;
GO