﻿
CREATE PROCEDURE [dbo].[sp_object_get_data]
(
	@ObjectID BIGINT = NULL
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF ((@ObjectID IS NULL) OR (@ObjectID = 0))
	BEGIN
		RAISERROR (N'Object ID can''t be null or equals zero.', 16, 1);
  END;

	SELECT
		CAST(Instances.[ObjectID] AS VARCHAR) AS 'ObjectID',
		Sections.[Name] AS 'SectionName',
		Categories.[Name] AS 'CategoryName',
		Regions.[Name] AS 'RegionName',
		Cities.[Name] AS 'CityName',
		MainInfo.[Name] AS 'ObjectName',
		MainInfo.[Description] AS 'ObjectDescription',
		MainInfo.[Price] AS 'ObjectPrice',
		MainInfo.[Url] AS 'ObjectUrl',
		MainInfo.[IssueYear] AS 'IssueYear'
	FROM [dbo].[avito_objects] AS Instances WITH(NOLOCK)
	INNER JOIN [dbo].[avito_object_maininfo] AS MainInfo WITH(NOLOCK) ON (MainInfo.[ObjectID] = Instances.[ObjectID])
	INNER JOIN [dbo].[avito_sections] AS Sections WITH(NOLOCK) ON (Sections.[SectionID] = Instances.[SectionID])
	INNER JOIN [dbo].[avito_section_categories] AS Categories WITH(NOLOCK) ON (Categories.[CategoryID] = Instances.[CategoryID])
	INNER JOIN [dbo].[avito_regions] AS Regions WITH(NOLOCK) ON (Regions.[RegionID] = Instances.[RegionID])
	INNER JOIN [dbo].[avito_region_cities] AS Cities WITH(NOLOCK) ON (Cities.[CityID] = Instances.[CityID])
	WHERE Instances.[ObjectID] = @ObjectID;
END;
GO