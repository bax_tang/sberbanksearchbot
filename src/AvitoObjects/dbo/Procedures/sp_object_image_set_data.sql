﻿
CREATE PROCEDURE [dbo].[sp_object_image_set_data]
(
	@ImageID BIGINT,
	@ObjectID BIGINT,
	@ImageUrl VARCHAR(1024),
	@NewImageID BIGINT = NULL OUTPUT	
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;
	
	IF ((@ObjectID IS NULL) OR (@ObjectID = 0))
	BEGIN
		RAISERROR (N'Object ID can''t be null or equals zero.', 16, 1);
	END;

	IF ((@ImageID IS NULL) OR (@ImageID = 0))
	BEGIN
		RAISERROR (N'Image ID can''t be null or equals zero.', 16, 1);
	END;

	MERGE INTO [dbo].[avito_object_images] WITH(ROWLOCK) AS Target
	USING (SELECT @ImageID, @ObjectID) AS Source([ImageID], [ObjectID])
	ON Target.[ImageID] = Source.[ImageID] AND Target.[ObjectID] = Source.[ObjectID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([ImageID], [ObjectID], [ImageUrl])
		VALUES (@ImageID, @ObjectID, @ImageUrl)
	WHEN MATCHED THEN
		UPDATE SET
			[ImageUrl] = @ImageUrl;

	SET @NewImageID = @ImageID;
END;
GO