﻿
CREATE PROCEDURE [dbo].[sp_region_get_by_section_name]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL,
	@SectionName NVARCHAR(32) = N''
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SectionID UNIQUEIDENTIFIER = [dbo].[fn_section_get_by_name](@SectionName);

	SELECT DISTINCT Regions.[Name]
	FROM [dbo].[avito_regions] AS Regions WITH(NOLOCK)
	INNER JOIN [dbo].[avito_objects] AS Instances WITH(NOLOCK) ON (Instances.[RegionID] = Regions.[RegionID])
	WHERE
		Instances.[DepartmentID] = @DepartmentID AND
		Instances.[SectionID] = @SectionID
	ORDER BY Regions.[Name];
END;
GO