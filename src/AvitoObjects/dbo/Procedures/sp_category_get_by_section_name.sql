﻿CREATE PROCEDURE [dbo].[sp_category_get_by_section_name]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL,
	@SectionName NVARCHAR(32) = N''
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SectionID UNIQUEIDENTIFIER = [dbo].[fn_section_get_by_name](@SectionName);

	SELECT DISTINCT Categories.[Name]
	FROM [dbo].[avito_section_categories] AS Categories WITH(NOLOCK)
	INNER JOIN [dbo].[avito_objects] AS Instances WITH(NOLOCK) ON (Instances.[CategoryID] = Categories.[CategoryID])
	WHERE
		Instances.[DepartmentID] = @DepartmentID AND
		Categories.[SectionID] = @SectionID
	ORDER BY Categories.[Name] ASC;
END;
GO
