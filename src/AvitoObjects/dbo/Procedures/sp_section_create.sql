﻿
CREATE PROCEDURE [dbo].[sp_section_create]
(
	@SectionID UNIQUEIDENTIFIER = NULL,
	@SectionName NVARCHAR(32) = N'',
	@NewSectionID UNIQUEIDENTIFIER = NULL OUTPUT
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF ((@SectionName IS NULL) OR (@SectionName = N''))
	BEGIN
		RAISERROR (N'Section name can''t be null or empty.', 16, 1);
  END;

	DECLARE @EmptyID UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000';
	IF ((@SectionID IS NULL) OR (@SectionID = @EmptyID))
		BEGIN
			DECLARE @NewSections TABLE ([SectionID] UNIQUEIDENTIFIER NOT NULL);

			INSERT INTO [dbo].[avito_sections] WITH(ROWLOCK) ([Name])
			OUTPUT Inserted.[SectionID] INTO @NewSections ([SectionID])
			VALUES (@SectionName);

			SELECT @NewSectionID = [SectionID] FROM @NewSections;
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[avito_sections] WITH(ROWLOCK) ([SectionID], [Name])
			VALUES (@SectionID, @SectionName);

			SET @NewSectionID = @SectionID;
		END;
END;
GO