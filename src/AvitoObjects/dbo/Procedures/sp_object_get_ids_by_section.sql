﻿
CREATE PROCEDURE [dbo].[sp_object_get_ids_by_section]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL,
	@SectionName NVARCHAR(32) = N''
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF ((@SectionName IS NULL) OR (@SectionName = N''))
	BEGIN
		RAISERROR (N'Section name can''t be null or empty.', 16, 1);
	END;

	DECLARE @SectionID UNIQUEIDENTIFIER = [dbo].[fn_section_get_by_name](@SectionName);

	SELECT CAST(Instances.[ObjectID] AS VARCHAR) AS 'ID'
	FROM [dbo].[avito_objects] AS Instances WITH(NOLOCK)
	WHERE Instances.[DepartmentID] = @DepartmentID AND Instances.[SectionID] = @SectionID
	ORDER BY Instances.[ObjectID];
END;
GO