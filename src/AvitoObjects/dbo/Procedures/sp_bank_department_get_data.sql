﻿
CREATE PROCEDURE [dbo].[sp_bank_department_get_data]
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		[DepartmentID] AS 'DepartmentID',
		[Name] AS 'Name',
		[AvitoPageUrl] AS 'AvitoPageUrl',
		[BotServiceUrl] AS 'BotServiceUrl'
	FROM [dbo].[avito_bank_departments] WITH(NOLOCK);
END;
GO