﻿
CREATE PROCEDURE [dbo].[sp_city_create]
(
	@CityID UNIQUEIDENTIFIER = NULL,
	@RegionID UNIQUEIDENTIFIER = NULL,
	@CityName NVARCHAR(64) = N'',
	@NewCityID UNIQUEIDENTIFIER = NULL OUTPUT
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EmptyID UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000';

	IF ((@RegionID IS NULL) OR (@RegionID = @EmptyID))
	BEGIN
		RAISERROR (N'Region ID can''t be null or empty.', 16, 1);
	END;

	IF ((@CityName IS NULL) OR (@CityName = N''))
	BEGIN
		RAISERROR (N'City name can''t be null or empty.', 16, 1);
	END;

	IF ((@CityID IS NULL) OR (@CityID = @EmptyID))
		BEGIN
			DECLARE @NewCities TABLE ([CityID] UNIQUEIDENTIFIER NOT NULL);

			INSERT INTO [dbo].[avito_region_cities] WITH(ROWLOCK) ([RegionID], [Name])
			OUTPUT Inserted.[CityID] INTO @NewCities ([CityID])
			VALUES (@RegionID, @CityName);

			SELECT @NewCityID = [CityID] FROM @NewCities;
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[avito_region_cities] WITH(ROWLOCK) ([CityID], [RegionID], [Name])
			VALUES (@CityID, @RegionID, @CityName);

			SET @NewCityID = @CityID;
		END;
END;
GO
