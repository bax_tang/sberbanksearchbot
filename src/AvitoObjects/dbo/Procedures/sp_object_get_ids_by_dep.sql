﻿
CREATE PROCEDURE [dbo].[sp_object_get_ids_by_dep]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [ObjectID] AS 'ID'
	FROM [dbo].[avito_objects] WITH(NOLOCK)
	WHERE [DepartmentID] = @DepartmentID;
END;
GO