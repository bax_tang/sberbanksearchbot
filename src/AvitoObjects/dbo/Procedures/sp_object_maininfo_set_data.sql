﻿
CREATE PROCEDURE [dbo].[sp_object_maininfo_set_data]
(
	@ObjectID BIGINT,
	@Name NVARCHAR(128),
	@Description NVARCHAR(MAX),
	@Price MONEY = NULL,
	@Url NVARCHAR(1024),
	@IssueYear INT = NULL
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS
		(
			SELECT 1
			FROM [dbo].[avito_object_maininfo] WITH(NOLOCK)
			WHERE [ObjectID] = @ObjectID
		)
	BEGIN
		UPDATE [dbo].[avito_object_maininfo] WITH(ROWLOCK)
		SET
			[Name] = @Name,
			[Description] = @Description,
			[Price] = @Price,
			[Url] = @Url,
			[IssueYear] = @IssueYear
		WHERE [ObjectID] = @ObjectID;
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[avito_object_maininfo] WITH(ROWLOCK) ([ObjectID], [Name], [Description], [Price], [Url], [IssueYear])
		VALUES (@ObjectID, @Name, @Description, @Price, @Url, @IssueYear);
	END;
END;
GO
