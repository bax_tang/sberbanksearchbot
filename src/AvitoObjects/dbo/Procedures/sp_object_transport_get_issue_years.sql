﻿
CREATE PROCEDURE [dbo].[sp_object_transport_get_issue_years]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	SELECT MainInfo.[IssueYear] AS 'IssueYear'
	FROM [dbo].[avito_object_maininfo] AS MainInfo WITH(NOLOCK)
	INNER JOIN [dbo].[avito_objects] AS Instances WITH(NOLOCK) ON (Instances.[ObjectID] = MainInfo.[ObjectID])
	WHERE Instances.[DepartmentID] = @DepartmentID;
END;
GO