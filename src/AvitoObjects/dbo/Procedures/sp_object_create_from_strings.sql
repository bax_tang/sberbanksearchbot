﻿
CREATE PROCEDURE [dbo].[sp_object_create_from_strings]
(
	@ObjectID BIGINT = NULL,
	@DepartmentID UNIQUEIDENTIFIER = NULL,
	@SectionName NVARCHAR(32) = NULL,
	@CategoryName NVARCHAR(32) = NULL,
	@RegionName NVARCHAR(64) = NULL,
	@CityName NVARCHAR(64) = NULL
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF ((@ObjectID IS NULL) OR (@ObjectID = 0) OR (@DepartmentID IS NULL)) RETURN;

	DECLARE
		@EmptyID UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000',
		@SectionID UNIQUEIDENTIFIER = NULL,
		@CategoryID UNIQUEIDENTIFIER = NULL,
		@RegionID UNIQUEIDENTIFIER = NULL,
		@CityID UNIQUEIDENTIFIER = NULL;

	SET @SectionID = [dbo].[fn_section_get_by_name] (@SectionName);
	IF ((@SectionID IS NULL) OR (@SectionID = @EmptyID))
	BEGIN
		EXEC [dbo].[sp_section_create] NULL, @SectionName, @SectionID OUTPUT;
	END;

	SET @CategoryID = [dbo].[fn_category_get_by_section_and_name] (@SectionID, @CategoryName);
	IF ((@CategoryID IS NULL) OR (@CategoryID = @EmptyID))
	BEGIN
		EXEC [dbo].[sp_category_create] NULL, @SectionID, @CategoryName, @CategoryID OUTPUT;
	END;

	SET @RegionID = [dbo].[fn_region_get_by_name] (@RegionName);
	IF ((@RegionID IS NULL) OR (@RegionID = @EmptyID))
	BEGIN
		EXEC [dbo].[sp_region_create] NULL, @RegionName, @RegionID OUTPUT;
	END;

	SET @CityID = [dbo].[fn_city_get_by_region_and_name] (@RegionID, @CityName);
	IF ((@CityID IS NULL) OR (@CityID = @EmptyID))
	BEGIN
		EXEC [dbo].[sp_city_create] NULL, @RegionID, @CityName, @CityID OUTPUT;
	END;

	IF ((@SectionID IS NULL) OR (@CategoryID IS NULL) OR (@RegionID IS NULL) OR (@CityID IS NULL)) RETURN;

	INSERT INTO [dbo].[avito_objects] WITH(ROWLOCK) ([ObjectID], [DepartmentID], [SectionID], [CategoryID], [RegionID], [CityID])
	SELECT
		@ObjectID,
		@DepartmentID,
		@SectionID,
		@CategoryID,
		@RegionID,
		@CityID;
END;
GO