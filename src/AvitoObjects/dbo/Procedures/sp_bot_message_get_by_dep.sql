﻿
CREATE PROCEDURE [dbo].[sp_bot_message_get_by_dep]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [Key], [Text]
	FROM [dbo].[avito_bot_messages] WITH(NOLOCK)
	WHERE (([DepartmentID] IS NULL) OR ([DepartmentID] = @DepartmentID));
END;
GO