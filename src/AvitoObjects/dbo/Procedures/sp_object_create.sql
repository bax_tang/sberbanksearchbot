﻿
CREATE PROCEDURE [dbo].[sp_object_create]
(
	@ObjectID BIGINT,
	@DepartmentID UNIQUEIDENTIFIER,
	@SectionID UNIQUEIDENTIFIER,
	@CategoryID UNIQUEIDENTIFIER,
	@RegionID UNIQUEIDENTIFIER,
	@CityID UNIQUEIDENTIFIER,
	@NewObjectID BIGINT = NULL OUTPUT
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF ((@ObjectID IS NULL) OR (@ObjectID = 0))
	BEGIN
		RAISERROR (N'Object ID can''t be null or equals zero.', 16, 1);
	END;
	
	IF EXISTS(
		SELECT 1
		FROM [dbo].[avito_objects] WITH(NOLOCK)
		WHERE (([DepartmentID] = @DepartmentID) AND ([ObjectID] = @ObjectID)))
	BEGIN
		SET @NewObjectID = -1;
		RETURN 0x100; -- object with specified identifier already exists
	END;

	INSERT INTO [dbo].[avito_objects] WITH(ROWLOCK) ([ObjectID], [DepartmentID], [SectionID], [CategoryID], [RegionID], [CityID])
	VALUES (@ObjectID, @DepartmentID, @SectionID, @CategoryID, @RegionID, @CityID);

	SET @NewObjectID = @ObjectID;
	RETURN 0; -- object created successfully
END;
GO
