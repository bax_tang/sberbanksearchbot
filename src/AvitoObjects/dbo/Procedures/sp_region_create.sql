﻿
CREATE PROCEDURE [dbo].[sp_region_create]
(
	@RegionID UNIQUEIDENTIFIER = NULL,
	@RegionName NVARCHAR(64) = N'',
	@NewRegionID UNIQUEIDENTIFIER = NULL OUTPUT
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF ((@RegionName IS NULL) OR (@RegionName = N''))
	BEGIN
		RAISERROR (N'Region name can''t be null or empty.', 16, 1);
	END;

	DECLARE @EmptyID UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000';
	IF ((@RegionID IS NULL) OR (@RegionID = @EmptyID))
		BEGIN
			DECLARE @NewRegions TABLE ([RegionID] UNIQUEIDENTIFIER NOT NULL);

			INSERT INTO [dbo].[avito_regions] WITH(ROWLOCK) ([Name])
			OUTPUT Inserted.[RegionID] INTO @NewRegions ([RegionID])
			VALUES (@RegionName);

			SELECT @NewRegionID = [RegionID] FROM @NewRegions;
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[avito_regions] WITH(ROWLOCK) ([RegionID], [Name])
			VALUES (@RegionID, @RegionName);

			SET @NewRegionID = @RegionID;
		END;
END;
GO