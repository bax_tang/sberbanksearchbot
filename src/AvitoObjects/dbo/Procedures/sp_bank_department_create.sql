﻿
CREATE PROCEDURE [dbo].[sp_bank_department_create]
(
	@DepartmentID UNIQUEIDENTIFIER = NULL,
	@DepartmentName NVARCHAR(128) = N'',
	@DepartmentPageUrl NVARCHAR(256) = N'',
	@NewDepartmentID UNIQUEIDENTIFIER = NULL OUTPUT
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	IF ((@DepartmentName IS NULL) OR (@DepartmentName = N''))
	BEGIN
		RAISERROR (N'Department name can''t be null or empty.', 16, 1);
	END;

	IF ((@DepartmentPageUrl IS NULL) OR (@DepartmentPageUrl = N''))
	BEGIN
		RAISERROR (N'Department page URL can''t be null or empty.', 16, 1);
	END;

	DECLARE @EmptyID UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000';
	IF ((@DepartmentID IS NULL) OR (@DepartmentID = @EmptyID))
		BEGIN
			DECLARE @NewDepartments TABLE ([DepartmentID] UNIQUEIDENTIFIER NOT NULL);

			INSERT INTO [dbo].[avito_bank_departments] WITH(ROWLOCK) ([Name], [AvitoPageUrl])
			OUTPUT Inserted.[DepartmentID] INTO @NewDepartments ([DepartmentID])
			VALUES (@DepartmentName, @DepartmentPageUrl);

			SELECT @NewDepartmentID = [DepartmentID] FROM @NewDepartments;
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[avito_bank_departments] WITH(ROWLOCK) ([DepartmentID], [Name], [AvitoPageUrl])
			VALUES (@DepartmentID, @DepartmentName, @DepartmentPageUrl);

			SET @NewDepartmentID = @DepartmentID;
		END;
END;
GO