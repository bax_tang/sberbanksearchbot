﻿
CREATE PROCEDURE [dbo].[sp_category_create]
(
	@CategoryID UNIQUEIDENTIFIER = NULL,
	@SectionID UNIQUEIDENTIFIER = NULL,
	@CategoryName NVARCHAR(32) = N'',
	@NewCategoryID UNIQUEIDENTIFIER = NULL OUTPUT
)
WITH EXECUTE AS N'dbo'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EmptyID UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000';

	IF ((@SectionID IS NULL) OR (@SectionID = @EmptyID))
	BEGIN
		RAISERROR (N'Section ID can'' be null or empty.', 16, 1);
	END;

	IF ((@CategoryName IS NULL) OR (@CategoryName = N''))
	BEGIN
		RAISERROR (N'Category name can''t be null or empty.', 16, 1);
	END;

	IF ((@CategoryID IS NULL) OR (@CategoryID = @EmptyID))
		BEGIN
			DECLARE @NewCategories TABLE ([CategoryID] UNIQUEIDENTIFIER NOT NULL);

			INSERT INTO [dbo].[avito_section_categories] WITH(ROWLOCK) ([SectionID], [Name])
			OUTPUT Inserted.[CategoryID] INTO @NewCategories ([CategoryID])
			VALUES (@SectionID, @CategoryName);

			SELECT @NewCategoryID = [CategoryID] FROM @NewCategories;
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[avito_section_categories] WITH(ROWLOCK) ([CategoryID], [SectionID], [Name])
			VALUES (@CategoryID, @SectionID, @CategoryName);

			SET @NewCategoryID = @CategoryID;
		END;
END;
GO
