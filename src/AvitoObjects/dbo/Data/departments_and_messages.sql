﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*
INSERT INTO [dbo].[avito_bank_departments] WITH(ROWLOCK) ([DepartmentID], [Name], [AvitoPageUrl], [BotServiceUrl])
VALUES
	(
		'55BF4517-D217-4169-A462-3509A0F43677',
		N'Западно-Сибирский банк ПАО "Сбербанк"',
		N'http://avito.ru/sberbank/rossiya',
		N'http://realtysearchbot.azurewebsites.net'
	),
	(
		'452F48D1-FEB0-4B02-8463-A9AF0BF6FC67',
		N'Сибирский банк ПАО "Сбербанк"',
		N'http://avito.ru/pao_sberbank/rossiya',
		N'http://paosberbanksearchbot.azurewebsites.net'
	);
GO

INSERT INTO [dbo].[avito_bot_messages] WITH(ROWLOCK) ([DepartmentID], [Key], [Text])
VALUES
	(
		'55BF4517-D217-4169-A462-3509A0F43677',
		N'GreetingCardText',
		N'Вас приветствует чат-бот уведомления о поступлении реализации недвижимости Западно-Сибирского банка ПАО ""Сбербанк"".

Вы можете управлять этим ботом с помощью следующих команд:'
	),
	(
		'55BF4517-D217-4169-A462-3509A0F43677',
		N'GreetingCardTextNoHeader',
		N'Чат-бот уведомления о поступлении реализации недвижимости Западно-Сибирского банка ПАО ""Сбербанк"".

Вы можете управлять этим ботом с помощью следующих команд:'
	),
	(
		'452F48D1-FEB0-4B02-8463-A9AF0BF6FC67',
		N'GreetingCardText',
		N'Вас приветствует чат-бот уведомления о поступлении реализации недвижимости Сибирского банка ПАО ""Сбербанк"".

Вы можете управлять этим ботом с помощью следующих команд:'
	),
	(
		'452F48D1-FEB0-4B02-8463-A9AF0BF6FC67',
		N'GreetingCardTextNoHeader',
		N'Чат-бот уведомления о поступлении реализации недвижимости Сибирского банка ПАО ""Сбербанк"".

Вы можете управлять этим ботом с помощью следующих команд:'
	);
*/