﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Data;
using System.Globalization;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class AvitoSqlDatabaseUpdater : IDisposable
    {
        #region Constants and fields

        private JArray JsonObjectsData;

        private AvitoDatabaseContext DatabaseContext;
        #endregion

        #region Constructors

        public AvitoSqlDatabaseUpdater(JArray jsonObjectsData, AvitoDatabaseContext databaseContext)
        {
            if (jsonObjectsData == null)
            {
                throw new ArgumentNullException(nameof(jsonObjectsData), "Json objects data can't be null.");
            }
            if (databaseContext == null)
            {
                throw new ArgumentNullException(nameof(databaseContext), "Database context can't be null.");
            }

            JsonObjectsData = jsonObjectsData;
            DatabaseContext = databaseContext;
        }
        #endregion

        #region IDisposable implementation

        void IDisposable.Dispose()
        {
            DatabaseContext.Dispose();
        }
        #endregion

        #region Public class methods

        public void UploadData()
        {
            PrepareDatabase();
            DoUpdate();
        }
        #endregion

        #region Private class methods

        private void PrepareDatabase()
        {
            DatabaseContext.PrepareDatabase();
        }

        private void DoUpdate()
        {
            JArray jsonData = JsonObjectsData;
            AvitoDatabaseContext context = DatabaseContext;

            for (int index = 0; index < jsonData.Count; ++index)
            {
                JObject currentObject = jsonData[index] as JObject;

                string identifier = (string)currentObject["ID"];
                Console.WriteLine("Processing object with ID = {0}", identifier);

                long objectID = CreateObjectAndSetMainInfo(identifier, currentObject, context);
                if (objectID != 0L)
                {
                    SetSectionSpecificInfo(objectID, currentObject, context);

                    JArray imagesArray = currentObject["Images"] as JArray;
                    if ((imagesArray != null) && (imagesArray.Count > 0))
                    {
                        AddObjectImages(objectID, imagesArray, context);
                    }

                    Console.WriteLine("Object successfully uploaded");
                }
            }
        }

        private static long CreateObjectAndSetMainInfo(string identifier, JObject currentObject, AvitoDatabaseContext context)
        {
            long objectID = 0L; long newObjectID = 0L;

            if (long.TryParse(identifier, NumberStyles.Any, CultureInfo.InvariantCulture, out objectID))
            {
                string sectionName = (string)currentObject["Section"];
                string categoryName = (string)currentObject["Category"];
                string regionName = (string)currentObject["Region"];
                string cityName = (string)currentObject["City"];

                newObjectID = context.CreateObject(objectID, sectionName, categoryName, regionName, cityName);

                string objectName = (string)currentObject["Name"];
                string objectDescription = (string)currentObject["Description"];
                string objectUrl = (string)currentObject["URL"];
                double? price = (double?)currentObject["Price"];
                decimal? objectPrice = price.HasValue ? (decimal?)price : null;

                context.SetMainInfo(newObjectID, objectName, objectDescription, objectPrice, objectUrl);
            }
            else
            {
                Console.WriteLine("Can't parse object identifier '{0}' to System.Int64 data type.", identifier);
            }

            return newObjectID;
        }

        private static void SetSectionSpecificInfo(long objectID, JObject currentObject, AvitoDatabaseContext context)
        {
            string sectionName = (string)currentObject["Section"];

            if (string.Equals("Транспорт", sectionName, StringComparison.OrdinalIgnoreCase))
            {
                int? issueYear = (int?)currentObject["Year"];

                context.SetTransportInfo(objectID, issueYear);
            }
            else if (string.Equals("Недвижимость", sectionName, StringComparison.OrdinalIgnoreCase))
            {
                // TODO: реализовать заполнение дополнительной информации по секции "Недвижимость"
            }
        }

        private static void AddObjectImages(long objectID, JArray imagesArray, AvitoDatabaseContext context)
        {
            for (int imageIndex = 0; imageIndex < imagesArray.Count; ++imageIndex)
            {
                string imageUrl = (string)imagesArray[imageIndex];

                int slashIndex = imageUrl.LastIndexOf('/');
                int dotIndex = imageUrl.LastIndexOf('.');

                string imageIdentifier = imageUrl.Substring(1 + slashIndex, dotIndex - slashIndex - 1);
                long imageID = long.Parse(imageIdentifier);

                context.AddObjectImage(objectID, imageID, imageUrl);
            }
        }
        #endregion
    }
}