﻿using System;

namespace SberbankSearchBot
{
    public static class AvitoDatabaseConstants
    {
        #region Constants

        public const string ClearObjects = "dbo.sp_objects_clear";

        public const string RegionGetByName = "dbo.fn_region_get_by_name";

        public const string CityGetByRegionAndName = "dbo.fn_city_get_by_region_and_name";

        public const string SectionGetByName = "dbo.fn_section_get_by_name";

        public const string CategoryGetBySectionAndName = "dbo.fn_category_get_by_section_and_name";

        public const string RegionCreate = "dbo.sp_region_create";

        public const string CityCreate = "dbo.sp_city_create";

        public const string SectionCreate = "dbo.sp_section_create";

        public const string CategoryCreate = "dbo.sp_category_create";

        public const string ObjectCreate = "dbo.sp_object_create";

        public const string ObjectSetMainInfo = "dbo.sp_object_maininfo_set_data";

        public const string ObjectSetRealtyInfo = "dbo.sp_object_realtyinfo_set_data";

        public const string ObjectSetTransportInfo = "dbo.sp_object_transportinfo_set_data";

        public const string ObjectAddImage = "dbo.sp_object_image_set_data";
        #endregion
    }
}