﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace SberbankSearchBot
{
    public class AvitoDatabaseContext : IDisposable
    {
        #region Constants and fields

        private SqlConnection InnerConnection;
        #endregion

        #region Constructors

        public AvitoDatabaseContext(SqlConnection connection)
        {
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection), "Database connection can't be null.");
            }
            if (connection.State != ConnectionState.Open)
            {
                throw new ArgumentException("Database connection must be in Open state.", nameof(connection));
            }

            InnerConnection = connection;
        }
        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            InnerConnection?.Dispose();
            InnerConnection = null;
        }
        #endregion

        #region Public class methods

        public void PrepareDatabase()
        {
            ClearObjects();
        }

        public Guid GetOrCreateRegion(string regionName)
        {
            Guid regionID = GetRegion(regionName);
            if (regionID == Guid.Empty)
            {
                regionID = CreateNewRegion(regionName);
                if (regionID == Guid.Empty)
                {
                    throw new InvalidOperationException("Region doesn't created.");
                }
            }
            return regionID;
        }

        public Guid GetOrCreateCity(Guid regionID, string cityName)
        {
            Guid cityID = GetCity(regionID, cityName);
            if (cityID == Guid.Empty)
            {
                cityID = CreateNewCity(regionID, cityName);
                if (cityID == Guid.Empty)
                {
                    throw new InvalidOperationException("City doesn't created.");
                }
            }
            return cityID;
        }

        public Guid GetOrCreateSection(string sectionName)
        {
            Guid sectionID = GetSection(sectionName);
            if (sectionID == Guid.Empty)
            {
                sectionID = CreateNewSection(sectionName);
                if (sectionID == Guid.Empty)
                {
                    throw new InvalidOperationException("Section doesn't created.");
                }
            }
            return sectionID;
        }

        public Guid GetOrCreateCategory(Guid sectionID, string categoryName)
        {
            Guid categoryID = GetCategory(sectionID, categoryName);
            if (categoryID == Guid.Empty)
            {
                categoryID = CreateNewCategory(sectionID, categoryName);
                if (categoryID == Guid.Empty)
                {
                    throw new InvalidOperationException("Category doesn't created.");
                }
            }
            return categoryID;
        }

        public long CreateObject(long objectID, string sectionName, string categoryName, string regionName, string cityName)
        {
            if (objectID == 0L)
            {
                throw new ArgumentOutOfRangeException(nameof(objectID), "Object ID can't be equal zero.");
            }
            if (string.IsNullOrEmpty(sectionName))
            {
                throw new ArgumentNullException(nameof(sectionName), "Section name can't be null or empty.");
            }
            if (string.IsNullOrEmpty(categoryName))
            {
                throw new ArgumentNullException(nameof(categoryName), "Category name can't be null or empty.");
            }
            if (string.IsNullOrEmpty(regionName))
            {
                throw new ArgumentNullException(nameof(regionName), "Region name can't be null or empty.");
            }
            if (string.IsNullOrEmpty(cityName))
            {
                throw new ArgumentNullException(nameof(cityName), "City name can't be null or empty.");
            }

            Guid sectionID = GetOrCreateSection(sectionName);
            Guid categoryID = GetOrCreateCategory(sectionID, categoryName);
            Guid regionID = GetOrCreateRegion(regionName);
            Guid cityID = GetOrCreateCity(regionID, cityName);

            long newObjectID = CreateNewObject(objectID, sectionID, categoryID, regionID, cityID);
            if (newObjectID == 0L)
            {
                throw new InvalidOperationException("Object doesn't created.");
            }
            return newObjectID;
        }

        public void SetMainInfo(long objectID, string objectName, string objectDescription, decimal? objectPrice, string objectUrl)
        {
            using (SqlCommand setCommand = CreateSetMainInfoCommand(objectID, objectName, objectDescription, objectPrice, objectUrl))
            {
                setCommand.ExecuteNonQuery();
            }
        }

        public void SetTransportInfo(long objectID, int? issueYear = null)
        {
            using (SqlCommand setCommand = CreateSetTransportInfoCommand(objectID, issueYear))
            {
                setCommand.ExecuteNonQuery();
            }
        }

        public long AddObjectImage(long objectID, long imageID, string imageUrl)
        {
            long newImageID = CreateObjectImage(objectID, imageID, imageUrl);
            if (newImageID == 0L)
            {
                throw new InvalidOperationException("Image doesn't added.");
            }
            return newImageID;
        }
        #endregion

        #region Private class methods

        private void ClearObjects()
        {
            using (SqlCommand clearCommand = CreateClearObjectsCommand())
            {
                clearCommand.ExecuteNonQuery();
            }
        }

        private Guid GetRegion(string regionName)
        {
            Guid regionID = Guid.Empty;

            using (SqlCommand getCommand = CreateGetRegionByNameCommand(regionName))
            {
                getCommand.ExecuteNonQuery();

                object result = getCommand.Parameters["RegionID"].Value;
                regionID = (Guid)result;
            }

            return regionID;
        }

        private Guid GetCity(Guid regionID, string cityName)
        {
            Guid cityID = Guid.Empty;

            using (SqlCommand getCommand = CreateGetCityByRegionAndNameCommand(regionID, cityName))
            {
                getCommand.ExecuteNonQuery();

                object result = getCommand.Parameters["CityID"].Value;
                cityID = (Guid)result;
            }

            return cityID;
        }

        private Guid GetSection(string sectionName)
        {
            Guid sectionID = Guid.Empty;

            using (SqlCommand getCommand = CreateGetSectionByNameCommand(sectionName))
            {
                getCommand.ExecuteNonQuery();

                object result = getCommand.Parameters["SectionID"].Value;
                sectionID = (Guid)result;
            }

            return sectionID;
        }

        private Guid GetCategory(Guid sectionID, string categoryName)
        {
            Guid categoryID = Guid.Empty;

            using (SqlCommand getCommand = CreateGetCategoryBySectionAndNameCommand(sectionID, categoryName))
            {
                getCommand.ExecuteNonQuery();

                object result = getCommand.Parameters["CategoryID"].Value;
                categoryID = (Guid)result;
            }

            return categoryID;
        }

        private Guid CreateNewRegion(string regionName)
        {
            Guid regionID = Guid.Empty;

            using (SqlCommand createCommand = CreateNewRegionCommand(Guid.Empty, regionName))
            {
                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewRegionID"].Value;
                regionID = (Guid)result;
            }

            return regionID;
        }

        private Guid CreateNewCity(Guid regionID, string cityName)
        {
            Guid cityID = Guid.Empty;

            using (SqlCommand createCommand = CreateNewCityCommand(Guid.Empty, regionID, cityName))
            {
                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewCityID"].Value;
                cityID = (Guid)result;
            }

            return cityID;
        }

        private Guid CreateNewSection(string sectionName)
        {
            Guid sectionID = Guid.Empty;

            using (SqlCommand createCommand = CreateNewSectionCommand(Guid.Empty, sectionName))
            {
                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewSectionID"].Value;
                sectionID = (Guid)result;
            }

            return sectionID;
        }

        private Guid CreateNewCategory(Guid sectionID, string categoryName)
        {
            Guid categoryID = Guid.Empty;

            using (SqlCommand createCommand = CreateNewCategoryCommand(Guid.Empty, sectionID, categoryName))
            {
                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewCategoryID"].Value;
                categoryID = (Guid)result;
            }

            return categoryID;
        }

        private long CreateNewObject(long objectID, Guid sectionID, Guid categoryID, Guid regionID, Guid cityID)
        {
            long newObjectID = 0L;

            using (SqlCommand createCommand = CreateNewObjectCommand(objectID, sectionID, categoryID, regionID, cityID))
            {
                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewObjectID"].Value;
                newObjectID = (result is DBNull) ? 0L : (long)result;
            }

            return newObjectID;
        }

        private long CreateObjectImage(long objectID, long imageID, string imageUrl)
        {
            long newImageID = 0L;

            using (SqlCommand createCommand = CreateAddObjectImageCommand(objectID, imageID, imageUrl))
            {
                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewImageID"].Value;
                newImageID = (result is DBNull) ? 0L : (long)result;
            }

            return newImageID;
        }
        #endregion

        #region Command creation methods

        private SqlCommand CreateClearObjectsCommand()
        {
            SqlCommand clearCommand = new SqlCommand(AvitoDatabaseConstants.ClearObjects, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            return clearCommand;
        }

        private SqlCommand CreateGetRegionByNameCommand(string regionName)
        {
            SqlCommand getCommand = new SqlCommand(AvitoDatabaseConstants.RegionGetByName, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            getCommand.Parameters.AddWithValue("RegionName", regionName).SqlDbType = SqlDbType.NVarChar;
            getCommand.Parameters.Add("RegionID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.ReturnValue;

            return getCommand;
        }

        private SqlCommand CreateGetCityByRegionAndNameCommand(Guid regionID, string cityName)
        {
            SqlCommand getCommand = new SqlCommand(AvitoDatabaseConstants.CityGetByRegionAndName, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            getCommand.Parameters.AddWithValue("RegionID", regionID).SqlDbType = SqlDbType.UniqueIdentifier;
            getCommand.Parameters.AddWithValue("CityName", cityName).SqlDbType = SqlDbType.NVarChar;
            getCommand.Parameters.AddWithValue("CityID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.ReturnValue;

            return getCommand;
        }

        private SqlCommand CreateGetSectionByNameCommand(string sectionName)
        {
            SqlCommand getCommand = new SqlCommand(AvitoDatabaseConstants.SectionGetByName, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            getCommand.Parameters.AddWithValue("SectionName", sectionName).SqlDbType = SqlDbType.NVarChar;
            getCommand.Parameters.Add("SectionID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.ReturnValue;

            return getCommand;
        }

        private SqlCommand CreateGetCategoryBySectionAndNameCommand(Guid sectionID, string categoryName)
        {
            SqlCommand getCommand = new SqlCommand(AvitoDatabaseConstants.CategoryGetBySectionAndName, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            getCommand.Parameters.AddWithValue("SectionID", sectionID).SqlDbType = SqlDbType.UniqueIdentifier;
            getCommand.Parameters.AddWithValue("CategoryName", categoryName).SqlDbType = SqlDbType.NVarChar;
            getCommand.Parameters.Add("CategoryID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.ReturnValue;

            return getCommand;
        }

        private SqlCommand CreateNewRegionCommand(Guid regionID, string regionName)
        {
            SqlCommand createCommand = new SqlCommand(AvitoDatabaseConstants.RegionCreate, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            createCommand.Parameters.AddWithValue("RegionID", regionID).SqlDbType = SqlDbType.UniqueIdentifier;
            createCommand.Parameters.AddWithValue("RegionName", regionName).SqlDbType = SqlDbType.NVarChar;
            createCommand.Parameters.Add("NewRegionID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;

            return createCommand;
        }

        private SqlCommand CreateNewCityCommand(Guid cityID, Guid regionID, string cityName)
        {
            SqlCommand createCommand = new SqlCommand(AvitoDatabaseConstants.CityCreate, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            createCommand.Parameters.AddWithValue("CityID", cityID).SqlDbType = SqlDbType.UniqueIdentifier;
            createCommand.Parameters.AddWithValue("RegionID", regionID).SqlDbType = SqlDbType.UniqueIdentifier;
            createCommand.Parameters.AddWithValue("CityName", cityName).SqlDbType = SqlDbType.NVarChar;
            createCommand.Parameters.Add("NewCityID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;

            return createCommand;
        }

        private SqlCommand CreateNewSectionCommand(Guid sectionID, string sectionName)
        {
            SqlCommand createCommand = new SqlCommand(AvitoDatabaseConstants.SectionCreate, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            createCommand.Parameters.AddWithValue("SectionID", sectionID).SqlDbType = SqlDbType.UniqueIdentifier;
            createCommand.Parameters.AddWithValue("SectionName", sectionName).SqlDbType = SqlDbType.NVarChar;
            createCommand.Parameters.Add("NewSectionID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;

            return createCommand;
        }

        private SqlCommand CreateNewCategoryCommand(Guid categoryID, Guid sectionID, string categoryName)
        {
            SqlCommand createCommand = new SqlCommand(AvitoDatabaseConstants.CategoryCreate, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            createCommand.Parameters.AddWithValue("CategoryID", categoryID).SqlDbType = SqlDbType.UniqueIdentifier;
            createCommand.Parameters.AddWithValue("SectionID", sectionID).SqlDbType = SqlDbType.UniqueIdentifier;
            createCommand.Parameters.AddWithValue("CategoryName", categoryName).SqlDbType = SqlDbType.NVarChar;
            createCommand.Parameters.Add("NewCategoryID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;

            return createCommand;
        }

        private SqlCommand CreateNewObjectCommand(long objectID, Guid sectionID, Guid categoryID, Guid regionID, Guid cityID)
        {
            SqlCommand createCommand = new SqlCommand(AvitoDatabaseConstants.ObjectCreate, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            createCommand.Parameters.AddWithValue("ObjectID", objectID).SqlDbType = SqlDbType.BigInt;
            createCommand.Parameters.AddWithValue("SectionID", sectionID).SqlDbType = SqlDbType.UniqueIdentifier;
            createCommand.Parameters.AddWithValue("CategoryID", categoryID).SqlDbType = SqlDbType.UniqueIdentifier;
            createCommand.Parameters.AddWithValue("RegionID", regionID).SqlDbType = SqlDbType.UniqueIdentifier;
            createCommand.Parameters.AddWithValue("CityID", cityID).SqlDbType = SqlDbType.UniqueIdentifier;
            createCommand.Parameters.Add("NewObjectID", SqlDbType.BigInt).Direction = ParameterDirection.InputOutput;

            return createCommand;
        }

        private SqlCommand CreateSetMainInfoCommand(long objectID, string objectName, string objectDescription, decimal? objectPrice, string objectUrl)
        {
            SqlCommand setCommand = new SqlCommand(AvitoDatabaseConstants.ObjectSetMainInfo, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            setCommand.Parameters.AddWithValue("ObjectID", objectID).SqlDbType = SqlDbType.BigInt;
            setCommand.Parameters.AddWithValue("ObjectName", objectName).SqlDbType = SqlDbType.NVarChar;
            setCommand.Parameters.AddWithValue("ObjectDescription", objectDescription).SqlDbType = SqlDbType.NVarChar;
            setCommand.Parameters.AddWithValue("ObjectPrice", objectPrice).SqlDbType = SqlDbType.Money;
            setCommand.Parameters.AddWithValue("ObjectUrl", objectUrl).SqlDbType = SqlDbType.VarChar;

            return setCommand;
        }

        private SqlCommand CreateSetRealtyInfoCommand(long objectID, int? roomsCount = null)
        {
            SqlCommand setCommand = new SqlCommand(AvitoDatabaseConstants.ObjectSetRealtyInfo, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            setCommand.Parameters.AddWithValue("ObjectID", objectID).SqlDbType = SqlDbType.BigInt;
            setCommand.Parameters.AddWithValue("RoomsCount", roomsCount).SqlDbType = SqlDbType.Int;

            return setCommand;
        }

        private SqlCommand CreateSetTransportInfoCommand(long objectID, int? issueYear = null)
        {
            SqlCommand setCommand = new SqlCommand(AvitoDatabaseConstants.ObjectSetTransportInfo, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            setCommand.Parameters.AddWithValue("ObjectID", objectID).SqlDbType = SqlDbType.BigInt;
            setCommand.Parameters.AddWithValue("IssueYear", issueYear).SqlDbType = SqlDbType.Int;

            return setCommand;
        }

        private SqlCommand CreateAddObjectImageCommand(long objectID, long imageID, string imageUrl)
        {
            SqlCommand addCommand = new SqlCommand(AvitoDatabaseConstants.ObjectAddImage, InnerConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            addCommand.Parameters.AddWithValue("ImageID", imageID).SqlDbType = SqlDbType.BigInt;
            addCommand.Parameters.AddWithValue("ObjectID", objectID).SqlDbType = SqlDbType.BigInt;
            addCommand.Parameters.AddWithValue("ImageUrl", imageUrl).SqlDbType = SqlDbType.VarChar;
            addCommand.Parameters.Add("NewImageID", SqlDbType.BigInt).Direction = ParameterDirection.InputOutput;

            return addCommand;
        }
        #endregion
    }
}