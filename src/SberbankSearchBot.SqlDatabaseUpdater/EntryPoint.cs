﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    internal class EntryPoint
    {
        #region The entry point

        internal static void Main(string[] args)
        {
            Console.Write("Введите строку подключения к целевой БД: ");
            string connectionString = Console.ReadLine();

            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = AvitoConstants.AvitoDBConnectionString;
            }

            string databasePath = GetJsonDatabaseFilePath();

            bool hasErrors = false;
            try
            {
                JArray jsonData = LoadJsonObjectsData(databasePath);
                SqlConnection connection = CreateDatabaseConnection(connectionString);
                AvitoDatabaseContext context = CreateDatabaseContext(connection);

                using (AvitoSqlDatabaseUpdater updater = new AvitoSqlDatabaseUpdater(jsonData, context))
                {
                    updater.UploadData();
                }
            }
            catch (Exception exc)
            {
                hasErrors = true;
                Console.WriteLine("Exception occured: {0}\r\n{1}", exc.Message, exc);
            }
            finally
            {
                if (hasErrors)
                {
                    Console.WriteLine("Updating stopped by error");
                }
                else
                {
                    Console.WriteLine("Updating finished successfully");
                }

                Console.ReadKey(true);
            }
        }
        #endregion

        #region Private class methods

        private static string GetJsonDatabaseFilePath()
        {
            string currentDirectory = Directory.GetCurrentDirectory();

            string filePath = string.Empty;

            DirectoryInfo current = new DirectoryInfo(currentDirectory);
            DirectoryInfo parent = current.Parent?.Parent?.Parent;
            if (parent != null)
            {
                filePath = Path.Combine(parent.FullName, AvitoConstants.JsonDatabaseRelativePath);
            }

            return filePath;
        }

        private static JArray LoadJsonObjectsData(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("Json database file path doesn't exist.", filePath);
            }

            JArray objectsDataArray = null;

            JsonLoadSettings loadSettings = new JsonLoadSettings
            {
                CommentHandling = CommentHandling.Ignore,
                LineInfoHandling = LineInfoHandling.Ignore
            };

            using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8))
            {
                objectsDataArray = JArray.Load(new JsonTextReader(reader), loadSettings);
            }

            return objectsDataArray;
        }

        private static SqlConnection CreateDatabaseConnection(string connectionString)
        {
            SqlConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Exception occured: {0}\r\n{1}", exc.Message, exc);
            }

            return connection;
        }

        private static AvitoDatabaseContext CreateDatabaseContext(SqlConnection connection)
        {
            return new AvitoDatabaseContext(connection);
        }
        #endregion
    }
}