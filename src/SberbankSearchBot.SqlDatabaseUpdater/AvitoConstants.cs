﻿using System;

namespace SberbankSearchBot
{
    internal static class AvitoConstants
    {
        #region Constants

        public const string JsonDatabaseRelativePath = "SberbankSearchBot\\App_Data\\ObjectsData.json";

        public const string AvitoDBConnectionString = "Data Source=DKALINOV-PC;Initial Catalog=AvitoDB;Integrated Security=True";
        #endregion
    }
}