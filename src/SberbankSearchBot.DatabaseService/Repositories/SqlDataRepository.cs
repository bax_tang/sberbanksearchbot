﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SberbankSearchBot.DatabaseService
{
    public class SqlDataRepository : IDataRepository, IDisposable
    {
        #region Fields and properties

        private AvitoDbContext Context;

        private StaticDataRepository StaticRepository;
        #endregion

        #region Constructors

        public SqlDataRepository(AvitoDbContext context, StaticDataRepository staticRepository)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context), "Database context can't be null.");
            }
            if (staticRepository == null)
            {
                throw new ArgumentNullException(nameof(staticRepository), "Static data repository can't be null.");
            }

            Context = context;
            StaticRepository = staticRepository;
        }
        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            Context?.Dispose();
            Context = null;
        }
        #endregion

        #region Public class methods

        public IList<DepartmentInfo> GetDepartments()
        {
            return Context.GetDepartments();
        }

        public void CreateObjects(Guid departmentID, IList<ObjectInfo> newObjects)
        {
            if (departmentID == Guid.Empty)
            {
                throw new ArgumentOutOfRangeException(nameof(departmentID), "Department ID can't be empty.");
            }
            if (newObjects == null)
            {
                throw new ArgumentNullException(nameof(newObjects), "Objects collection can't be null.");
            }

            if (newObjects.Count > 0)
            {
                for (int index = 0; index < newObjects.Count; ++index)
                {
                    ObjectInfo currentObject = newObjects[index];

                    CreateObject(departmentID, currentObject);
                }
            }
        }

        public void CreateObject(Guid departmentID, ObjectInfo objectInfo)
        {
            StaticRepository.CreateObject(Context, departmentID, objectInfo);
        }

        public void DeleteDepartmentObjects(Guid departmentID)
        {
            Context.DeleteDepartmentObjects(departmentID);
            StaticRepository.RemoveObjects(departmentID);
        }

        public bool ObjectExists(Guid departmentID, long objectID)
        {
            return StaticRepository.ObjectExists(departmentID, objectID);
        }
        #endregion
    }
}