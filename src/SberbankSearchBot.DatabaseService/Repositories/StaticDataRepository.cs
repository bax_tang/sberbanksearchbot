﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace SberbankSearchBot.DatabaseService
{
    public class StaticDataRepository : IDataRepository
    {
        #region Fields and properties

        private readonly ConcurrentDictionary<Guid, HashSet<long>> ObjectsStore;

        private ConcurrentDictionary<string, Guid> SectionsStore;

        private ConcurrentDictionary<Tuple<string, string>, Guid> CategoriesStore;

        private ConcurrentDictionary<string, Guid> RegionsStore;

        private ConcurrentDictionary<Tuple<string, string>, Guid> CitiesStore;
        #endregion

        #region Constructors

        public StaticDataRepository()
        {
            ObjectsStore = new ConcurrentDictionary<Guid, HashSet<long>>();
            SectionsStore = new ConcurrentDictionary<string, Guid>();
            CategoriesStore = new ConcurrentDictionary<Tuple<string, string>, Guid>();
            RegionsStore = new ConcurrentDictionary<string, Guid>();
            CitiesStore = new ConcurrentDictionary<Tuple<string, string>, Guid>();
        }
        #endregion

        #region Public class methods

        public bool CreateObject(AvitoDbContext context, Guid departmentID, ObjectInfo newObject)
        {
            long objectID = newObject.ObjectID;
            Guid sectionID = GetSectionID(context, newObject.Section);
            Guid categoryID = GetCategoryID(context, newObject.Section, newObject.Category);
            Guid regionID = GetRegionID(context, newObject.Region);
            Guid cityID = GetCityID(context, newObject.Region, newObject.City);

            long newObjectID = context.CreateObject(objectID, departmentID, sectionID, categoryID, regionID, cityID);
            bool created = (newObjectID != -1L);
            if (created)
            {
                context.SetObjectData(newObject);
                AddObject(departmentID, newObjectID);
            }
            return created;
        }

        public bool AddObject(Guid departmentID, long objectID)
        {
            HashSet<long> objectsSet = ObjectsStore.GetOrAdd(departmentID, CreateNewObjectsSet);

            return objectsSet.Add(objectID);
        }

        public bool ObjectExists(Guid departmentID, long objectID)
        {
            HashSet<long> objectsSet = ObjectsStore.GetOrAdd(departmentID, CreateNewObjectsSet);

            return objectsSet.Contains(objectID);
        }

        public bool RemoveObject(Guid departmentID, long objectID)
        {
            HashSet<long> objectsSet = ObjectsStore.GetOrAdd(departmentID, CreateNewObjectsSet);

            return objectsSet.Remove(objectID);
        }

        public IEnumerable<object> GetObjectCounts()
        {
            foreach (var entry in ObjectsStore)
            {
                yield return new
                {
                    DepartmentID = entry.Key,
                    Count = entry.Value.Count
                };
            }
        }

        public void RemoveObjects()
        {
            foreach (var entry in ObjectsStore)
            {
                entry.Value.Clear();
            }
        }

        public void RemoveObjects(Guid departmentID)
        {
            HashSet<long> objectsSet = ObjectsStore.GetOrAdd(departmentID, CreateNewObjectsSet);

            objectsSet.Clear();
        }
        #endregion

        #region Private class methods

        private Guid GetSectionID(AvitoDbContext context, string sectionName)
        {
            return SectionsStore.GetOrAdd(sectionName, context.GetOrCreateSection);
        }

        private Guid GetCategoryID(AvitoDbContext context, string sectionName, string categoryName)
        {
            var categoryKey = Tuple.Create(sectionName, categoryName);

            Guid categoryID = Guid.Empty;
            bool exists = CategoriesStore.TryGetValue(categoryKey, out categoryID);
            if (!exists)
            {
                Guid sectionID = GetSectionID(context, sectionName);
                categoryID = context.GetOrCreateCategory(sectionID, categoryName);
                CategoriesStore[categoryKey] = categoryID;
            }
            return categoryID;
        }

        private Guid GetRegionID(AvitoDbContext context, string regionName)
        {
            return RegionsStore.GetOrAdd(regionName, context.GetOrCreateRegion);
        }

        private Guid GetCityID(AvitoDbContext context, string regionName, string cityName)
        {
            var cityKey = Tuple.Create(regionName, cityName);

            Guid cityID = Guid.Empty;
            bool exists = CitiesStore.TryGetValue(cityKey, out cityID);
            if (!exists)
            {
                Guid regionID = GetRegionID(context, regionName);
                cityID = context.GetOrCreateCity(regionID, cityName);
                CitiesStore[cityKey] = cityID;
            }
            return cityID;
        }

        private static HashSet<long> CreateNewObjectsSet(Guid departmentID)
        {
            return new HashSet<long>();
        }
        #endregion
    }
}