﻿using System;

namespace SberbankSearchBot.DatabaseService
{
    public interface IDataRepository
    {
        bool ObjectExists(Guid departmentID, long objectID);
    }
}