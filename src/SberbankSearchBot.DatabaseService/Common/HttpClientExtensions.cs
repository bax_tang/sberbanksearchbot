﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using HtmlAgilityPack;

namespace SberbankSearchBot.DatabaseService
{
    public static class HttpClientExtensions
    {
        #region Public class methods

        public static HtmlDocument GetDocument(this HttpClient client, Uri requestUri)
        {
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client), "Http client can't be null.");
            }
            if (requestUri == null)
            {
                throw new ArgumentNullException(nameof(requestUri), "Request URI can't be null.");
            }
            
            Task<HttpResponseMessage> responseTask = client.GetAsync(requestUri);
            responseTask.Wait();

            HttpResponseMessage response = responseTask.Result;
			if (response.IsSuccessStatusCode)
			{
				HttpContent content = response.Content;
				string charSet = content.Headers.ContentType.CharSet ?? "utf-8";
				Encoding contentEncoding = Encoding.GetEncoding(charSet);

				Stream contentStream = content.ReadAsStreamAsync().Result;

				HtmlDocument document = new HtmlDocument();
				document.Load(contentStream, contentEncoding);
				return document;
			}

			return null;
        }
        #endregion
    }
}