﻿using System;

namespace SberbankSearchBot.DatabaseService
{
    public static class ComparableExtensions
    {
        #region Public class methods

        public static bool IsBetween<TValue>(this TValue value, TValue left, TValue right) where TValue : IComparable<TValue>
        {
            bool greaterOrEqualLeft = value.CompareTo(left) >= 0;
            bool lessThanRight = value.CompareTo(right) < 0;

            return greaterOrEqualLeft && lessThanRight;
        }
        #endregion
    }
}