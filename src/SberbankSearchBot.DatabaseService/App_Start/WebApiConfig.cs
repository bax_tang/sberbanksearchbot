﻿using System;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

using Autofac;
using Autofac.Integration.WebApi;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

using NLog;

namespace SberbankSearchBot.DatabaseService
{
    public static class WebApiConfig
    {
        #region Constants and fields

        private static IContainer Container;
        #endregion

        #region Public class methods

        public static void Register(HttpConfiguration config)
        {
            UpdateCurrentSerializerSettings(config);

            JsonConvert.DefaultSettings = GetDefaultSettings;

            IContainer container = CreateContainer(config);
            Container = container;

            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

            ServiceConfiguration configuration = container.Resolve<ServiceConfiguration>();
            if (configuration.AutoStart)
            {
                container.Resolve<DatabaseUpdatingService>().StartProcessing();
            }
        }
        #endregion

        #region Private class methods

        private static void UpdateCurrentSerializerSettings(HttpConfiguration config)
        {
            JsonSerializerSettings currentSettings = config.Formatters.JsonFormatter.SerializerSettings;

            currentSettings.ContractResolver = new DefaultContractResolver();
            currentSettings.DateFormatString = "dd.MM.yyyy HH:mm:ss";
            currentSettings.Formatting = Formatting.None;
            currentSettings.NullValueHandling = NullValueHandling.Ignore;
        }

        private static JsonSerializerSettings GetDefaultSettings()
        {
            IContractResolver resolver = new DefaultContractResolver();

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                ContractResolver = resolver,
                DateFormatString = "dd.MM.yyyy HH:mm:ss",
                Formatting = Formatting.None,
                NullValueHandling = NullValueHandling.Ignore
            };

            return settings;
        }

        private static IContainer CreateContainer(HttpConfiguration config)
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterWebApiModelBinderProvider();

            builder.RegisterInstance(config);

            builder.Register(GetConfiguration).SingleInstance();
            builder.Register(GetLogger).As<ILoggerBase, ILogger, Logger>().InstancePerDependency();
            builder.Register(CreateBotClient).InstancePerDependency();
            builder.Register(CreateDatabaseConnection).InstancePerDependency();
            builder.Register(CreateDatabaseContext).InstancePerDependency();
            builder.RegisterType<AvitoParserFactory>().InstancePerDependency();
            builder.RegisterType<StaticDataRepository>().SingleInstance();
            builder.RegisterType<SqlDataRepository>().InstancePerDependency();
            builder.RegisterType<DatabaseUpdatingService>().SingleInstance();
            builder.RegisterType<DatabaseUpdatingWorker>().InstancePerDependency();

            var currentAssembly = typeof(WebApiConfig).Assembly;
            builder.RegisterApiControllers(currentAssembly).InstancePerRequest();
            //builder.RegisterType<ServiceController>().InstancePerRequest();

            IContainer container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            return container;
        }

        private static ServiceConfiguration GetConfiguration(IComponentContext context)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/DatabaseService.json");
            string fileData = File.ReadAllText(filePath, Encoding.UTF8);

            ServiceConfiguration configuration = JsonConvert.DeserializeObject<ServiceConfiguration>(fileData);
            ServiceConfiguration.SetInstance(configuration);
            return configuration;
        }

        private static Logger GetLogger(IComponentContext context)
        {
            return LogManager.GetCurrentClassLogger();
        }

        private static BotMaintenanceClient CreateBotClient(IComponentContext context)
        {
            ILogger logger = context.Resolve<ILogger>();

            return new BotMaintenanceClient(logger, new HttpClient());
        }

        private static SqlConnection CreateDatabaseConnection(IComponentContext context)
        {
            ServiceConfiguration configuration = context.Resolve<ServiceConfiguration>();

            string connectionString = configuration.Database.ConnectionString;

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        private static AvitoDbContext CreateDatabaseContext(IComponentContext context)
        {
            ServiceConfiguration configuration = context.Resolve<ServiceConfiguration>();
            SqlConnection connection = context.Resolve<SqlConnection>();

            return new AvitoDbContext(connection, configuration.Database.ExecutionTimeout);
        }
        #endregion
    }
}