﻿using System;
using System.Web.Http;

using Newtonsoft.Json;

namespace SberbankSearchBot.DatabaseService
{
    public class ServiceController : ApiController
    {
        #region Constants and fields

        private DatabaseUpdatingService UpdatingService;
        #endregion

        #region Constructors

        public ServiceController(DatabaseUpdatingService updatingService)
        {
            if (updatingService == null)
            {
                throw new ArgumentNullException(nameof(updatingService), "Database updating service can't be null.");
            }

            UpdatingService = updatingService;
        }
        #endregion

        #region Api methods

        [HttpGet]
        public IHttpActionResult Status()
        {
            var statusInfo = UpdatingService.GetStatusInfo();
            var settings = JsonConvert.DefaultSettings.Invoke();

            return Json(statusInfo, settings);
        }

        [HttpPost]
        public IHttpActionResult Start()
        {
            UpdatingService.StartProcessing();
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Suspend()
        {
            UpdatingService.SuspendProcessing();
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Resume()
        {
            UpdatingService.ResumeProcessing();
            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Stop()
        {
            UpdatingService.StopProcessing();
            return Ok();
        }
        #endregion
    }
}