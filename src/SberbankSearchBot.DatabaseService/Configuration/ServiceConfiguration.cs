﻿using System;
using System.Text.RegularExpressions;

namespace SberbankSearchBot.DatabaseService
{
    public class ServiceConfiguration
    {
        #region Constants, fields and properties

        private const double DefaultTimeSpanOffsetInHours = 5.0;

        private const double ProcessingIntervalInHours = 1.0;

        private const string YearExpression = "(?:(?:[1]{1}\\d{1}\\d{1}\\d{1})|(?:[2]{1}\\d{3}))";

        private static ServiceConfiguration ConfigInstance;

        public static ServiceConfiguration Instance => ConfigInstance;

        private Regex YearRegexCore;

        /// <summary>
        /// Возвращает или задаёт значение, определяющее режим запуска механизма обновления БД при запуске приложения.
        /// Значение по умолчанию - true (автоматический режим).
        /// </summary>
        public bool AutoStart { get; set; } = true;
        /// <summary>
        /// Возвращает или задаёт базовый URI для доступа к сайту Avito.
        /// Значение по умолчанию - https://www.avito.ru/sberbank/rossiya.
        /// </summary>
        public string AvitoBaseUri { get; set; } = "https://www.avito.ru/sberbank/rossiya";
        /// <summary>
        /// Возвращает или задаёт базовый URI для доступа к боту.
        /// Значение по умолчанию - http://realtysearchbot.azurewebsites.net/.
        /// </summary>
        public string BotBaseUri { get; set; } = "http://realtysearchbot.azurewebsites.net/";
        /// <summary>
        /// Возвращает или задаёт объект, содержащий параметры базы данных AvitoObjects.
        /// </summary>
        public DatabaseConfiguration Database { get; set; } = new DatabaseConfiguration();
        /// <summary>
        /// Возвращает или задаёт интервал смещения времени относительно <see cref="DateTime.UtcNow"/> в часах.
        /// Значение по умолчанию - +5 часов.
        /// </summary>
        public TimeSpan OffsetTimeSpan { get; set; } = TimeSpan.FromHours(DefaultTimeSpanOffsetInHours);
        /// <summary>
        /// Возвращает или задаёт интервал времени между запусками процесса обновления.
        /// Значение по умолчанию - 1 час.
        /// </summary>
        public TimeSpan ProcessingInterval { get; set; } = TimeSpan.FromHours(ProcessingIntervalInHours);
        /// <summary>
        /// Возвращает или задаёт минимальное значение текущего часа, при котором выполняется полное обновление БД.
        /// Значение по умолчанию - 4 часа утра.
        /// </summary>
        public int MinFullUpdateHour { get; set; } = 4;
        /// <summary>
        /// Возвращает или задаёт максимальное значение текущего часа, при котором выполняется полное обновление БД.
        /// Значение по умолчанию - 6 часов утра.
        /// </summary>
        public int MaxFullUpdateHour { get; set; } = 6;
        /// <summary>
        /// Возвращает или задаёт минимальное значение текущего часа, начиная с которого выполняется кумулятивное обновление.
        /// Значение по умолчанию - 6 часов утра.
        /// </summary>
        public int MinCumulativeUpdateHour { get; set; } = 6;
        /// <summary>
        /// Возвращает или задаёт максимальное значение текущего часа, до наступления которого выполняется кумулятивное обновление.
        /// Значение по умолчанию - 23 часа ночи.
        /// </summary>
        public int MaxCumulativeUpdateHour { get; set; } = 23;
        /// <summary>
        /// Возвращает экземпляр регулярного выражения для выделения года
        /// </summary>
        public Regex YearRegex
        {
            get
            {
                Regex yearRegex = YearRegexCore;
                if (yearRegex == null)
                {
                    YearRegexCore = yearRegex = new Regex(YearExpression, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                }
                return yearRegex;
            }
        }
        #endregion

        #region Constructors

        static ServiceConfiguration()
        {
            ConfigInstance = new ServiceConfiguration();
        }

        public ServiceConfiguration() { }
        #endregion

        #region Public class methods

        public DateTime GetDateTimeWithOffset()
        {
            return DateTime.UtcNow + OffsetTimeSpan;
        }

        public static void SetInstance(ServiceConfiguration newInstance)
        {
            if (newInstance == null)
            {
                throw new ArgumentNullException(nameof(newInstance), "Service configuration instance can't be null.");
            }

            ConfigInstance = newInstance;
        }
        #endregion
    }
}