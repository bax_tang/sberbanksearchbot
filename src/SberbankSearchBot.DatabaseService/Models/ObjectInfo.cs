﻿using System;
using System.Collections.Generic;

namespace SberbankSearchBot.DatabaseService
{
    public class ObjectInfo
    {
        #region Constants, fields and properties

        public static readonly ObjectInfo Empty;

        public long ObjectID;

        public string Name;

        public string Url;

        public string Description;

        public string Region;

        public string City;

        public string Section;

        public string Category;

        public double? Price;

        public int? IssueYear;

        public IList<ObjectImage> Images;

        public decimal? DecimalPrice
        {
            get
            {
                double? price = Price;
                return (price.HasValue) ? new decimal?((decimal)price) : null;
            }
        }
        #endregion

        #region Constructors

        static ObjectInfo()
        {
            Empty = new ObjectInfo(-1L, string.Empty, string.Empty);
        }

        public ObjectInfo(long objectID, string objectName, string objectUrl)
        {
            ObjectID = objectID;
            Name = objectName;
            Url = objectUrl;
        }
        #endregion

        #region object methods overriding

        public override string ToString()
        {
            return string.Concat(ObjectID, ": ", Name);
        }
        #endregion
    }
}