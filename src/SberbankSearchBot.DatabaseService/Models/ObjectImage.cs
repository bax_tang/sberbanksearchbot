﻿using System;

namespace SberbankSearchBot.DatabaseService
{
    public class ObjectImage
    {
        #region Constants and fields

        public readonly long ImageID;

        public readonly string ImageUrl;
        #endregion

        #region Constructors

        public ObjectImage(long imageID, string imageUrl)
        {
            if (imageID <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(imageID), "Invalid image identifier.");
            }
            if (string.IsNullOrEmpty(imageUrl))
            {
                throw new ArgumentNullException(nameof(imageUrl), "Image URL can't be null or empty.");
            }

            ImageID = imageID;
            ImageUrl = imageUrl;
        }
        #endregion

        #region object methods overriding

        public override string ToString()
        {
            return string.Concat(ImageID, ": ", ImageUrl);
        }
        #endregion
    }
}