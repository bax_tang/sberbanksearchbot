﻿using System;

namespace SberbankSearchBot.DatabaseService
{
    public class DepartmentInfo
    {
        #region Constants, fields and properties

        public Guid DepartmentID { get; set; }

        public string Name { get; set; }

        public string AvitoPageUrl { get; set; }

        public string BotServiceUrl { get; set; }
        #endregion

        #region Constructors

        public DepartmentInfo() { }
        #endregion

        #region object methods overriding

        public override string ToString()
        {
            return string.Format("{0}: {1}", Name, AvitoPageUrl);
        }
        #endregion
    }
}