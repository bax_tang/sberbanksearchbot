﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using NLog;

namespace SberbankSearchBot.DatabaseService
{
    public class DatabaseUpdatingService
    {
        #region Constants, fields and properties

        private readonly ServiceConfiguration Configuration;

        private readonly ILogger Logger;

        private StaticDataRepository StaticRepository;

        private Func<DatabaseUpdatingWorker> WorkerFactory;

        private bool IsSuspended = false;

        private CancellationTokenSource CancellationSource;

        private Task ProcessingTask;

        private DateTime StartTime;

        private DateTime NextUpdateTime;
        #endregion

        #region Constructors

        public DatabaseUpdatingService(ServiceConfiguration configuration, ILogger logger, StaticDataRepository staticRepository, Func<DatabaseUpdatingWorker> workerFactory)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration), "Service configuration can't be null.");
            }
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger), "Logger can't be null.");
            }
            if (staticRepository == null)
            {
                throw new ArgumentNullException(nameof(staticRepository), "Static data repository can't be null.");
            }
            if (workerFactory == null)
            {
                throw new ArgumentNullException(nameof(workerFactory), "Worker factory can't be null.");
            }

            Configuration = configuration;
            Logger = logger;
            StaticRepository = staticRepository;
            WorkerFactory = workerFactory;

            CancellationSource = new CancellationTokenSource();
            NextUpdateTime = configuration.GetDateTimeWithOffset().AddHours(-1.0);
        }
        #endregion

        #region Public class methods

        public object GetStatusInfo()
        {
            bool isCanceled = CancellationSource?.IsCancellationRequested ?? false;
            bool isSuspended = IsSuspended;
            string taskStatus = null;
            TimeSpan aliveTime = TimeSpan.Zero;
            DateTime? nextUpdateTime = null;

            bool taskExists = (ProcessingTask != null);
            if (taskExists)
            {
                taskStatus = ProcessingTask.Status.ToString();
                aliveTime = (DateTime.Now - StartTime);
                nextUpdateTime = NextUpdateTime;
            }

            var statusInfo = new
            {
                IsCanceled = isCanceled,
                IsSuspended = isSuspended,
                TaskExists = taskExists,
                TaskStatus = taskStatus,
                AliveTime = aliveTime,
                NextUpdateTime = nextUpdateTime,
                ObjectCounts = StaticRepository.GetObjectCounts()
            };
            return statusInfo;
        }

        public bool StartProcessing()
        {
            CancellationTokenSource cancellationSource = new CancellationTokenSource();
            CancellationSource = cancellationSource;
            ProcessingTask = Task.Run(new Action(ProcessDatabaseUpdating), cancellationSource.Token);

            DateTime nextRunTime = Configuration.GetDateTimeWithOffset() + Configuration.ProcessingInterval;
            NextUpdateTime = nextRunTime;

            StartTime = DateTime.Now;
            return true;
        }

        public void SuspendProcessing()
        {
            IsSuspended = true;
        }

        public void ResumeProcessing()
        {
            IsSuspended = false;
        }

        public bool StopProcessing()
        {
            CancellationSource.Cancel();
            return true;
        }
        #endregion

        #region Private class methods

        private void ProcessDatabaseUpdating()
        {
            bool fullUpdateExecuted = false;
            ServiceConfiguration configuration = Configuration;

            while (true)
            {
                if (CancellationSource.IsCancellationRequested) break;

                Thread.Sleep(configuration.ProcessingInterval);

                if (IsSuspended) continue;

                if (CancellationSource.IsCancellationRequested) break;

                DateTime currentTime = configuration.GetDateTimeWithOffset();
                NextUpdateTime = currentTime + configuration.ProcessingInterval;

                if (currentTime.Hour.IsBetween(configuration.MinCumulativeUpdateHour, configuration.MaxCumulativeUpdateHour))
                {
                    fullUpdateExecuted = false;
                    ExecuteCumulativeUpdate();
                }
                else if (currentTime.Hour.IsBetween(configuration.MinFullUpdateHour, configuration.MaxFullUpdateHour))
                {
                    if (fullUpdateExecuted) continue;

                    fullUpdateExecuted = true;
                    ExecuteFullUpdate();
                }
                else continue;
            }
        }

        private void ExecuteCumulativeUpdate() => UpdateDatabase(false);

        private void ExecuteFullUpdate() => UpdateDatabase(true);

        internal void UpdateDatabase(bool deleteExistingObjects)
        {
            try
            {
                using (DatabaseUpdatingWorker worker = WorkerFactory.Invoke())
                {
                    worker.UpdateDatabase(deleteExistingObjects);
                }
            }
            catch (Exception exc)
            {
                Logger.Error(exc, exc.Message);
            }
        }
        #endregion
    }
}