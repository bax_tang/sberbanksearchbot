﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace SberbankSearchBot.DatabaseService
{
    public class DatabaseUpdatingWorker : IDisposable
    {
        #region Fields and properties

        private Random Generator = new Random();

        private AvitoParserFactory ParserFactory;

        private SqlDataRepository DataRepository;

        private BotMaintenanceClient BotClient;
        #endregion

        #region Constructors

        public DatabaseUpdatingWorker(
            AvitoParserFactory parserFactory,
            SqlDataRepository dataRepository,
            BotMaintenanceClient botClient)
        {
            if (parserFactory == null)
            {
                throw new ArgumentNullException(nameof(parserFactory), "Parser factory can't be null.");
            }
            if (dataRepository == null)
            {
                throw new ArgumentNullException(nameof(dataRepository), "Data repository can't be null.");
            }
            if (botClient == null)
            {
                throw new ArgumentNullException(nameof(botClient), "Bot client can't be null.");
            }

            ParserFactory = parserFactory;
            DataRepository = dataRepository;
            BotClient = botClient;
        }
        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            DataRepository?.Dispose();
            DataRepository = null;
        }
        #endregion

        #region Public class methods

        public void UpdateDatabase(bool deleteExistingObjects)
        {
            IList<DepartmentInfo> departments = DataRepository.GetDepartments();
            if (departments.Count > 0)
            {
                for (int departmentIndex = 0; departmentIndex < departments.Count; ++departmentIndex)
                {
                    DepartmentInfo currentDepartment = departments[departmentIndex];
                    UpdateDepartmentObjects(currentDepartment, deleteExistingObjects);
                }
            }
        }
        #endregion

        #region Private class methods

        private void UpdateDepartmentObjects(DepartmentInfo department, bool deleteExistingObjects)
        {
            Random generator = new Random();
            SqlDataRepository repository = DataRepository;
            
            Guid departmentID = department.DepartmentID;

            if (deleteExistingObjects)
            {
                repository.DeleteDepartmentObjects(departmentID);
            }

            AvitoWebParser parser = ParserFactory.CreateParser(department.AvitoPageUrl);
            List<ObjectInfo> parsedObjects = ParseObjects(parser, departmentID);
            DataRepository.CreateObjects(departmentID, parsedObjects);

            if (string.IsNullOrEmpty(department.BotServiceUrl)) return;
            else
            {
                BotClient.ResetBotRepository(department.BotServiceUrl);
            }
        }

        private List<ObjectInfo> ParseObjects(AvitoWebParser parser, Guid departmentID)
        {
            Random generator = Generator;
            SqlDataRepository repository = DataRepository;

            int pagesCount = parser.GetPagesCount();

            List<ObjectInfo> parsedObjects = new List<ObjectInfo>(50 * pagesCount);
            for (int pageIndex = 1; pageIndex <= pagesCount; ++pageIndex)
            {
                var objectsFromPage = parser.GetObjectsFromPage(pageIndex);
                foreach (ObjectInfo parsedObject in objectsFromPage)
                {
                    if (parsedObject.ObjectID <= 0L) continue;
                    if (repository.ObjectExists(departmentID, parsedObject.ObjectID)) continue;

                    if (parser.FillAdditionalInfo(parsedObject))
                    {
                        parsedObjects.Add(parsedObject);
                    }

                    Thread.Sleep(generator.Next(3500, 4000));
                }
            }
            return parsedObjects;
        }
        #endregion
    }
}