﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

using NLog;

namespace SberbankSearchBot.DatabaseService
{
    public class BotMaintenanceClient : IDisposable
    {
        #region Constants and fields

        private ILogger Logger;

        private HttpClient InnerClient;
        #endregion

        #region Constructors

        public BotMaintenanceClient(ILogger logger, HttpClient client)
        {
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger), "Logger can't be null.");
            }
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client), "Inner HttpClient can't be null.");
            }

            Logger = logger;
            InnerClient = client;
        }
        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            InnerClient?.Dispose();
        }
        #endregion

        #region Public class methods

        public bool ResetBotRepository(string botServiceUrl)
        {
            if (string.IsNullOrEmpty(botServiceUrl))
            {
                throw new ArgumentNullException(nameof(botServiceUrl), "Bot service url can't be null or empty.");
            }

            bool result = false;

            string requestUrl = BuildRequestUrl(botServiceUrl, "api/maintenance/clear");
            var request = new HttpRequestMessage(HttpMethod.Post, requestUrl);
            try
            {
                var responseTask = InnerClient.SendAsync(request);
                responseTask.Wait(TimeSpan.FromSeconds(5.0));
                result = responseTask.Result.IsSuccessStatusCode;
            }
            catch (Exception exc)
            {
                Logger.Error(exc, exc.Message);
                result = false;
            }

            return result;
        }
        #endregion

        #region Private class methods

        private string BuildRequestUrl(string serviceUrl, string actionUrl)
        {
            string requestUrl = null;

            if (serviceUrl[serviceUrl.Length - 1] != '/')
            {
                requestUrl = string.Concat(serviceUrl, "/", actionUrl);
            }
            else
            {
                requestUrl = string.Concat(serviceUrl, actionUrl);
            }

            return requestUrl;
        }
        #endregion
    }
}