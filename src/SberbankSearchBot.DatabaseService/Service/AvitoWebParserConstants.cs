﻿using System;

namespace SberbankSearchBot.DatabaseService
{
    internal static class AvitoWebParserConstants
    {
        #region Constants and fields

        public const string ItemNodePath = "//div[@class='item item_table clearfix js-catalog-item-enum c-b-0']";
        #endregion
    }
}