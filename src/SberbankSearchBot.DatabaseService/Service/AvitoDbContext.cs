﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SberbankSearchBot.DatabaseService
{
    public class AvitoDbContext : IDisposable
    {
        #region Constants, fields and properties

        private SqlConnection Connection;

        private int ExecutionTimeout;
        #endregion

        #region Constructors

        public AvitoDbContext(SqlConnection connection, int executionTimeout)
        {
            if (connection == null)
            {
                throw new ArgumentNullException(nameof(connection), "Database connection can't be null.");
            }
            if (executionTimeout < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(executionTimeout), "Execution timeout can't be less than zero.");
            }

            Connection = connection;
            ExecutionTimeout = executionTimeout;
        }
        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            Connection?.Dispose();
            Connection = null;
        }
        #endregion

        #region Public class methods

        public IList<DepartmentInfo> GetDepartments()
        {
            IList<DepartmentInfo> departments = GetDepartments(Connection);

            return departments ?? new List<DepartmentInfo>();
        }

        public void DeleteAllExistingObjects()
        {
            DeleteAllObjects(Connection);
        }

        public void DeleteDepartmentObjects(Guid departmentID)
        {
            DeleteObjects(Connection, departmentID);
        }

        public bool ObjectExists(long objectID)
        {
            return GetObjectStatus(Connection, objectID);
        }

        public IList<long> GetObjectIdentifiers()
        {
            IList<long> identifiers = GetObjectIdentifiers(Connection);

            return identifiers ?? new List<long>();
        }

        public Guid GetOrCreateSection(string sectionName)
        {
            Guid sectionID = GetSection(Connection, sectionName);
            if (sectionID == Guid.Empty)
            {
                sectionID = CreateSection(Connection, sectionID, sectionName);
            }
            return sectionID;
        }

        public Guid GetOrCreateCategory(Guid sectionID, string categoryName)
        {
            Guid categoryID = GetCategory(Connection, sectionID, categoryName);
            if (categoryID == Guid.Empty)
            {
                categoryID = CreateCategory(Connection, sectionID, categoryID, categoryName);
            }
            return categoryID;
        }

        public Guid GetOrCreateRegion(string regionName)
        {
            Guid regionID = GetRegion(Connection, regionName);
            if (regionID == Guid.Empty)
            {
                regionID = CreateRegion(Connection, regionID, regionName);
            }
            return regionID;
        }

        public Guid GetOrCreateCity(Guid regionID, string cityName)
        {
            Guid cityID = GetCity(Connection, regionID, cityName);
            if (cityID == Guid.Empty)
            {
                cityID = CreateCity(Connection, regionID, cityID, cityName);
            }
            return cityID;
        }
        
        public long CreateObject(long objectID, Guid departmentID, Guid sectionID, Guid categoryID, Guid regionID, Guid cityID)
        {
            #region arguments checking
            if (objectID <= 0L)
            {
                throw new ArgumentOutOfRangeException(nameof(objectID), "Invalid object identifier.");
            }
            if (departmentID == Guid.Empty)
            {
                throw new ArgumentOutOfRangeException(nameof(departmentID), "Invalid department identifier.");
            }
            if (sectionID == Guid.Empty)
            {
                throw new ArgumentOutOfRangeException(nameof(sectionID), "Invalid section identifier.");
            }
            if (categoryID == Guid.Empty)
            {
                throw new ArgumentOutOfRangeException(nameof(categoryID), "Invalid category identifier.");
            }
            if (regionID == Guid.Empty)
            {
                throw new ArgumentOutOfRangeException(nameof(regionID), "Invalid region identifier.");
            }
            if (cityID == Guid.Empty)
            {
                throw new ArgumentOutOfRangeException(nameof(cityID), "Invalid city identifier.");
            }
            #endregion

            long newObjectID = 0L;
            
            using (SqlCommand createCommand = new SqlCommand(AvitoDb.ObjectCreate, Connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                createCommand.Parameters.AddWithValue("ObjectID", objectID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("DepartmentID", departmentID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("SectionID", sectionID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("CategoryID", categoryID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("RegionID", regionID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("CityID", cityID).Direction = ParameterDirection.Input;
                createCommand.Parameters.Add("NewObjectID", SqlDbType.BigInt).Direction = ParameterDirection.InputOutput;

                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewObjectID"].Value;
                newObjectID = (long)result;
            }

            return newObjectID;
        }

        public void SetObjectData(ObjectInfo objectInfo)
        {
            if (objectInfo == null)
            {
                throw new ArgumentNullException(nameof(objectInfo), "Object info can't be null.");
            }

            SetObjectData(Connection, objectInfo);

            IList<ObjectImage> images = objectInfo.Images;
            if ((images != null) && (images.Count > 0))
            {
                for (int imageIndex = 0; imageIndex < images.Count; ++imageIndex)
                {
                    ObjectImage currentImage = images[imageIndex];

                    SetImageData(objectInfo.ObjectID, currentImage);
                }
            }
        }
        #endregion

        #region Private class methods
        
        private void DeleteAllObjects(SqlConnection connection)
        {
            using (SqlCommand deleteCommand = new SqlCommand(AvitoDb.ObjectDeleteAll, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                deleteCommand.ExecuteNonQuery();
            }
        }

        private void DeleteObjects(SqlConnection connection, Guid departmentID)
        {
            using (SqlCommand deleteCommand = new SqlCommand(AvitoDb.ObjectDeleteByDepartment, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                deleteCommand.Parameters.AddWithValue("DepartmentID", departmentID);
                deleteCommand.ExecuteNonQuery();
            }
        }

        private IList<DepartmentInfo> GetDepartments(SqlConnection connection)
        {
            List<DepartmentInfo> departments = null;

            using (SqlCommand selectCommand = new SqlCommand(AvitoDb.DepartmentsGetAll, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                using (SqlDataReader reader = selectCommand.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        departments = new List<DepartmentInfo>(4);

                        int idOrdinal = reader.GetOrdinal("DepartmentID");
                        int nameOrdinal = reader.GetOrdinal("Name");
                        int pageOrdinal = reader.GetOrdinal("AvitoPageUrl");
                        int serviceOrdinal = reader.GetOrdinal("BotServiceUrl");

                        do
                        {
                            DepartmentInfo department = new DepartmentInfo
                            {
                                DepartmentID = reader.GetGuid(idOrdinal),
                                Name = reader.GetString(nameOrdinal),
                                AvitoPageUrl = reader.GetString(pageOrdinal),
                                BotServiceUrl = reader.GetString(serviceOrdinal)
                            };

                            departments.Add(department);
                        }
                        while (reader.Read());
                    }
                }
            }

            return departments;
        }

        private IList<long> GetObjectIdentifiers(SqlConnection connection)
        {
            List<long> identifiers = null;

            using (SqlCommand selectCommand = new SqlCommand(AvitoDb.ObjectGetIds, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                using (SqlDataReader reader = selectCommand.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        identifiers = new List<long>(200);

                        int idOrdinal = reader.GetOrdinal("ID");
                        do
                        {
                            long objectID = reader.GetInt64(idOrdinal);
                            identifiers.Add(objectID);
                        }
                        while (reader.Read());
                    }
                }
            }

            return identifiers;
        }

        private bool GetObjectStatus(SqlConnection connection, long objectID)
        {
            bool exists = true;
            
            using (SqlCommand checkCommand = new SqlCommand(AvitoDb.ObjectGetStatus, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                checkCommand.Parameters.AddWithValue("ObjectID", objectID);
                checkCommand.Parameters.Add("Result", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                checkCommand.ExecuteNonQuery();

                int result = (int)checkCommand.Parameters["Result"].Value;
                exists = (result == 1);
            }

            return exists;
        }

        private Guid GetSection(SqlConnection connection, string sectionName)
        {
            Guid sectionID = Guid.Empty;
            
            using (SqlCommand getCommand = new SqlCommand(AvitoDb.SectionGetByName, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                getCommand.Parameters.AddWithValue("SectionName", sectionName);
                getCommand.Parameters.Add("SectionID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.ReturnValue;

                getCommand.ExecuteNonQuery();

                object result = getCommand.Parameters["SectionID"].Value;
                sectionID = (Guid)result;
            }

            return sectionID;
        }

        private Guid CreateSection(SqlConnection connection, Guid sectionID, string sectionName)
        {
            Guid newSectionID = Guid.Empty;
            
            using (SqlCommand createCommand = new SqlCommand(AvitoDb.SectionCreate, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                createCommand.Parameters.AddWithValue("SectionID", sectionID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("SectionName", sectionName).Direction = ParameterDirection.Input;
                createCommand.Parameters.Add("NewSectionID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;

                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewSectionID"].Value;
                newSectionID = (Guid)result;
            }

            return newSectionID;
        }

        private Guid GetCategory(SqlConnection connection, Guid sectionID, string categoryName)
        {
            Guid categoryID = Guid.Empty;

            using (SqlCommand getCommand = new SqlCommand(AvitoDb.CategoryGetBySectionAndName, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                getCommand.Parameters.AddWithValue("SectionID", sectionID).Direction = ParameterDirection.Input;
                getCommand.Parameters.AddWithValue("CategoryName", categoryName).Direction = ParameterDirection.Input;
                getCommand.Parameters.Add("CategoryID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.ReturnValue;

                getCommand.ExecuteNonQuery();

                object result = getCommand.Parameters["CategoryID"].Value;
                categoryID = (Guid)result;
            }

            return categoryID;
        }

        private Guid CreateCategory(SqlConnection connection, Guid sectionID, Guid categoryID, string categoryName)
        {
            Guid newCategoryID = Guid.Empty;

            using (SqlCommand createCommand = new SqlCommand(AvitoDb.CategoryCreate, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                createCommand.Parameters.AddWithValue("CategoryID", categoryID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("SectionID", sectionID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("CategoryName", categoryName).Direction = ParameterDirection.Input;
                createCommand.Parameters.Add("NewCategoryID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;

                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewCategoryID"].Value;
                newCategoryID = (Guid)result;
            }

            return newCategoryID;
        }

        private Guid GetRegion(SqlConnection connection, string regionName)
        {
            Guid regionID = Guid.Empty;

            using (SqlCommand getCommand = new SqlCommand(AvitoDb.RegionGetByName, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                getCommand.Parameters.AddWithValue("RegionName", regionName);
                getCommand.Parameters.Add("RegionID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.ReturnValue;

                getCommand.ExecuteNonQuery();

                object result = getCommand.Parameters["RegionID"].Value;
                regionID = (Guid)result;
            }

            return regionID;
        }

        private Guid CreateRegion(SqlConnection connection, Guid regionID, string regionName)
        {
            Guid newRegionID = Guid.Empty;

            using (SqlCommand createCommand = new SqlCommand(AvitoDb.RegionCreate, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                createCommand.Parameters.AddWithValue("RegionID", regionID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("RegionName", regionName).Direction = ParameterDirection.Input;
                createCommand.Parameters.Add("NewRegionID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;

                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewRegionID"].Value;
                newRegionID = (Guid)result;
            }

            return newRegionID;
        }

        private Guid GetCity(SqlConnection connection, Guid regionID, string cityName)
        {
            Guid cityID = Guid.Empty;

            using (SqlCommand getCommand = new SqlCommand(AvitoDb.CityGetByRegionAndName, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                getCommand.Parameters.AddWithValue("RegionID", regionID).Direction = ParameterDirection.Input;
                getCommand.Parameters.AddWithValue("CityName", cityName).Direction = ParameterDirection.Input;
                getCommand.Parameters.Add("CityID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.ReturnValue;

                getCommand.ExecuteNonQuery();

                object result = getCommand.Parameters["CityID"].Value;
                cityID = (Guid)result;
            }

            return cityID;
        }

        private Guid CreateCity(SqlConnection connection, Guid regionID, Guid cityID, string cityName)
        {
            Guid newCityID = Guid.Empty;

            using (SqlCommand createCommand = new SqlCommand(AvitoDb.CityCreate, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                createCommand.Parameters.AddWithValue("RegionID", regionID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("CityID", cityID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("CityName", cityName).Direction = ParameterDirection.Input;
                createCommand.Parameters.Add("NewCityID", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.InputOutput;

                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewCityID"].Value;
                newCityID = (Guid)result;
            }

            return newCityID;
        }

        private void SetObjectData(SqlConnection connection, ObjectInfo objectInfo)
        {
            using (SqlCommand updateCommand = new SqlCommand(AvitoDb.ObjectSetData, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                updateCommand.Parameters.AddWithValue("ObjectID", objectInfo.ObjectID).Direction = ParameterDirection.Input;
                updateCommand.Parameters.AddWithValue("Name", objectInfo.Name).Direction = ParameterDirection.Input;
                updateCommand.Parameters.AddWithValue("Description", objectInfo.Description).Direction = ParameterDirection.Input;
                updateCommand.Parameters.AddWithValue("Price", objectInfo.DecimalPrice).Direction = ParameterDirection.Input;
                updateCommand.Parameters.AddWithValue("Url", objectInfo.Url).Direction = ParameterDirection.Input;
                updateCommand.Parameters.AddWithValue("IssueYear", objectInfo.IssueYear).Direction = ParameterDirection.Input;

                updateCommand.ExecuteNonQuery();
            }
        }
        
        private long SetImageData(long objectID, ObjectImage objectImage)
        {
            long newImageID = 0L;

            using (SqlCommand createCommand = new SqlCommand(AvitoDb.ObjectImageSetData, Connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            })
            {
                createCommand.Parameters.AddWithValue("ObjectID", objectID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("ImageID", objectImage.ImageID).Direction = ParameterDirection.Input;
                createCommand.Parameters.AddWithValue("ImageUrl", objectImage.ImageUrl).Direction = ParameterDirection.Input;
                createCommand.Parameters.Add("NewImageID", SqlDbType.BigInt).Direction = ParameterDirection.InputOutput;

                createCommand.ExecuteNonQuery();

                object result = createCommand.Parameters["NewImageID"].Value;
                newImageID = (long)result;
            }

            return newImageID;
        }
        #endregion
    }
}