﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SberbankSearchBot.DatabaseService
{
    public class AvitoParserFactory
    {
        #region Fields

        private ServiceConfiguration Configuration;
        #endregion

        #region Constructors

        public AvitoParserFactory(ServiceConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration), "Service configuration can't be null.");
            }

            Configuration = configuration;
        }
        #endregion

        #region Public class methods

        public AvitoWebParser CreateParser(string pageUrl)
        {
            HttpClient client = CreateAvitoHttpClient();

            return new AvitoWebParser(Configuration, client, pageUrl);
        }
        #endregion

        #region Private class methods

        private static HttpClient CreateAvitoHttpClient()
        {
            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue
            {
                MaxAge = TimeSpan.FromSeconds(0.0)
            };
            client.DefaultRequestHeaders.Connection.TryParseAdd("keep-alive");
            client.DefaultRequestHeaders.UserAgent.TryParseAdd("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
            client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");

            return client;
        }
        #endregion
    }
}