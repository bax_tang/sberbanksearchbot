﻿using System;

namespace SberbankSearchBot.DatabaseService
{
    internal static class AvitoDb
    {
        #region Constants and fields

        public const string DepartmentsGetAll = "[dbo].[sp_bank_department_get_data]";

        public const string ObjectDeleteAll = "[dbo].[sp_object_delete_all]";

        public const string ObjectDeleteByDepartment = "[dbo].[sp_object_delete_by_dep]";

        public const string ObjectGetIds = "[dbo].[sp_object_get_ids]";

        public const string ObjectGetStatus = "[dbo].[fn_object_get_status]";

        public const string SectionGetByName = "[dbo].[fn_section_get_by_name]";

        public const string SectionCreate = "[dbo].[sp_section_create]";

        public const string CategoryGetBySectionAndName = "[dbo].[fn_category_get_by_section_and_name]";

        public const string CategoryCreate = "[dbo].[sp_category_create]";

        public const string RegionGetByName = "[dbo].[fn_region_get_by_name]";

        public const string RegionCreate = "[dbo].[sp_region_create]";

        public const string CityGetByRegionAndName = "[dbo].[fn_city_get_by_region_and_name]";

        public const string CityCreate = "[dbo].[sp_city_create]";

        public const string ObjectCreate = "[dbo].[sp_object_create]";

        public const string ObjectSetData = "[dbo].[sp_object_maininfo_set_data]";

        public const string ObjectImageSetData = "[dbo].[sp_object_image_set_data]";
        #endregion
    }
}