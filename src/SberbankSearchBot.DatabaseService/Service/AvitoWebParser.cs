﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using HtmlAgilityPack;

namespace SberbankSearchBot.DatabaseService
{
    public class AvitoWebParser : IDisposable
    {
        #region Constants and fields

        private const string ItemNodePath = "//div[@class='item item_table clearfix js-catalog-item-enum c-b-0']";

        private const string BreadcrumbsSelector = "//div[@class='b-catalog-breadcrumbs']";

        private const string DescriptionNodePathV1 = "//div[@class='item-description-html']";

        private const string DescriptionNodePathV2 = "//div[@class='item-description-text']";

        private const string AddressNodePath = "//div[@class='seller-info js-seller-info']";

        private const string AddresMapLocationPath = "//div[@class='item-map-location']";

        private const string PriceNodePath = "//span[@class='price-value-string js-price-value-string']";

        private const string ImageNodesPath = "//div[@class='gallery-img-wrapper js-gallery-img-wrapper']";

        private const string SellerInfoPropertyClass = "seller-info-prop";

        private const string SellerInfoValueClass = "seller-info-value";

        private const string ObjectUrlHostPart = "https://www.avito.ru";

        private static readonly string[] Realties = new[] { "дом", "дач", "коттедж", "коммерч", "недвижим", "гараж", "квартир", "земел", "комнат", "кварт" };

        private static readonly string[] Autos = new[] { "грузовик", "спецтехника", "автомобил", "автомоб" };

        private const string Equipment = "Оборудование";

        private const string Business = "бизнес";

        private ServiceConfiguration Configuration;

        private HttpClient Client;

        private string PageUrl;
        #endregion

        #region Constructors

        public AvitoWebParser(ServiceConfiguration configuration, HttpClient client, string pageUrl)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration), "Service configuration can't be null.");
            }
            if (client == null)
            {
                throw new ArgumentNullException(nameof(client), "Http client can't be null.");
            }
            if (string.IsNullOrEmpty(pageUrl))
            {
                throw new ArgumentNullException(nameof(pageUrl), "Page url can't be null or empty.");
            }

            Configuration = configuration;
            Client = client;
            PageUrl = pageUrl;
        }
        #endregion

        #region IDisposable implementation

        public void Dispose() { }
        #endregion

        #region Public class methods
        
        public int GetPagesCount()
        {
            HtmlDocument mainPage = Client.GetDocument(new Uri(PageUrl));

            return GetPagesCountFromMainPage(mainPage);
        }

        public IEnumerable<ObjectInfo> GetObjectsFromPage(int pageIndex)
        {
            if (pageIndex <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(pageIndex), "Invalid page index.");
            }

            string pageUri = string.Concat(PageUrl, "?p=", pageIndex.ToString());
            HtmlDocument pageDocument = Client.GetDocument(new Uri(pageUri));

            HtmlNodeCollection itemNodes = pageDocument.DocumentNode.SelectNodes(ItemNodePath);

            return itemNodes.Select(GetObjectInfoFromItemNode);
        }
        
		public ObjectInfo GetObjectFromUrl(string objectUri)
		{
			if (string.IsNullOrEmpty(objectUri))
			{
				throw new ArgumentNullException(nameof(objectUri), "Object URI can't be null or empty.");
			}
			
			string objectName = GetObjectNameFromPage(objectUri);
			if (string.IsNullOrEmpty(objectName))
			{
				return null;
			}

			int lastIndex = objectUri.LastIndexOf('_');
			string identifier = objectUri.Substring(lastIndex + 1);
			long objectID = long.Parse(identifier);

			ObjectInfo objectInfo = new ObjectInfo(objectID, objectName, objectUri);

			FillAdditionalInfo(objectInfo);

			return objectInfo;
		}

        public bool FillAdditionalInfo(ObjectInfo objectInfo)
        {
            if (objectInfo == null)
            {
                throw new ArgumentNullException(nameof(objectInfo), "Object info can't be null.");
            }

            Uri objectUri = new Uri(objectInfo.Url);
            HtmlDocument objectDocument = Client.GetDocument(objectUri);

            bool documentExists = (objectDocument != null);
            if (documentExists)
            {
                FillObjectFromDocument(objectDocument, objectInfo);
            }
            return documentExists;
        }
        #endregion

        #region Private class methods

        private int GetPagesCountFromMainPage(HtmlDocument mainPage)
        {
            int pagesCount = 0;

            HtmlNode paginationNode = mainPage.DocumentNode.SelectSingleNode("//div[@class='pagination-pages clearfix']");
            if (paginationNode != null)
            {
                HtmlNodeCollection childNodes = paginationNode.ChildNodes;
                for (int index = 0; index < childNodes.Count; ++index)
                {
                    string nodeName = childNodes[index].Name;
                    if (string.Equals("span", nodeName) || string.Equals("a", nodeName))
                    {
                        pagesCount++;
                    }
                }
            }

            return pagesCount;
        }

		private string GetObjectNameFromPage(string objectUri)
		{
			HtmlDocument objectPage = Client.GetDocument(new Uri(objectUri));

			HtmlNode nameNode = objectPage?.DocumentNode?.SelectSingleNode("//span[@class='title-info-title-text']");
			if (nameNode != null)
			{
				return WebUtility.HtmlDecode(nameNode.InnerText);
			}

			return null;
		}

        private ObjectInfo GetObjectInfoFromItemNode(HtmlNode itemNode)
        {
            string identifier = itemNode.Attributes["id"].Value.Substring(1);

            long objectID = -1L;
            bool parsed = long.TryParse(identifier, NumberStyles.Any, CultureInfo.InvariantCulture, out objectID);

            ObjectInfo result = null;

            if (parsed)
            {
                HtmlNode informationNode = null;

                HtmlNodeCollection itemChildNodes = itemNode.ChildNodes;
                for (int index = 0; index < itemChildNodes.Count; ++index)
                {
                    HtmlNode currentChild = itemChildNodes[index];
                    HtmlAttribute classAttribute = currentChild.Attributes["class"];
                    if (classAttribute != null && string.Equals(classAttribute.Value, "description", StringComparison.OrdinalIgnoreCase))
                    {
                        informationNode = currentChild; break;
                    }
                }

                if (informationNode != null)
                {
                    HtmlNode titleNode = informationNode.ChildNodes["h3"].ChildNodes["a"];

                    string objectName = titleNode.InnerText.Trim();
                    objectName = WebUtility.HtmlDecode(objectName);

                    string objectUrl = string.Concat(ObjectUrlHostPart, titleNode.Attributes["href"].Value);

                    result = new ObjectInfo(objectID, objectName, objectUrl);
                }
            }

            return result ?? ObjectInfo.Empty;
        }

        private void FillObjectFromDocument(HtmlDocument objectDocument, ObjectInfo objectInfo)
        {
            // section and category extraction
            HtmlNode breadcrumbsNode = objectDocument.DocumentNode.SelectSingleNode(BreadcrumbsSelector);
            if (breadcrumbsNode != null)
            {
                int index = 0;
                foreach (HtmlNode infoNode in breadcrumbsNode.Elements("a"))
                {
                    switch (index++)
                    {
                        case 0: break;
                        case 1:
                            objectInfo.Section = WebUtility.HtmlDecode(infoNode.InnerText);
                            continue;
                        case 2:
                            string categoryName = WebUtility.HtmlDecode(infoNode.InnerText);
                            objectInfo.Category = FixupCategoryName(categoryName);
                            continue;
                        default: break;
                    }
                }
            }
            
            if (objectInfo.Section.Any(char.IsPunctuation)) // <<< fixup '...'
            {
                objectInfo.Section = GetSectionByCategory(objectInfo.Category);
            }

            // fixup 'Для бизнеса'
            if (!string.IsNullOrEmpty(objectInfo.Section) &&
                objectInfo.Section.IndexOf(Business, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                objectInfo.Section = "Бизнес";
            }

            // city and region extraction
            HtmlNode addressNode = objectDocument.DocumentNode.SelectSingleNode(AddressNodePath);
            if (addressNode != null)
            {
                HtmlNode addressPropNode = addressNode.ChildNodes.LastOrDefault(IsSellerInfoPropertyNode);
                HtmlNode addressValueNode = addressPropNode.ChildNodes.LastOrDefault(IsSellerInfoValueNode);

                string addressValue = addressValueNode.InnerText.Trim();
                int commaIndex = addressValue.IndexOf(',');
                objectInfo.Region = addressValue.Substring(0, commaIndex);

                string cityText = null;

                int secondCommaIndex = addressValue.IndexOf(',', commaIndex + 1);
                if (secondCommaIndex != -1)
                {
                    int textLength = secondCommaIndex - (commaIndex + 1);
                    cityText = addressValue.Substring(commaIndex + 1, textLength).Trim();
                }
                else
                {
                    cityText = addressValue.Substring(commaIndex + 1).Trim();
                }

                int spaceIndex = cityText.IndexOf(' ');
                if (spaceIndex != -1)
                {
                    objectInfo.City = cityText.Substring(0, spaceIndex);
                }
                else objectInfo.City = cityText;
            }

            // city extraction fixup
            if (string.IsNullOrEmpty(objectInfo.City))
            {
                HtmlNode cityParentNode = objectDocument.DocumentNode.SelectSingleNode(AddresMapLocationPath);
                if (cityParentNode != null)
                {
                    foreach (HtmlNode cityNode in cityParentNode.Elements("span"))
                    {
                        if (cityNode.GetAttributeValue("itemprop", string.Empty) == "name")
                        {
                            if (!string.Equals(objectInfo.Region, cityNode.InnerText, StringComparison.OrdinalIgnoreCase))
                            {
                                objectInfo.City = WebUtility.HtmlDecode(cityNode.InnerText);
                                break;
                            }
                        }
                    }
                }
            }

            // description and issue year extraction
            HtmlNode descriptionNode = objectDocument.DocumentNode.SelectSingleNode(DescriptionNodePathV1);
            if (descriptionNode == null)
            {
                descriptionNode = objectDocument.DocumentNode.SelectSingleNode(DescriptionNodePathV2);
            }
            if (descriptionNode != null)
            {
                string descriptionText = descriptionNode.InnerText.Trim();
                objectInfo.Description = WebUtility.HtmlDecode(descriptionText);

                if (string.Equals("Транспорт", objectInfo.Section, StringComparison.OrdinalIgnoreCase))
                {
                    int? year = ExtractYearFromObjectDescription(objectInfo.Description);
                    objectInfo.IssueYear = year;
                }
            }

            // price extraction
            HtmlNode priceNode = objectDocument.DocumentNode.SelectSingleNode(PriceNodePath);
            if (priceNode != null)
            {
                double? price = ExtractPriceFromNodeText(priceNode.InnerText);
                objectInfo.Price = price;
            }

            // images extraction
            HtmlNodeCollection imageNodes = objectDocument.DocumentNode.SelectNodes(ImageNodesPath);
            if (imageNodes != null)
            {
                int count = imageNodes.Count;

                ObjectImage[] imagesArray = new ObjectImage[count];
                for (int index = 0; index < count; ++index)
                {
                    HtmlNode divNode = imageNodes[index].ChildNodes.FirstOrDefault(IsDivNode);
                    HtmlAttribute urlAttribute = divNode?.Attributes["data-url"];
                    if (urlAttribute != null)
                    {
                        string relativeUrl = urlAttribute.Value;
                        string absoluteUrl = string.Concat("https:", urlAttribute.Value);

                        int slashIndex = relativeUrl.LastIndexOf('/');
                        int dotIndex = relativeUrl.LastIndexOf('.');

                        string imageIdentifier = relativeUrl.Substring(1 + slashIndex, dotIndex - slashIndex - 1);
                        long imageID = long.Parse(imageIdentifier);

                        imagesArray[index] = new ObjectImage(imageID, absoluteUrl);
                    }
                }
                objectInfo.Images = imagesArray;
            }
        }

        private int? ExtractYearFromObjectDescription(string objectDescription)
        {
            Regex yearRegex = Configuration.YearRegex;

            int? year = null;
            int currentYear = DateTime.UtcNow.Year;

            if (!string.IsNullOrEmpty(objectDescription))
            {
                Match yearMatch = yearRegex.Match(objectDescription);
                if (yearMatch.Success)
                {
                    do
                    {
                        year = int.Parse(yearMatch.Value);
                        if ((year >= 1900) && (year <= currentYear))
                        {
                            break;
                        }

                        yearMatch = yearMatch.NextMatch();
                    }
                    while (yearMatch.Success);
                }
            }

            return year;
        }

        private double? ExtractPriceFromNodeText(string priceNodeText)
        {
            string dataText = WebUtility.HtmlDecode(priceNodeText).Trim();

            StringBuilder priceBuilder = new StringBuilder(dataText.Length);
            for (int index = 0; index < dataText.Length; ++index)
            {
                char current = dataText[index];
                if (char.IsDigit(current) || (current == '.'))
                {
                    priceBuilder.Append(current);
                }
            }

            double priceValue = 0.0D;

            bool parsingAvailable = (priceBuilder.Length >= 1);
            bool isParsed = false;
            if (parsingAvailable)
            {
                if (priceBuilder[priceBuilder.Length - 1] == '.')
                {
                    priceBuilder.Append('0');
                }

                string priceText = priceBuilder.ToString();
                isParsed = double.TryParse(priceText, NumberStyles.Any, CultureInfo.InvariantCulture, out priceValue);
            }

            return (isParsed) ? new double?(priceValue) : null;
        }

        private string FixupCategoryName(string categoryName)
        {
            int realtyIndex = categoryName.IndexOf("недвижимость");

            return (realtyIndex != -1) ? categoryName.Substring(0, realtyIndex - 1) : categoryName;
        }

        private string GetSectionByCategory(string categoryName)
        {
            string sectionName = null;

            CategoryNameIndexer indexer = new CategoryNameIndexer(categoryName);
            if (Array.FindIndex(Realties, indexer.CheckPartIndex) >= 0)
            {
                sectionName = "Недвижимость";
            }
            else if (Array.FindIndex(Autos, indexer.CheckPartIndex) >= 0)
            {
                sectionName = "Транспорт";
            }
            else if (categoryName.IndexOf(Equipment) >= 0)
            {
                sectionName = "Оборудование";
            }
            else if (categoryName.IndexOf(Business, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                sectionName = "Бизнес";
            }
            else sectionName = "Прочее";

            return sectionName;
        }

        private bool IsSellerInfoPropertyNode(HtmlNode node)
        {
            HtmlAttribute classAttribute = node?.Attributes["class"];

            return (classAttribute != null) && string.Equals(SellerInfoPropertyClass, classAttribute.Value, StringComparison.OrdinalIgnoreCase);
        }

        private bool IsSellerInfoValueNode(HtmlNode node)
        {
            HtmlAttribute classAttribute = node?.Attributes["class"];

            return (classAttribute != null) && string.Equals(SellerInfoValueClass, classAttribute.Value, StringComparison.OrdinalIgnoreCase);
        }

        private bool IsDataNode(HtmlNode node)
        {
            return
                (node != null) &&
                string.Equals(node.Name, "div", StringComparison.OrdinalIgnoreCase) &&
                string.Equals(node.Attributes["class"]?.Value, "data", StringComparison.OrdinalIgnoreCase);
        }

        private bool IsDivNode(HtmlNode node)
        {
            return (node != null) && string.Equals("div", node.Name, StringComparison.OrdinalIgnoreCase);
        }
        #endregion

        #region Nested types

        private class CategoryNameIndexer
        {
            private string CategoryName;
            public CategoryNameIndexer(string categoryName)
            {
                CategoryName = categoryName;
            }
            public bool CheckPartIndex(string categoryPart)
            {
                return CategoryName.IndexOf(categoryPart, StringComparison.OrdinalIgnoreCase) >= 0;
            }
        }
        #endregion
    }
}