﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using HtmlAgilityPack;

namespace SberbankSearchBot
{
    public static class HttpClientExtensions
    {
        #region Public class methods

        public static HtmlDocument GetDocument(this HttpClient client, string url)
        {
            var responseTask = client.GetAsync(url);
            responseTask.Wait();

            var response = responseTask.Result;
            var content = response.Content;

            string charSet = content.Headers.ContentType.CharSet ?? "utf-8";
            Encoding responseEncoding = Encoding.GetEncoding(charSet);

            Stream responseStream = content.ReadAsStreamAsync().Result;

            HtmlDocument document = new HtmlDocument();
            document.Load(responseStream, responseEncoding);
            return document;
        }
        #endregion
    }
}