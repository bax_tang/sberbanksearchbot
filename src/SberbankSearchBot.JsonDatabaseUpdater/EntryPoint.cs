﻿using System;
using System.Diagnostics;
using System.IO;

namespace SberbankSearchBot
{
    internal static class EntryPoint
    {
        #region Constants

        private const string RelativeDatabasePath = "SberbankSearchBot\\App_Data\\ObjectsData.json";
        #endregion

        #region The entry point

        internal static int Main(string[] args)
        {
            int exitCode = 0;

            try
            {
                string filePath = GetDatabaseFilePath();

                JsonDatabaseUpdater updater = new JsonDatabaseUpdater(filePath, Console.Out);
                if (updater.ParseAvitoObjects())
                {
                    updater.UpdateDatabase();
                }
            }
            catch (Exception exc)
            {
                exitCode = exc.HResult;
                Console.WriteLine("Exception occured: {0}\r\n{1}", exc.Message, exc);
            }
            finally
            {
                PauseOnDebugging();
            }
            
            return exitCode;
        }
        #endregion

        #region Private class methods
        
        private static void PauseOnDebugging()
        {
            if (Debugger.IsAttached)
            {
                Console.ReadKey(true);
            }
        }

        private static string GetDatabaseFilePath()
        {
            string currentDirectory = Directory.GetCurrentDirectory();

            string filePath = string.Empty;

            DirectoryInfo current = new DirectoryInfo(currentDirectory);
            DirectoryInfo parent = current.Parent?.Parent?.Parent;
            if (parent != null)
            {
                filePath = Path.Combine(parent.FullName, RelativeDatabasePath);
            }

            return filePath;
        }
        #endregion
    }
}