﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using HtmlAgilityPack;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace SberbankSearchBot
{
    public class JsonDatabaseUpdater
    {
        #region Constants and fields
        
        private static readonly string[] Realties = new[] { "дом", "дач", "коттедж", "коммерч", "недвижим", "гараж", "квартир", "земел", "комнат" };

        private static readonly string[] Autos = new[] { "грузовик", "спецтехника", "автомобил" };

        private static readonly string Equipment = "Оборудование";

        private static readonly string Business = "бизнес";

        private const string InitialUrl = "https://www.avito.ru/sberbank/rossiya";

        private const string YearExpression = "(?:(?:[1]{1}\\d{1}\\d{1}\\d{1})|(?:[2]{1}\\d{3}))";

        private readonly HttpClient Client;

        private readonly Random Rnd;

        private readonly Regex YearRegex;

        private readonly string DatabaseFilePath;

        private readonly TextWriter Logger;

        private List<JObject> ParsedObjects;
        #endregion

        #region Constructors

        private JsonDatabaseUpdater()
        {
            Rnd = new Random();
            Client = new HttpClient();
            ParsedObjects = new List<JObject>(250);
            YearRegex = new Regex(YearExpression, RegexOptions.IgnoreCase | RegexOptions.Singleline);
        }

        public JsonDatabaseUpdater(string databaseFilePath, TextWriter logger) : this()
        {
            if (string.IsNullOrEmpty(databaseFilePath))
            {
                throw new ArgumentNullException(nameof(databaseFilePath), "Database file path can't be null or empty.");
            }
            if (IsPathContainsInvalidCharacters(databaseFilePath))
            {
                throw new ArgumentException("Database file path contains invalid characters.", nameof(databaseFilePath));
            }
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger), "Logger can't be null.");
            }

            DatabaseFilePath = databaseFilePath;
            Logger = logger;
        }
        #endregion

        #region Public class methods

        public bool ParseAvitoObjects()
        {
            WriteLogMessage("Запуск процесса парсинга");

            bool result = true;
            try
            {
                int pagesCount = GetPagesCount();
                WriteLogMessage("Получено количество страниц: {0}", pagesCount);

                for (int pageIndex = 1; pageIndex <= pagesCount; ++pageIndex)
                {
                    WriteLogMessage("Начата обработка {0} страницы", pageIndex);

                    ProcessSitePage(pageIndex);

                    WriteLogMessage("Завершена обработка {0} страницы", pageIndex);

                    Thread.Sleep(Rnd.Next(2000, 3000));
                }
            }
            catch (Exception exc)
            {
                WriteLogMessage("Exception occured: {0}\r\n{1}", exc.Message, exc);
                result = false;
            }
            finally
            {
                if (result)
                {
                    WriteLogMessage("Парсинг успешно завершён");
                }
            }

            return result;
        }

        public void UpdateDatabase()
        {
            WriteLogMessage("Начат процесс обновления базы данных");

            var parsedObject = ParsedObjects;

            JsonSerializer serializer = CreateSerializer();
            using (StreamWriter writer = new StreamWriter(DatabaseFilePath, false, Encoding.UTF8, 4096))
            {
                serializer.Serialize(writer, parsedObject);
                writer.Flush();
            }

            WriteLogMessage("База данных успешно обновлена");
        }
        #endregion

        #region Private class methods

        private static bool IsPathContainsInvalidCharacters(string filePath)
        {
            char[] invalidPathChars = Path.GetInvalidPathChars();

            return (-1 != filePath.IndexOfAny(invalidPathChars));
        }

        private static bool IsDataNode(HtmlNode node)
        {
            return (node != null) &&
                string.Equals(node.Name, "div", StringComparison.OrdinalIgnoreCase) &&
                string.Equals(node.Attributes["class"].Value, "data", StringComparison.OrdinalIgnoreCase);
        }

        private static JsonSerializer CreateSerializer()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver(),
                DateFormatString = "dd.MM.yyyy",
                DefaultValueHandling = DefaultValueHandling.Include,
                Formatting = Formatting.Indented,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };

            JsonSerializer serializer = JsonSerializer.Create(settings);

            return serializer;
        }

        private void WriteLogMessage(string message)
        {
            Logger.WriteLine(message);
        }

        private void WriteLogMessage(string messageFormat, object arg0)
        {
            Logger.WriteLine(messageFormat, arg0);
        }

        private void WriteLogMessage(string messageFormat, object arg0, object arg1)
        {
            Logger.WriteLine(messageFormat, arg0, arg1);
        }

        private int GetPagesCount()
        {
            HtmlDocument mainPage = Client.GetDocument(InitialUrl);

            return GetPagesCountFromDocument(mainPage);
        }

        private int GetPagesCountFromDocument(HtmlDocument mainPage)
        {
            int pagesCount = 0;

            HtmlNode paginationNode = mainPage.DocumentNode.SelectSingleNode("//div[@class='pagination-pages clearfix']");
            if (paginationNode != null)
            {
                HtmlNodeCollection childNodes = paginationNode.ChildNodes;
                for (int index = 0; index < childNodes.Count; ++index)
                {
                    string nodeName = childNodes[index].Name;
                    if (string.Equals("span", nodeName) || string.Equals("a", nodeName))
                    {
                        pagesCount++;
                    }
                }
            }

            return pagesCount;
        }

        private void ProcessSitePage(int pageIndex)
        {
            string pageUrl = string.Concat(InitialUrl, "?p=", pageIndex.ToString());

            HtmlDocument pageDocument = Client.GetDocument(pageUrl);

            HtmlNodeCollection itemNodes = pageDocument.DocumentNode.SelectNodes("//div[@class='item item_table clearfix js-catalog-item-enum c-b-0']");
            if ((itemNodes != null) && (itemNodes.Count > 0))
            {
                for (int index = 0; index < itemNodes.Count; ++index)
                {
                    WriteLogMessage("Обработка {0} объекта на странице {1}", (1 + index), pageIndex);

                    ProcessItemNode(itemNodes[index]);

                    WriteLogMessage("Обработка объекта завершена");

                    Thread.Sleep(Rnd.Next(2000, 3000));
                }
            }
        }
        
        private void ProcessItemNode(HtmlNode itemNode)
        {
            string itemID = itemNode.Attributes["id"].Value.Substring(1);

            HtmlNode descriptionNode = null;

            HtmlNodeCollection childNodes = itemNode.ChildNodes;
            for (int index = 0; index < childNodes.Count; ++index)
            {
                HtmlNode currentChild = childNodes[index];
                HtmlAttribute classAttribute = currentChild.Attributes["class"];
                if (classAttribute != null && string.Equals(classAttribute.Value, "description", StringComparison.OrdinalIgnoreCase))
                {
                    descriptionNode = currentChild; break;
                }
            }

            if (descriptionNode != null)
            {
                HtmlNode titleNode = descriptionNode.ChildNodes["h3"].ChildNodes["a"];

                JObject itemData = new JObject();

                string itemUrl = string.Concat("https://www.avito.ru", titleNode.Attributes["href"].Value);
                string name = titleNode.InnerText.Trim();

                itemData["ID"] = itemID;
                itemData["Name"] = WebUtility.HtmlDecode(name);
                itemData["URL"] = itemUrl;
                
                HtmlDocument itemDocument = Client.GetDocument(itemUrl);

                if (itemData["Price"] == null)
                {
                    HtmlNode priceNode = itemDocument.DocumentNode.SelectSingleNode("//span[@class='price-value-string']");
                    if (priceNode != null)
                    {
                        string dataText = WebUtility.HtmlDecode(priceNode.InnerText).Trim();

                        StringBuilder priceBuilder = new StringBuilder(dataText.Length);
                        for (int index = 0; index < dataText.Length; ++index)
                        {
                            char current = dataText[index];
                            if (char.IsDigit(current) || (current == '.'))
                            {
                                priceBuilder.Append(current);
                            }
                        }

                        if (priceBuilder.Length >= 1)
                        {
                            if (priceBuilder[priceBuilder.Length - 1] == '.')
                            {
                                priceBuilder.Append('0');
                            }

                            double priceValue = 0.0;
                            if (double.TryParse(priceBuilder.ToString(), NumberStyles.Any, CultureInfo.InvariantCulture, out priceValue))
                            {
                                itemData["Price"] = priceValue;
                            }
                            else
                            {
                                WriteLogMessage("Не удалось спарсить значение цены для объекта #{0}; фактическое значение: {1}",
                                    itemID,
                                    priceBuilder);
                            }
                        }
                    }
                }

                if (itemData["Images"] == null)
                {
                    HtmlNodeCollection imageNodes = itemDocument.DocumentNode.SelectNodes("//div[@class='gallery-img-wrapper js-gallery-img-wrapper']");
                    if (imageNodes != null)
                    {
                        int count = imageNodes.Count;

                        string[] imageUrls = new string[count];
                        for (int index = 0; index < count; ++index)
                        {
                            HtmlAttribute urlAttribute = imageNodes[index].ChildNodes.FirstOrDefault(node => node.Name == "div")?.Attributes["data-url"];
                            if (urlAttribute != null)
                            {
                                imageUrls[index] = string.Concat("https:", urlAttribute.Value);
                            }
                        }
                        itemData["Images"] = new JArray(imageUrls);
                    }
                }

                if (itemData["Description"] == null)
                {
                    HtmlNode itemDescriptionNode = itemDocument.DocumentNode.SelectSingleNode("//div[@class='item-description-html']");
                    if (itemDescriptionNode != null)
                    {
                        string descriptionText = itemDescriptionNode.InnerText.Trim();

                        itemData["Description"] = WebUtility.HtmlDecode(descriptionText);
                    }
                }

                HtmlNode addressNode = itemDocument.DocumentNode.SelectSingleNode("//div[@class='seller-info js-seller-info']");
                HtmlNode addressPropNode = addressNode.ChildNodes.LastOrDefault(node => string.Equals("seller-info-prop", node.Attributes["class"]?.Value));
                HtmlNode addressValueNode = addressPropNode.ChildNodes.LastOrDefault(node => string.Equals("seller-info-value", node.Attributes["class"]?.Value));

                string addressValue = addressValueNode.InnerText.Trim();
                int commaIndex = addressValue.IndexOf(',');
                string region = addressValue.Substring(0, commaIndex);
                itemData["Region"] = region;

                HtmlNode dataNode = descriptionNode.ChildNodes.FirstOrDefault(IsDataNode);

                itemData["City"] = dataNode.ChildNodes[3].InnerText;

                string category = dataNode.ChildNodes[1].InnerText;
                string section = GetSectionFromCategory(category);
                itemData["Section"] = section;
                itemData["Category"] = FixupCategory(category);

                if (string.Equals("Транспорт", section, StringComparison.OrdinalIgnoreCase))
                {
                    string itemDescription = (string)itemData["Description"];
                    int year = ExtractYearFromItemDescription(itemDescription);
                    itemData["Year"] = year;
                }

                WriteLogMessage("Успешно обработан объект #{0}: {1}", itemID, itemData["Name"]);

                ParsedObjects.Add(itemData);
            }
        }

        private int ExtractYearFromItemDescription(string itemDescription)
        {
            int year = -1;
            int currentYear = DateTime.Now.Year;

            if (!string.IsNullOrEmpty(itemDescription))
            {
                Match yearMatch = YearRegex.Match(itemDescription);
                if (yearMatch.Success)
                {
                    do
                    {
                        year = int.Parse(yearMatch.Value);
                        if (year >= 1900 && year <= currentYear)
                        {
                            break;
                        }

                        yearMatch = yearMatch.NextMatch();
                    }
                    while (yearMatch.Success);
                }
            }

            return year;
        }

        private string GetSectionFromCategory(string category)
        {
            string section = null;

            if (Realties.Any(s => category.IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0))
            {
                section = "Недвижимость";
            }
            else if (Autos.Any(s => category.IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0))
            {
                section = "Транспорт";
            }
            else if (category.IndexOf(Equipment) >= 0)
            {
                section = "Оборудование";
            }
            else if (category.IndexOf(Business, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                section = "Бизнес";
            }
            else section = "Неизвестный раздел";

            return section;
        }

        private string FixupCategory(string category)
        {
            int realtyIndex = category.IndexOf("недвижимость");

            return (realtyIndex != -1) ? category.Substring(0, realtyIndex - 1) : category;
        }
        #endregion
    }
}