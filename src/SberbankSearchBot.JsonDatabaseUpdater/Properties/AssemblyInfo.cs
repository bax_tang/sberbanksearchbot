﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SberbankSearchBot.JsonDatabaseUpdater")]
[assembly: AssemblyDescription("Automatic JSON database updater for SberbankSearchBot")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("OIS")]
[assembly: AssemblyProduct("OIS SberbankSearchBot")]
[assembly: AssemblyCopyright("Copyright © OIS 2016")]
[assembly: AssemblyTrademark("OIS 2016 - 2017. All rights reserved.")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c0aa8ce9-3729-4194-8b98-7f9de1106ccf")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.1.*")]
[assembly: AssemblyFileVersion("1.0.1")]
