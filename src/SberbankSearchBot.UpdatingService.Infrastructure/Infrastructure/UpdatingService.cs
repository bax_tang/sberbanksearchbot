﻿using System;

namespace SberbankSearchBot
{
    public sealed class UpdatingService
    {
        #region Constants, fields and properties
        #endregion

        #region Events

        public event EventHandler UpdateRequest;
        #endregion

        #region Constructors
        #endregion

        #region Public class methods

        public bool Start()
        {
            return true;
        }

        public bool Stop()
        {
            return true;
        }
        #endregion
    }
}