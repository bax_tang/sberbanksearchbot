﻿using System;

using Autofac;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace SberbankSearchBot.Modules
{
    public sealed class ConfigurationModule : Module
    {
        #region Constants and fields
        #endregion

        #region Constructors

        public ConfigurationModule() { }
        #endregion

        #region Module methods overriding

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(CreateConfiguration).SingleInstance();
        }
        #endregion

        #region Private class methods

        private static UpdatingServiceConfiguration CreateConfiguration(IComponentContext context)
        {
            return UpdatingServiceConfiguration.Instance;
        }
        #endregion
    }
}