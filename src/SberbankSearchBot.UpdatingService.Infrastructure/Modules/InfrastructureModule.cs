﻿using System;

using Autofac;

namespace SberbankSearchBot.Modules
{
    public sealed class InfrastructureModule : Module
    {
        #region Constructors

        public InfrastructureModule() : base() { }
        #endregion

        #region Module methods overriding

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterModule(new LoggingModule());
            builder.RegisterModule(new ConfigurationModule());
        }
        #endregion
    }
}