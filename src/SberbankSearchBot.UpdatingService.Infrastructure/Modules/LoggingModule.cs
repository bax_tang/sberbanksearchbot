﻿using System;

using Autofac;
using NLog;

namespace SberbankSearchBot.Modules
{
    public sealed class LoggingModule : Module
    {
        #region Constructors
        #endregion

        #region Module methods overriding

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.Register(CreateLogger).AsImplementedInterfaces().InstancePerDependency();
        }
        #endregion

        #region Private class methods

        private static Logger CreateLogger(IComponentContext context)
        {
            return LogManager.GetCurrentClassLogger();
        }
        #endregion
    }
}