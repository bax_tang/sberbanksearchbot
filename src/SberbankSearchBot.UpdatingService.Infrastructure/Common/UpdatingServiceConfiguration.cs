﻿using System;

namespace SberbankSearchBot
{
    public sealed class UpdatingServiceConfiguration
    {
        #region Constants, fields and properties

        private const double UpdatingIntervalInHours = 4.0D; // 4 hours by default 

        private static UpdatingServiceConfiguration ConfigInstance;

        public static UpdatingServiceConfiguration Instance => ConfigInstance;

        public string ServiceName { get; set; } = "SberbankSearchBot.UpdatingService";

        public string ServiceDisplayName { get; set; } = "SberbankSearchBot database updating service";

        public string ServiceDescription { get; set; } = "Выполняет автоматическое обновление базы данных объектов бота Сбербанк.";

        public TimeSpan UpdatingInterval { get; set; } = TimeSpan.FromHours(UpdatingIntervalInHours);

        public string DatabaseConnectionString { get; set; } = "";

        public string InitialSiteUrl { get; set; } = "";
        #endregion

        #region Constructors

        static UpdatingServiceConfiguration()
        {
            ConfigInstance = new UpdatingServiceConfiguration();
        }

        internal UpdatingServiceConfiguration() { }
        #endregion

        #region Public class methods

        public static void SetInstance(UpdatingServiceConfiguration newInstance)
        {
            if (newInstance == null)
            {
                throw new ArgumentNullException(nameof(newInstance), "New instance of service configuration can't be null.");
            }

            ConfigInstance = newInstance;
        }
        #endregion
    }
}