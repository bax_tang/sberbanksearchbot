﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Newtonsoft.Json;

namespace SberbankSearchBot
{
    public class SubscriptionsController : ApiController
    {
        #region Constants and fields

        private BotActionsRepository ActionsRepository = null;
        #endregion

        #region Constructors

        public SubscriptionsController(BotActionsRepository actionsRepository)
        {
            if (actionsRepository == null)
            {
                throw new ArgumentNullException(nameof(actionsRepository), "Repository of bot actions can't be null.");
            }

            ActionsRepository = actionsRepository;
        }
        #endregion

        #region Public class methods

        public IHttpActionResult Get([FromUri]string subscriberKey)
        {
            IHttpActionResult actionResult = null;
            
            if (string.IsNullOrEmpty(subscriberKey))
            {
                actionResult = GetAllSubscriptions();
            }
            else
            {
                actionResult = GetSubscriberSubscriptions(subscriberKey);
            }

            return actionResult;
        }

        public IHttpActionResult Delete([FromUri]string subscriberKey)
        {
            IHttpActionResult actionResult = null;

            if (string.IsNullOrEmpty(subscriberKey))
            {
                actionResult = DeleteAllSubscriptions();
            }
            else
            {
                actionResult = DeleteSubscriberSubscriptions(subscriberKey);
            }

            return actionResult;
        }
        #endregion

        #region Private class methods

        private IHttpActionResult DeleteAllSubscriptions()
        {
            return Ok();
        }

        private IHttpActionResult DeleteSubscriberSubscriptions(string subscriberKey)
        {
            return Ok();
        }

        private IHttpActionResult GetAllSubscriptions()
        {
            var subscriptionInfos = ActionsRepository.Subscriptions.GetSubscriptionInfos();

            JsonSerializerSettings settings = Configuration.Formatters.JsonFormatter.SerializerSettings;

            return Json(subscriptionInfos, settings);
        }

        private IHttpActionResult GetSubscriberSubscriptions(string subscriberKey)
        {
            var subscriptionInfos = ActionsRepository.Subscriptions.GetSubscriptionInfos(subscriberKey);

            JsonSerializerSettings settings = Configuration.Formatters.JsonFormatter.SerializerSettings;

            return Json(subscriptionInfos, settings);
        }
        #endregion
    }
}