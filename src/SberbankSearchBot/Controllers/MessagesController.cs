﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using Microsoft.Bot.Connector;

using Newtonsoft.Json;

namespace SberbankSearchBot
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        #region Constants, fields and properties

        private BotActionsRepository ActionsRepository = null;
        #endregion

        #region Constructors

        public MessagesController(BotActionsRepository actionsRepository)
        {
            if (actionsRepository == null)
            {
                throw new ArgumentNullException(nameof(actionsRepository), "Repository of bot actions can't be null.");
            }

            ActionsRepository = actionsRepository;
        }
        #endregion

        #region Api methods
        /// <summary>
        /// POST: api/messages. Receive a message from a user and reply to it.
        /// </summary>
        public async Task<HttpResponseMessage> Post([FromBody]KeyedActivity activity)
        {
            ResourceResponse response = null;

            if (string.Equals(activity.Type, ActivityTypes.Message, StringComparison.OrdinalIgnoreCase))
            {
                response = await ProcessMessageActivityAsync(activity);
            }
            else
            {
                response = await ProcessSystemActivityAsync(activity);
            }

            HttpResponseMessage httpResponse = Request.CreateResponse(HttpStatusCode.OK);
            return httpResponse;
        }
        #endregion

        #region Private class methods

        private Task<ResourceResponse> ProcessMessageActivityAsync(KeyedActivity userActivity)
        {
            Task<ResourceResponse> responseTask = null;

            if (string.IsNullOrEmpty(userActivity.Text))
            {
                responseTask = Task.FromResult(new ResourceResponse("Success"));
            }
            else
            {
                BotActionsRepository actionsRepository = ActionsRepository;

                string activityText = userActivity.Text;
                string actionCode = ActionSource.GetActionCode(activityText);

                BotAction targetAction = null;
                bool exists = actionsRepository.TryGetAction(actionCode, out targetAction);
                if (exists)
                {
                    responseTask = targetAction.ProcessActivityAsync(userActivity);
                }
                else
                {
                    string activityKey = userActivity.GetActivityKey();
                    CurrentActionState actionState = actionsRepository.StatesExecutor.GetActionState(activityKey);
                    if (actionState != CurrentActionState.Unknown)
                    {
                        responseTask = actionsRepository.StatesExecutor.ProcessActionStateAsync(userActivity, actionState);
                    }
                    else if (actionsRepository.TryGetAction("start", out targetAction))
                    {
                        userActivity.Text = "start -h";
                        responseTask = targetAction.ProcessActivityAsync(userActivity);
                    }
                    else
                    {
                        responseTask = Task.FromResult(new ResourceResponse("Success"));
                    }
                }
            }

            return responseTask;
        }

        private Task<ResourceResponse> ProcessSystemActivityAsync(Activity systemActivity)
        {
            return Task.FromResult(new ResourceResponse("Success"));
        }
        #endregion
    }
}