﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Dependencies;

using Autofac;
using Autofac.Integration.WebApi;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace SberbankSearchBot
{
    public static class WebApiConfig
    {

        public static void Register(HttpConfiguration config)
        {
            // Json settings
            JsonSerializerSettings settings = config.Formatters.JsonFormatter.SerializerSettings;
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            settings.Formatting = Formatting.Indented;

            JsonConvert.DefaultSettings = GetDefaultSettings;

            // Web API configuration and services
            IContainer container = CreateContainer(config);
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

            container.Resolve<MessagesRepository>().PreloadMessages();
        }

        internal static JsonSerializerSettings GetDefaultSettings()
        {
            IContractResolver resolver = new CamelCasePropertyNamesContractResolver();

            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                ContractResolver = resolver,
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
            };

            return settings;
        }

        internal static IContainer CreateContainer(HttpConfiguration config)
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterWebApiModelBinderProvider();

            builder.RegisterInstance(config);

            Assembly current = Assembly.GetExecutingAssembly();
            builder.RegisterAssemblyTypes(current).Where(typeof(BotAction).IsAssignableFrom).As<BotAction>();

            builder.Register(GetConfiguration).SingleInstance();
            builder.Register(GetDatabaseConnection).InstancePerDependency();
            builder.Register(GetDataSource).SingleInstance();

            builder.Register(GetConfigurationRepository).SingleInstance();
            builder.Register(GetBotActionsRepository).SingleInstance();
            builder.RegisterType<ConnectorClientsRepository>().SingleInstance();
            builder.RegisterType<MessagesRepository>().SingleInstance();
            builder.RegisterType<ObjectsDataRepository>().SingleInstance();

            var assembly = typeof(WebApiConfig).Assembly;
            builder.RegisterApiControllers(assembly).InstancePerRequest();

            /*
            builder.RegisterType<MaintenanceController>().InstancePerRequest();
            builder.RegisterType<MessagesController>().InstancePerRequest();
            builder.RegisterType<SubscriptionsController>().InstancePerRequest();
            */
            
            IContainer container = builder.Build();
            return container;
        }

        internal static BotConfiguration GetConfiguration(IComponentContext context)
        {
            BotConfiguration configuration = BotConfiguration.Instance;

            string filePath = HostingEnvironment.MapPath("~/App_Data/BotConfiguration.json");
            if (File.Exists(filePath))
            {
                string configText = File.ReadAllText(filePath, Encoding.UTF8);

                configuration = JsonConvert.DeserializeObject<BotConfiguration>(configText);
                BotConfiguration.SetInstance(configuration);
            }

            return configuration;
        }

        internal static SqlConnection GetDatabaseConnection(IComponentContext context)
        {
            BotConfiguration configuration = context.Resolve<BotConfiguration>();

            SqlConnection connection = new SqlConnection(configuration.ConnectionString);
            connection.Open();
            return connection;
        }

        internal static IObjectsDataSource GetDataSource(IComponentContext context)
        {
            BotConfiguration configuration = context.Resolve<BotConfiguration>();
            string dataSourceType = configuration.DataSourceType;

            IObjectsDataSource dataSource = null;

            if (string.Equals(dataSourceType, "mssql", StringComparison.OrdinalIgnoreCase))
            {
                dataSource = GetSqlDataSource(context);
            }
            else if (string.Equals(dataSourceType, "json", StringComparison.OrdinalIgnoreCase))
            {
                dataSource = GetJsonDataSource();
            }
            else
            {
                throw new InvalidOperationException("Unknown data source type.");
            }

            return dataSource;
        }

        internal static IObjectsDataSource GetSqlDataSource(IComponentContext context)
        {
            BotConfiguration configuration = context.Resolve<BotConfiguration>();
            Func<SqlConnection> connectionCreator = context.Resolve<Func<SqlConnection>>();

            return new ObjectsSqlDataSource(connectionCreator, configuration);
        }

        internal static IObjectsDataSource GetJsonDataSource()
        {
            string dataFilePath = HostingEnvironment.MapPath("~/App_Data/ObjectsData.json");

            return new ObjectsJsonDataSource(dataFilePath);
        }
                
        internal static SearchConfigurationRepository GetConfigurationRepository(IComponentContext context)
        {
            string filePath = HostingEnvironment.MapPath("~/App_Data/SearchConfiguration.json");

            return new SearchConfigurationRepository(filePath);
        }

        internal static BotActionsRepository GetBotActionsRepository(IComponentContext context)
        {
            var configRepository = context.Resolve<SearchConfigurationRepository>();
            var dataRepository = context.Resolve<ObjectsDataRepository>();
            var clientsRepository = context.Resolve<ConnectorClientsRepository>();
            var messagesRepository = context.Resolve<MessagesRepository>();

            BotActionsRepository actionsRepository = new BotActionsRepository(
                clientsRepository,
                configRepository,
                dataRepository,
                messagesRepository);

            IEnumerable<BotAction> detectedActions = context.Resolve<IEnumerable<BotAction>>();
            foreach (BotAction detectedAction in detectedActions)
            {
                actionsRepository.AddAction(detectedAction);
            }

            return actionsRepository;
        }
    }
}
