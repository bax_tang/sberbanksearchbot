﻿using System;
using System.Collections.Concurrent;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class BotActionsRepository
    {
        #region Constants and fields

        private readonly ConcurrentDictionary<string, BotAction> ActionStore;

        internal readonly ObjectsDataRepository ObjectsData;

        internal readonly SearchConfigurationRepository Configuration;

        internal readonly ConnectorClientsRepository Connectors;

        internal readonly ActionStatesExecutor StatesExecutor;

        internal readonly UserFavoritesRepository Favorites;

        internal readonly SearchSubscriptionsRepository Subscriptions;

        internal readonly SystemConfigurationRepository SystemConfiguration;

        internal readonly MessagesRepository Messages;
        #endregion

        #region Constructors

        private BotActionsRepository()
        {
            ActionStore = new ConcurrentDictionary<string, BotAction>(StringComparer.OrdinalIgnoreCase);
        }

        public BotActionsRepository(
            ConnectorClientsRepository connectorsRepository,
            SearchConfigurationRepository configurationRepository,
            ObjectsDataRepository dataRepository,
            MessagesRepository messagesRepository) : this()
        {
            if (connectorsRepository == null)
            {
                throw new ArgumentNullException(nameof(connectorsRepository), "Connectors repository can't be null.");
            }
            if (configurationRepository == null)
            {
                throw new ArgumentNullException(nameof(configurationRepository), "Configuration repository can't be null.");
            }
            if (dataRepository == null)
            {
                throw new ArgumentNullException(nameof(dataRepository), "Data repository can't be null.");
            }
            if (messagesRepository == null)
            {
                throw new ArgumentNullException(nameof(messagesRepository), "Messages repository can't be null.");
            }

            Configuration = configurationRepository;
            Connectors = connectorsRepository;
            Messages = messagesRepository;
            ObjectsData = dataRepository;

            Favorites = new UserFavoritesRepository();
            StatesExecutor = new ActionStatesExecutor(this);
            Subscriptions = new SearchSubscriptionsRepository(this);
            SystemConfiguration = new SystemConfigurationRepository();
        }
        #endregion

        #region Public class methods

        public bool AddAction(BotAction actionInstance)
        {
            if (actionInstance == null)
            {
                throw new ArgumentNullException(nameof(actionInstance), "Bot action can't be null.");
            }
            
            bool added = ActionStore.TryAdd(actionInstance.ActionCode, actionInstance);
            if (added)
            {
                actionInstance.SetParentRepository(this);
            }
            return added;
        }

        public BotAction GetAction(string actionCode)
        {
            BotAction targetAction = null;
            bool exists = ActionStore.TryGetValue(actionCode, out targetAction);
            if (!exists)
            {
                ActionStore.TryGetValue("start", out targetAction);
            }
            return targetAction;
        }

        public bool TryGetAction(string actionCode, out BotAction targetAction)
        {
            return ActionStore.TryGetValue(actionCode, out targetAction);
        }
        #endregion
    }
}