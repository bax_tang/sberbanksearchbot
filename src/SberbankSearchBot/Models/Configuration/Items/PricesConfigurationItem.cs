﻿using System;

namespace SberbankSearchBot
{
    public class PricesConfigurationItem : SearchConfigurationItem
    {
        #region Constants and fields

        private const double UnknownPrice = -1.0D;

        public double MinPrice;

        public double MaxPrice;

        public bool MinPriceExists;

        public bool MaxPriceExists;

        public string MinPriceHeader;

        public string MaxPriceHeader;
        #endregion

        #region Constructors

        public PricesConfigurationItem(double minPrice = UnknownPrice, double maxPrice = UnknownPrice) : base(BotConstants.PriceActionCode, "Цена", "любая")
        {
            MinPrice = minPrice;
            MaxPrice = maxPrice;
            MinPriceExists = false;
            MaxPriceExists = false;
            MinPriceHeader = string.Empty;
            MaxPriceHeader = string.Empty;
        }
        #endregion

        #region SearchConfigurationItem methods overriding

        public override void Reset()
        {
            base.Reset();

            MinPrice = UnknownPrice;
            MinPriceExists = false;
            MinPriceHeader = string.Empty;

            MaxPrice = UnknownPrice;
            MaxPriceExists = false;
            MaxPriceHeader = string.Empty;
        }

        public override string ToString()
        {
            string result = string.Empty;

            if (MinPriceExists && MaxPriceExists)
            {
                result = string.Concat(ItemName, ": от ", MinPriceHeader, " до ", MaxPriceHeader);
            }
            else if (MinPriceExists)
            {
                result = string.Concat(ItemName, ": от ", MinPriceHeader);
            }
            else if (MaxPriceExists)
            {
                result = string.Concat(ItemName, ": до ", MaxPriceHeader);
            }
            else
            {
                result = string.Concat(ItemName, ": ", AnyHeader);
            }

            return result;
        }
        #endregion

        #region Public class methods

        public void ChangePrice(double priceValue, string priceHeader, PriceType priceType)
        {
            if (priceType != PriceType.Unknown)
            {
                if (priceType == PriceType.MinPrice)
                {
                    MinPrice = priceValue;
                    MinPriceHeader = priceHeader;
                    MinPriceExists = true;
                }
                else if (priceType == PriceType.MaxPrice)
                {
                    MaxPrice = priceValue;
                    MaxPriceHeader = priceHeader;
                    MaxPriceExists = true;
                }
            }
        }
        #endregion
    }
}