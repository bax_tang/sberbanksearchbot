﻿using System;

namespace SberbankSearchBot
{
    public class RegionConfigurationItem : SearchConfigurationItem
    {
        #region Constants and fields

        public string RegionName;
        #endregion

        #region Constructors

        public RegionConfigurationItem() : base(BotConstants.RegionActionCode, "Регион", "любой")
        {
            RegionName = string.Empty;
        }
        #endregion

        #region SearchConfigurationItem methods overriding

        public override void Reset()
        {
            base.Reset();

            RegionName = string.Empty;
        }

        public override string ToString()
        {
            string result = string.Empty;

            string regionName = RegionName;
            if (string.IsNullOrEmpty(regionName))
            {
                result = base.ToString();
            }
            else
            {
                result = string.Concat(ItemName, ": ", RegionName);
            }

            return result;
        }
        #endregion
    }
}