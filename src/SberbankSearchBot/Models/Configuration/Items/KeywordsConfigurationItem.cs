﻿using System;

namespace SberbankSearchBot
{
    public class KeywordsConfigurationItem : SearchConfigurationItem
    {
        #region Constants and fields

        public string[] Keywords;
        #endregion

        #region Constructors

        public KeywordsConfigurationItem() : base(BotConstants.KeywordsActionCode, "Ключевые слова", "любые")
        {
            Keywords = new string[0];
        }
        #endregion

        #region SearchConfigurationItem methods overriding

        public override void Reset()
        {
            base.Reset();

            Keywords = new string[0];
        }

        public override string ToString()
        {
            string result = string.Empty;

            if (Keywords.Length > 0)
            {
                result = string.Concat(ItemName, ": ", string.Join("; ", Keywords));
            }
            else
            {
                result = base.ToString();
            }

            return result;
        }
        #endregion
    }
}