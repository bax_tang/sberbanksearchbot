﻿using System;

namespace SberbankSearchBot
{
    public class SubscriptionInfo
    {
        #region Constants and fields

        public string SectionName;

        public string Parameters;
        #endregion

        #region Constructors

        internal SubscriptionInfo() { }
        #endregion
    }
}