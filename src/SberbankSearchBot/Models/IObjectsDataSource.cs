﻿using System;
using System.Collections.Generic;

namespace SberbankSearchBot
{
    public interface IObjectsDataSource
    {

        IList<string> GetObjectIdentifiers(string sectionName);

        ObjectData GetByID(string objectID);
        
        IList<string> GetSections();

        IList<string> GetCategories(string sectionName);

        IList<string> GetRegions(string sectionName);

        IList<string> GetCities(string sectionName, string regionName);

        IList<int> GetTransportIssueYears();
    }
}