﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class SearchSubscription
    {
        #region Constants and fields

        private static readonly Random rnd;

        private static ConfigurationBuilder Builder;

        public readonly ConnectorClient Connector;

        public readonly Activity InitialActivity;

        public string ActivityKey;

        public SearchConfiguration Configuration;

        public HashSet<string> SearchResults;

        public event EventHandler Execute;

        private CancellationTokenSource TokenSource;

        private Task SubscriptionTask;
        #endregion

        #region Constructors

        static SearchSubscription()
        {
            rnd = new Random();
            Builder = new ConfigurationBuilder();
        }

        private SearchSubscription()
        {
            TokenSource = new CancellationTokenSource();
        }

        public SearchSubscription(ConnectorClient connector, Activity initialActivity) : this()
        {
            if (connector == null)
            {
                throw new ArgumentNullException(nameof(connector), "Connector can't be null.");
            }
            if (initialActivity == null)
            {
                throw new ArgumentNullException(nameof(initialActivity), "Initial activity can't be null.");
            }

            Connector = connector;
            InitialActivity = initialActivity;
        }
        #endregion

        #region Public class methods

        public void Start()
        {
            CreateSubscriptionTask();
        }

        public void Cancel()
        {
            TokenSource.Cancel();
        }

        public string GetTitle()
        {
            return string.Format(BotConstants.SubscriptionCardTitleFormat, Configuration.SectionName);
        }

        public string GetText()
        {
            SearchConfiguration configuration = Configuration;

            return BuildConfigurationText(configuration);
        }
        #endregion

        #region Private class methods

        private void CreateSubscriptionTask()
        {
            SubscriptionTask = Task.Run(new Action(ExecuteSubscription), TokenSource.Token);
        }

        private void ExecuteSubscription()
        {
            while (true)
            {
                if (TokenSource.IsCancellationRequested) break;

                int delayInterval = rnd.Next(BotConstants.MinDelayInterval, BotConstants.MaxDelayInterval);
                Thread.Sleep(delayInterval);

                if (TokenSource.IsCancellationRequested) break;

                Execute?.Invoke(this, EventArgs.Empty);

                if (TokenSource.IsCancellationRequested) break;
            }
        }

        private string BuildConfigurationText(SearchConfiguration configuration)
        {
            StringBuilder result = new StringBuilder(64);
            
            for (int index = 0; index < configuration.Count; ++index)
            {
                SearchConfigurationItem configItem = configuration[index];
                if (configItem.Enabled)
                {
                    string text = configItem.ToString();

                    result.Append(text).AppendLine();
                }
            }

            return result.ToString();
        }
        #endregion
    }
}