﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace SberbankSearchBot
{
    public class ObjectsSqlDataSource : IObjectsDataSource
    {
        #region Constants and fields

        private readonly BotConfiguration Configuration;

        private readonly Func<SqlConnection> ConnectionCreator;

        private int ExecutionTimeout = 300;
        #endregion

        #region Constructors

        public ObjectsSqlDataSource(Func<SqlConnection> connectionCreator, BotConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration), "Bot configuration can't be null.");
            }
            if (connectionCreator == null)
            {
                throw new ArgumentNullException(nameof(connectionCreator), "Connection creator can't be null or empty.");
            }

            Configuration = configuration;
            ConnectionCreator = connectionCreator;
            ExecutionTimeout = configuration.ExecutionTimeout;
        }
        #endregion

        #region IObjectsDataSource implementation

        public ObjectData GetByID(string objectID)
        {
            ObjectData result = null;

            long identifier = 0L;
            bool isParsed = long.TryParse(objectID, NumberStyles.Any, CultureInfo.InvariantCulture, out identifier);
            if (isParsed)
            {
                using (SqlConnection connection = ConnectionCreator.Invoke())
                {
                    result = GetObjectDataCore(connection, identifier);
                }
            }

            return result ?? ObjectData.Empty;
        }

        public IList<string> GetObjectIdentifiers(string sectionName)
        {
            IList<string> identifiers = null;

            using (SqlConnection connection = ConnectionCreator.Invoke())
            {
                identifiers = GetObjectIdentifiersCore(connection, sectionName, Configuration.Department);
            }

            return identifiers ?? new List<string>();
        }

        public IList<string> GetSections()
        {
            IList<string> sectionNames = null;

            using (SqlConnection connection = ConnectionCreator.Invoke())
            {
                sectionNames = GetSectionsCore(connection, Configuration.Department);
            }

            return sectionNames ?? new List<string>();
        }

        public IList<string> GetCategories(string sectionName)
        {
            IList<string> categoryNames = null;

            using (SqlConnection connection = ConnectionCreator.Invoke())
            {
                categoryNames = GetCategoriesCore(connection, sectionName, Configuration.Department);
            }

            return categoryNames ?? new List<string>();
        }

        public IList<string> GetRegions(string sectionName)
        {
            IList<string> regionNames = null;

            using (SqlConnection connection = ConnectionCreator.Invoke())
            {
                regionNames = GetRegionsCore(connection, sectionName, Configuration.Department);
            }

            return regionNames ?? new List<string>();
        }

        public IList<string> GetCities(string sectionName, string regionName)
        {
            IList<string> cityNames = null;

            using (SqlConnection connection = ConnectionCreator.Invoke())
            {
                cityNames = GetCitiesCore(connection, sectionName, regionName, Configuration.Department);
            }

            return cityNames ?? new List<string>();
        }

        public IList<int> GetTransportIssueYears()
        {
            IList<int> issueYears = null;

            using (SqlConnection connection = ConnectionCreator.Invoke())
            {
                issueYears = GetTransportIssueYearsCore(connection, Configuration.Department);
            }

            return issueYears ?? new List<int>();
        }
        #endregion

        #region Private class methods

        private IList<string> GetObjectIdentifiersCore(SqlConnection connection, string sectionName, Guid departmentID)
        {
            List<string> identifiers = null;

            using (SqlCommand selectCommand = CreateGetObjectIdentifiersCommand(connection, sectionName, departmentID))
            {
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read())
                {
                    identifiers = new List<string>(50);

                    int idOrdinal = reader.GetOrdinal("ID");
                    do
                    {
                        identifiers.Add(reader.GetString(idOrdinal));
                    }
                    while (reader.Read());

                    reader.Close();
                }
            }

            return identifiers;
        }

        private IList<string> GetSectionsCore(SqlConnection connection, Guid departmentID)
        {
            List<string> sectionNames = null;

            using (SqlCommand selectCommand = CreateGetSectionsCommand(connection, departmentID))
            {
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read())
                {
                    sectionNames = new List<string>(6);

                    int nameOrdinal = reader.GetOrdinal("Name");
                    do
                    {
                        sectionNames.Add(reader.GetString(nameOrdinal));
                    }
                    while (reader.Read());

                    reader.Close();
                }
            }

            return sectionNames;
        }

        private IList<string> GetCategoriesCore(SqlConnection connection, string sectionName, Guid departmentID)
        {
            List<string> categoryNames = null;

            using (SqlCommand selectCommand = CreateGetCategoriesCommand(connection, sectionName, departmentID))
            {
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read())
                {
                    categoryNames = new List<string>(6);

                    int nameOrdinal = reader.GetOrdinal("Name");
                    do
                    {
                        categoryNames.Add(reader.GetString(nameOrdinal));
                    }
                    while (reader.Read());

                    reader.Close();
                }
            }

            return categoryNames;
        }

        private IList<string> GetRegionsCore(SqlConnection connection, string sectionName, Guid departmentID)
        {
            List<string> regionNames = null;

            using (SqlCommand selectCommand = CreateGetRegionsCommand(connection, sectionName, departmentID))
            {
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read())
                {
                    regionNames = new List<string>(10);

                    int nameOrdinal = reader.GetOrdinal("Name");
                    do
                    {
                        regionNames.Add(reader.GetString(nameOrdinal));
                    }
                    while (reader.Read());

                    reader.Close();
                }
            }

            return regionNames;
        }

        private IList<string> GetCitiesCore(SqlConnection connection, string sectionName, string regionName, Guid departmentID)
        {
            List<string> cityNames = null;

            using (SqlCommand selectCommand = CreateGetCitiesCommand(connection, sectionName, regionName, departmentID))
            {
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read())
                {
                    cityNames = new List<string>(15);

                    int nameOrdinal = reader.GetOrdinal("Name");
                    do
                    {
                        cityNames.Add(reader.GetString(nameOrdinal));
                    }
                    while (reader.Read());

                    reader.Close();
                }
            }

            return cityNames;
        }

        private IList<int> GetTransportIssueYearsCore(SqlConnection connection, Guid departmentID)
        {
            List<int> issueYears = null;

            using (SqlCommand selectCommand = CreateGetTransportIssueYearsCommand(connection, departmentID))
            {
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read())
                {
                    issueYears = new List<int>(10);

                    int yearOrdinal = reader.GetOrdinal("IssueYear");
                    do
                    {
                        if (reader.IsDBNull(yearOrdinal)) continue;

                        issueYears.Add(reader.GetInt32(yearOrdinal));
                    }
                    while (reader.Read());

                    reader.Close();
                }
            }

            return issueYears;
        }

        private ObjectData GetObjectDataCore(SqlConnection connection, long objectID)
        {
            ObjectData objectData = null;

            bool exists = FillObjectDataMainInfo(connection, objectID, out objectData);
            if (exists)
            {
                FillObjectDataImages(connection, objectID, objectData);
            }

            return objectData;
        }

        private bool FillObjectDataMainInfo(SqlConnection connection, long objectID, out ObjectData objectData)
        {
            bool exists = false; objectData = null;

            using (SqlCommand selectCommand = CreateGetObjectMainInfoCommand(connection, objectID))
            {
                SqlDataReader reader = selectCommand.ExecuteReader();
                exists = reader.Read();
                if (exists)
                {
                    objectData = new ObjectData();
                    objectData.ID = reader.GetString(reader.GetOrdinal("ObjectID"));
                    objectData.Section = reader.GetString(reader.GetOrdinal("SectionName"));
                    objectData.Category = reader.GetString(reader.GetOrdinal("CategoryName"));
                    objectData.Region = reader.GetString(reader.GetOrdinal("RegionName"));
                    objectData.City = reader.GetString(reader.GetOrdinal("CityName"));
                    objectData.Name = reader.GetString(reader.GetOrdinal("ObjectName"));
                    objectData.Description = reader.GetString(reader.GetOrdinal("ObjectDescription"));
                    objectData.URL = reader.GetString(reader.GetOrdinal("ObjectUrl"));

                    int priceOrdinal = reader.GetOrdinal("ObjectPrice");
                    if (!reader.IsDBNull(priceOrdinal))
                    {
                        decimal price = reader.GetDecimal(priceOrdinal);
                        objectData.Price = (double)price;
                    }

                    int issueYearOrdinal = reader.GetOrdinal("IssueYear");
                    if (!reader.IsDBNull(issueYearOrdinal))
                    {
                        objectData.Year = reader.GetInt32(issueYearOrdinal);
                    }
                }
                reader.Close();
            }

            return exists;
        }

        private void FillObjectDataImages(SqlConnection connection, long objectID, ObjectData objectData)
        {
            using (SqlCommand selectCommand = CreateGetObjectImagesCommand(connection, objectID))
            {
                SqlDataReader reader = selectCommand.ExecuteReader();
                if (reader.Read())
                {
                    List<string> imageUrls = new List<string>(10);

                    int urlOrdinal = reader.GetOrdinal("ImageUrl");
                    do
                    {
                        imageUrls.Add(reader.GetString(urlOrdinal));
                    }
                    while (reader.Read());

                    objectData.Images = imageUrls;
                }
                else objectData.Images = new string[0];
            }
        }
        #endregion

        #region SqlCommand creation methods

        private SqlCommand CreateGetObjectIdentifiersCommand(SqlConnection connection, string sectionName, Guid departmentID)
        {
            string commandText = StoredProcedures.GetObjectIdentifiersProcedureName;

            SqlCommand selectCommand = new SqlCommand(commandText, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            };

            selectCommand.Parameters.AddWithValue("DepartmentID", departmentID);
            selectCommand.Parameters.AddWithValue("SectionName", sectionName);

            return selectCommand;
        }

        private SqlCommand CreateGetSectionsCommand(SqlConnection connection, Guid departmentID)
        {
            string commandText = StoredProcedures.GetSectionsProcedureName;

            SqlCommand selectCommand = new SqlCommand(commandText, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            };

            selectCommand.Parameters.AddWithValue("DepartmentID", departmentID);

            return selectCommand;
        }

        private SqlCommand CreateGetCategoriesCommand(SqlConnection connection, string sectionName, Guid departmentID)
        {
            string commandText = StoredProcedures.GetCategoriesBySectionProcedureName;

            SqlCommand selectCommand = new SqlCommand(commandText, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            };

            selectCommand.Parameters.AddWithValue("DepartmentID", departmentID);
            selectCommand.Parameters.AddWithValue("SectionName", sectionName);

            return selectCommand;
        }

        private SqlCommand CreateGetRegionsCommand(SqlConnection connection, string sectionName, Guid departmentID)
        {
            string commandText = StoredProcedures.GetRegionsBySectionProcedureName;

            SqlCommand selectCommand = new SqlCommand(commandText, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            };

            selectCommand.Parameters.AddWithValue("DepartmentID", departmentID);
            selectCommand.Parameters.AddWithValue("SectionName", sectionName);

            return selectCommand;
        }

        private SqlCommand CreateGetCitiesCommand(SqlConnection connection, string sectionName, string regionName, Guid departmentID)
        {
            string commandText = StoredProcedures.GetCityBySectionAndRegionProcedureName;

            SqlCommand selectCommand = new SqlCommand(commandText, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            };

            selectCommand.Parameters.AddWithValue("DepartmentID", departmentID);
            selectCommand.Parameters.AddWithValue("SectionName", sectionName);
            selectCommand.Parameters.AddWithValue("RegionName", regionName);

            return selectCommand;
        }

        private SqlCommand CreateGetTransportIssueYearsCommand(SqlConnection connection, Guid departmentID)
        {
            string commandText = StoredProcedures.GetTransportIssueYearsProcedureName;

            SqlCommand selectCommand = new SqlCommand(commandText, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            };

            selectCommand.Parameters.AddWithValue("DepartmentID", departmentID);

            return selectCommand;
        }

        private SqlCommand CreateGetObjectMainInfoCommand(SqlConnection connection, long objectID)
        {
            string commandText = StoredProcedures.GetObjectMainInfoProcedureName;

            SqlCommand selectCommand = new SqlCommand(commandText, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            };

            selectCommand.Parameters.AddWithValue("ObjectID", objectID);

            return selectCommand;
        }

        private SqlCommand CreateGetObjectImagesCommand(SqlConnection connection, long objectID)
        {
            string commandText = StoredProcedures.GetObjectImagesProcedureName;

            SqlCommand selectCommand = new SqlCommand(commandText, connection)
            {
                CommandTimeout = ExecutionTimeout,
                CommandType = CommandType.StoredProcedure
            };

            selectCommand.Parameters.AddWithValue("ObjectID", objectID);

            return selectCommand;
        }
        #endregion
    }

    internal static class StoredProcedures
    {
        #region Constants and fields

        public const string GetObjectIdentifiersProcedureName = "[dbo].[sp_object_get_ids_by_section]";

        public const string GetSectionsProcedureName = "[dbo].[sp_section_get_names]";

        public const string GetCategoriesBySectionProcedureName = "[dbo].[sp_category_get_by_section_name]";

        public const string GetRegionsBySectionProcedureName = "[dbo].[sp_region_get_by_section_name]";

        public const string GetCityBySectionAndRegionProcedureName = "[dbo].[sp_city_get_by_section_and_region]";

        public const string GetTransportIssueYearsProcedureName = "[dbo].[sp_object_transport_get_issue_years]";

        public const string GetObjectMainInfoProcedureName = "[dbo].[sp_object_get_data]";

        public const string GetObjectImagesProcedureName = "[dbo].[sp_object_image_get_data]";
        #endregion
    }
}