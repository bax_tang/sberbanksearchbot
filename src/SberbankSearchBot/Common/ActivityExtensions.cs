﻿using System;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public static class ActivityExtensions
    {
        #region Public class methods

        public static string GetActivityKey(this Activity userActivity)
        {
            if (userActivity == null)
            {
                throw new ArgumentNullException(nameof(userActivity), "Activity can't be null.");
            }

            string activityKey = null;

            KeyedActivity keyedActivity = userActivity as KeyedActivity;
            if (keyedActivity != null)
            {
                activityKey = keyedActivity.GetActivityKey();
            }
            else
            {
                if (userActivity.From == null)
                {
                    throw new ArgumentNullException(nameof(userActivity.From), "Sender can't be null.");
                }
                if (userActivity.Conversation == null)
                {
                    throw new ArgumentNullException(nameof(userActivity.Conversation), "Conversation can't be null.");
                }

                activityKey = string.Concat(userActivity.From.Id, "|", userActivity.Conversation.Id);
            }

            return activityKey;
        }
        #endregion
    }
}