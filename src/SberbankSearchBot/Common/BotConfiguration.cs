﻿using System;

namespace SberbankSearchBot
{
    /// <summary>
    /// Предоставляет параметры конфигурации бота. Данный класс не может наследоваться.
    /// </summary>
    public sealed class BotConfiguration
    {
        #region Constants, fields and properties

        private static BotConfiguration ConfigInstance;

        /// <summary>
        /// Возвращает текущий экземпляр конфигурации
        /// </summary>
        public static BotConfiguration Instance => ConfigInstance;

        /// <summary>
        /// Возвращает или задаёт строку подключения к БД.
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// Возвращает или задаёт идентификатор подразделения ПАО Сбербанк, по которому выполняется выборка.
        /// </summary>
        public Guid Department { get; set; }
        /// <summary>
        /// Возвращает или задаёт тип используемого источника данных.
        /// Значение по умолчанию - "json".
        /// </summary>
        public string DataSourceType { get; set; } = "json";
        /// <summary>
        /// Возвращает или задаёт таймаут выполнения команды на SQL Server в секундах.
        /// Значение по умолчанию - 300.
        /// </summary>
        public int ExecutionTimeout { get; set; } = 300;
        #endregion

        #region Constructors

        static BotConfiguration()
        {
            ConfigInstance = new BotConfiguration();
        }

        public BotConfiguration() { }
        #endregion

        #region Public class methods

        public static void SetInstance(BotConfiguration newInstance)
        {
            if (newInstance == null)
            {
                throw new ArgumentNullException(nameof(newInstance), "Bot configuration instance can't be null.");
            }

            ConfigInstance = newInstance;
        }
        #endregion
    }
}