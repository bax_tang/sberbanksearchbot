﻿using System;
using System.Text;

using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public static class JsonPathBuilder
    {
        #region Public class methods

        public static string BuildJsonPath(JObject searchConfiguration)
        {
            if (searchConfiguration == null)
            {
                throw new ArgumentNullException(nameof(searchConfiguration), "Search configuration can't be null or empty.");
            }

            StringBuilder pathBuilder = new StringBuilder("$..[?(", 128);
            
            string sectionName = (string)searchConfiguration["Section"];
            pathBuilder.Append("(@.Section=='").Append(sectionName).Append("')");

            JArray searchParameters = searchConfiguration["Parameters"] as JArray;
            JObject categoryParameter = searchParameters.SelectToken("$..[?(@.Code=='category')]", false) as JObject;
            if (categoryParameter != null)
            {
                pathBuilder.Append("&&").Append('(');

                JArray categories = categoryParameter["Value"] as JArray;
                for (int index = 0; index < categories.Count; ++index)
                {
                    JObject categoryItem = categories[index] as JObject;
                    if ((bool)categoryItem["enabled"])
                    {
                        string category = (string)categoryItem["name"];
                        pathBuilder.Append("@.Category=='").Append(category).Append("'||");
                    }
                }

                pathBuilder.Remove(pathBuilder.Length - 2, 2).Append(')');
            }

            pathBuilder.Append(")]");

            return pathBuilder.ToString();
        }
        #endregion
    }
}