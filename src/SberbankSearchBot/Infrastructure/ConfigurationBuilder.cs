﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class ConfigurationBuilder
    {
        #region Constants and fields

        private readonly ConcurrentDictionary<string, Func<JToken, string>> Builders;

        private readonly JsonConverter[] Converters;
        #endregion

        #region Constructors

        public ConfigurationBuilder()
        {
            var builders = new ConcurrentDictionary<string, Func<JToken, string>>(StringComparer.OrdinalIgnoreCase);

            builders["category"] = BuildCategoriesToken;
            builders["region"] = BuildRegionsToken;
            builders["price"] = BuildPricesToken;
            builders["year"] = BuildYearsToken;
            builders["keywords"] = BuildKeywordsToken;

            Builders = builders;
            Converters = new JsonConverter[0];
        }
        #endregion

        #region Public class methods

        public string BuildTitleFromConfigItem(JObject configItem)
        {
            string actionCode = (string)configItem["Code"];
            string actionTitle = (string)configItem["Title"];

            JToken valueToken = configItem["Value"];
            if (valueToken != null)
            {
                string valueText = BuildTitleTextFromToken(actionCode, valueToken);
                actionTitle = string.Concat(actionTitle, ": ", valueText);
            }
            else
            {
                actionTitle = string.Concat(actionTitle, ": ", (string)configItem["Any"]);
            }

            return actionTitle;
        }
        #endregion

        #region Private class methods

        private string BuildTitleTextFromToken(string actionCode, JToken valueToken)
        {
            Func<JToken, string> builder = null;
            bool exists = Builders.TryGetValue(actionCode, out builder);
            return (exists) ? builder.Invoke(valueToken) : valueToken.ToString(Formatting.None, Converters);
        }

        private static string BuildCategoriesToken(JToken categoriesToken)
        {
            StringBuilder builder = new StringBuilder(64);

            bool anyEnabled = false;

            JArray categoriesArray = categoriesToken as JArray;
            for (int index = 0; index < categoriesArray.Count; ++index)
            {
                JToken arrayItem = categoriesArray[index];
                string categoryName = (string)arrayItem["name"];
                bool isEnabled = (bool)arrayItem["enabled"];
                if (isEnabled)
                {
                    anyEnabled = true;
                    builder.Append(categoryName).Append(", ");
                }
            }

            string result = null;

            if (anyEnabled)
            {
                builder.Remove(builder.Length - 2, 2);
                result = builder.ToString();
            }
            else result = "любая";

            return result;
        }

        private static string BuildRegionsToken(JToken regionsToken)
        {
            return string.Empty;
        }

        private static string BuildPricesToken(JToken pricesToken)
        {
            string minPriceHeader = null;
            string maxPriceHeader = null;

            JToken minPriceToken = pricesToken["min"];
            if (minPriceToken != null && minPriceToken.HasValues)
            {
                minPriceHeader = (string)minPriceToken["header"];
            }

            JToken maxPriceToken = pricesToken["max"];
            if (maxPriceToken != null && maxPriceToken.HasValues)
            {
                maxPriceHeader = (string)maxPriceToken["header"];
            }

            string result = null;

            if (minPriceHeader == null && maxPriceHeader == null)
            {
                result = "любая";
            }
            else
            {
                if (minPriceHeader == null)
                {
                    result = string.Concat("до ", maxPriceHeader);
                }
                else if (maxPriceHeader == null)
                {
                    result = string.Concat("от ", minPriceHeader);
                }
                else
                {
                    result = string.Concat("от ", minPriceHeader, " до ", maxPriceHeader);
                }
            }

            return result;
        }

        private static string BuildYearsToken(JToken yearsToken)
        {
            StringBuilder builder = new StringBuilder(32);

            bool anyEnabled = false;
            JArray yearsArray = yearsToken as JArray;
            for (int index = 0; index < yearsArray.Count; ++index)
            {
                JToken arrayItem = yearsArray[index];
                int year = (int)arrayItem["year"];
                bool isEnabled = (bool)arrayItem["enabled"];
                if (isEnabled)
                {
                    anyEnabled = true;
                    builder.Append(year).Append(", ");
                }
            }

            string result = null;

            if (anyEnabled)
            {
                builder.Remove(builder.Length - 2, 2);
                result = builder.ToString();
            }
            else result = "любой";

            return result;
        }

        private static string BuildKeywordsToken(JToken keywordsToken)
        {
            StringBuilder builder = new StringBuilder(64);

            JArray wordsArray = keywordsToken as JArray;
            for (int index = 0; index < wordsArray.Count; ++index)
            {
                string word = (string)wordsArray[index];
                builder.Append(word).Append(", ");
            }

            if (builder.Length > 2)
            {
                builder.Remove(builder.Length - 2, 2);
            }

            return builder.ToString();
        }
        #endregion
    }
}