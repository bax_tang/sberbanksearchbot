﻿using System;
using System.Globalization;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public static class ActionSource
    {
        #region Public class methods

        public static string GetActionCode(string activityText)
        {
            string result = null;

            int slashIndex = activityText.IndexOf('/');
            int spaceIndex = activityText.IndexOf(' ');

            if ((slashIndex == -1) && (spaceIndex == -1))
            {
                result = activityText;
            }
            else
            {
                int startIndex = (slashIndex != -1) ? 1 + slashIndex : 0;
                int length = (spaceIndex != -1) ? (spaceIndex - startIndex) : activityText.Length - 1;

                result = activityText.Substring(startIndex, length);
            }

            return result;
        }

        public static CardAction CreateStartAction(string title = BotConstants.GreetingActionTitle, string value = BotConstants.GreetingActionCodeWithoutHeader)
        {
            return new CardAction(ActionTypes.PostBack, title, null, value);
        }

        public static CardAction CreateBackAction(string targetAction)
        {
            string value = string.Concat(BotConstants.BackActionCode, " ", targetAction);

            return new CardAction(ActionTypes.PostBack, BotConstants.BackActionTitle, null, value);
        }

        public static CardAction CreateForwardAction()
        {
            return new CardAction(ActionTypes.PostBack, BotConstants.ForwardActionTitle, null, BotConstants.ForwardActionCode);
        }

        public static CardAction CreateSearchAction(string title = BotConstants.SearchActionTitle)
        {
            return new CardAction(ActionTypes.PostBack, title, null, BotConstants.SearchActionCode);
        }

        public static CardAction CreateSectionAction(string sectionName)
        {
            string value = string.Concat(BotConstants.SectionActionCode, " ", sectionName);

            return new CardAction(ActionTypes.PostBack, sectionName, null, value);
        }

        public static CardAction CreateCategoryAction(string categoryName, bool isEnabled)
        {
            string title = (isEnabled) ? categoryName + ": включена" : categoryName;
            string value = "category set " + categoryName;

            return new CardAction(ActionTypes.PostBack, title, null, value);
        }
        
        public static CardAction CreateYearAction(int year, bool isEnabled)
        {
            string title = (isEnabled) ? $"{year}: включен" : $"{year}";
            string value = $"year set {year}";

            return new CardAction(ActionTypes.PostBack, title, null, value);
        }

        public static CardAction CreateAddToFavoritesAction(string objectID)
        {
            string value = string.Concat("add_to_favorites ", objectID);

            return new CardAction(ActionTypes.PostBack, BotConstants.AddToFavoritesActionTitle, null, value);
        }

        public static CardAction CreateRemoveFromFavoritesAction(string objectID)
        {
            string value = string.Concat("remove_from_favorites ", objectID);

            return new CardAction(ActionTypes.PostBack, BotConstants.RemoveFromFavoritesActionTitle, null, value);
        }

        public static CardAction CreateManageFavoritesAction(string title = BotConstants.ManageFavoritesActionTitle)
        {
            return new CardAction(ActionTypes.PostBack, title, null, BotConstants.ManageFavoritesActionCode);
        }

        public static CardAction CreateManageSubscriptionsAction()
        {
            return new CardAction(ActionTypes.PostBack, BotConstants.ManageSubscriptionsActionTitle, null, BotConstants.ManageSubscriptionsActionCode);
        }

        public static CardAction CreateClearSubscriptionsAction()
        {
            return new CardAction(ActionTypes.PostBack, BotConstants.ClearSubscriptionsActionTitle, null, BotConstants.ClearSubscriptionsActionCode);
        }

        public static CardAction CreateAddSubscriptionAction()
        {
            return new CardAction(ActionTypes.PostBack, BotConstants.AddSubscriptionActionTitle, null, BotConstants.AddSubscriptionActionCode);
        }

        public static CardAction CreateRemoveSubscriptionAction(int index, string title = BotConstants.RemoveSubscriptionActionTitle)
        {
            string indexText = index.ToString();
            string value = string.Concat(BotConstants.RemoveSubscriptionActionCode, " ", indexText);

            return new CardAction(ActionTypes.PostBack, title, null, value);
        }

        public static CardAction CreateCityAction(string cityName, bool isEnabled)
        {
            string title = (isEnabled) ? cityName + ": включен" : cityName;
            string value = "city set " + cityName;

            return new CardAction(ActionTypes.PostBack, title, null, value);
        }

        public static CardAction CreatePriceAction(string title, bool isMinPrice)
        {
            string value = string.Concat("price", " ", (isMinPrice) ? "min" : "max");

            return new CardAction(ActionTypes.PostBack, title, null, value);
        }

        public static CardAction CreateSetPriceAction(string price, int index, bool isMinPrice)
        {
            string value = string.Concat("price set ", (isMinPrice) ? "min " : "max ", index.ToString(CultureInfo.InvariantCulture));

            return new CardAction(ActionTypes.PostBack, price, null, value);
        }

        public static CardAction CreateCustomPriceAction(string title, string value)
        {
            return new CardAction(ActionTypes.PostBack, title, null, value);
        }

        public static CardAction CreateDoSearchAction()
        {
            return new CardAction(ActionTypes.PostBack, BotConstants.DoSearchActionTitle, null, BotConstants.DoSearchActionCode);
        }

        public static CardAction CreateRetrieveNextAction()
        {
            return new CardAction(ActionTypes.PostBack, BotConstants.RetrieveNextActionTitle, null, BotConstants.RetrieveNextActionCode);
        }

        public static CardAction CreateBookAction(string objectID)
        {
            string value = string.Concat("book ", objectID);

            return new CardAction(ActionTypes.PostBack, BotConstants.BookActionTitle, null, value);
        }

        public static CardAction CreateOpenUrlAction(string url, string title = BotConstants.OpenUrlActionTitle)
        {
            return new CardAction(ActionTypes.OpenUrl, title, null, url);
        }

        public static CardAction CreateRegionAction(string regionName)
        {
            string value = string.Concat(BotConstants.RegionActionCode, " set ", regionName);

            return new CardAction(ActionTypes.PostBack, regionName, null, value);
        }

        public static CardAction CreateRegionAnyAction()
        {
            return new CardAction(ActionTypes.PostBack, "Любой", null, BotConstants.RegionActionAnyValue);
        }

        public static CardAction CreateSettingsAction()
        {
            return new CardAction(ActionTypes.PostBack, BotConstants.SystemSettingsActionTitle, null, BotConstants.SystemSettingsActionCode);
        }

        public static CardAction CreateSearchResultsAction(int currentCount)
        {
            string title = string.Concat(BotConstants.ResultsCountActionTitle, ": ", currentCount.ToString());

            return new CardAction(ActionTypes.PostBack, title, null, BotConstants.ResultsCountActionCode);
        }
        #endregion
    }
}