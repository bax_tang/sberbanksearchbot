﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class ActionStatesExecutor
    {
        #region Constants and fields

        private readonly BotActionsRepository ActionsRepository;

        private readonly ConcurrentDictionary<string, CurrentActionState> ActionStates;

        private readonly ConcurrentDictionary<CurrentActionState, Func<KeyedActivity, Task<ResourceResponse>>> StateHandlers;
        #endregion

        #region Constructors

        private ActionStatesExecutor()
        {
            ActionStates = new ConcurrentDictionary<string, CurrentActionState>(StringComparer.OrdinalIgnoreCase);
        }

        public ActionStatesExecutor(BotActionsRepository actionsRepository) : this()
        {
            if (actionsRepository == null)
            {
                throw new ArgumentNullException(nameof(actionsRepository), "The repository of bot actions can't be null.");
            }

            ActionsRepository = actionsRepository;

            StateHandlers = CreateStateHandlers();
        }
        #endregion

        #region Public class methods

        public CurrentActionState GetActionState(string activityKey)
        {
            return ActionStates.GetOrAdd(activityKey, CurrentActionState.Unknown);
        }

        public void SetActionState(string activityKey, CurrentActionState actionState)
        {
            ActionStates[activityKey] = actionState;
        }

        public Task<ResourceResponse> ProcessActionStateAsync(KeyedActivity userActivity, CurrentActionState actionState)
        {
            Task<ResourceResponse> responseTask = null;

            Func<KeyedActivity, Task<ResourceResponse>> stateHandler = null;
            bool exists = StateHandlers.TryGetValue(actionState, out stateHandler);
            if (exists)
            {
                responseTask = stateHandler.Invoke(userActivity);
            }
            else
            {
                responseTask = ProcessUnknownState(userActivity);
            }

            return responseTask;
        }
        #endregion

        #region Private class methods

        private ConcurrentDictionary<CurrentActionState, Func<KeyedActivity, Task<ResourceResponse>>> CreateStateHandlers()
        {
            var stateHandlers = new ConcurrentDictionary<CurrentActionState, Func<KeyedActivity, Task<ResourceResponse>>>();

            stateHandlers[CurrentActionState.Unknown] = ProcessUnknownState;
            stateHandlers[CurrentActionState.WaitForKeywords] = ProcessWaitForKeywordsState;
            stateHandlers[CurrentActionState.WaitForMinPrice] = ProcessWaitForMinPriceState;
            stateHandlers[CurrentActionState.WaitForMaxPrice] = ProcessWaitForMaxPriceState;
            stateHandlers[CurrentActionState.WaitForSearchResultsCount] = ProcessWaitForSearchResultsCountState;

            return stateHandlers;
        }

        private Task<ResourceResponse> ProcessUnknownState(KeyedActivity userActivity)
        {
            return Task.FromResult(new ResourceResponse(userActivity.Text));
        }

        private Task<ResourceResponse> ProcessWaitForKeywordsState(KeyedActivity userActivity)
        {
            string activityKey = userActivity.GetActivityKey();
            SearchConfiguration configuration = ActionsRepository.Configuration.GetConfiguration(activityKey);
            
            string[] keywords = userActivity.Text.Split(" ,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            configuration.KeywordsItem.Keywords = keywords;
            ActionStates[activityKey] = CurrentActionState.Unknown;

            userActivity.Text = string.Concat(BotConstants.SectionActionCode, " ", configuration.SectionName);

            BotAction sectionAction = ActionsRepository.GetAction(BotConstants.SectionActionCode);
            return sectionAction.ProcessActivityAsync(userActivity);
        }

        private Task<ResourceResponse> ProcessWaitForMinPriceState(KeyedActivity userActivity)
        {
            return ProcessPriceState(userActivity, PriceType.MinPrice);
        }

        private Task<ResourceResponse> ProcessWaitForMaxPriceState(KeyedActivity userActivity)
        {
            return ProcessPriceState(userActivity, PriceType.MaxPrice);
        }

        private Task<ResourceResponse> ProcessPriceState(KeyedActivity userActivity, PriceType priceType)
        {
            Task<ResourceResponse> responseTask = null;

            Regex decimalRegex = RegularExpressions.GetDecimalRegex();
            Match priceMatch = decimalRegex.Match(userActivity.Text);
            if (priceMatch.Success)
            {
                double priceValue = -1.0; string priceHeader = string.Empty;
                if (PriceParser.TryParseToValue(userActivity.Text, priceMatch.Value, out priceValue, out priceHeader))
                {
                    string activityKey = userActivity.GetActivityKey();
                    SearchConfiguration configuration = ActionsRepository.Configuration.GetConfiguration(activityKey);

                    configuration.PricesItem.ChangePrice(priceValue, priceHeader, priceType);

                    ActionStates[activityKey] = CurrentActionState.Unknown;

                    BotAction priceAction = ActionsRepository.GetAction(BotConstants.PriceActionCode);
                    responseTask = priceAction.ProcessActivityAsync(userActivity);
                }
            }
            
            if (responseTask == null)
            {
                Activity replyActivity = userActivity.CreateReply(BotConstants.PriceIncorrectOwnValue, "ru");
                ConnectorClient connector = ActionsRepository.Connectors.GetConnector(userActivity);
                responseTask = connector.Conversations.ReplyToActivityAsync(replyActivity);
            }

            return responseTask;
        }

        private Task<ResourceResponse> ProcessWaitForSearchResultsCountState(KeyedActivity userActivity)
        {
            Task<ResourceResponse> responseTask = null;

            string activityKey = userActivity.GetActivityKey();
            SystemConfiguration configuration = ActionsRepository.SystemConfiguration.GetConfiguration(activityKey);

            Regex decimalRegex = RegularExpressions.GetDecimalRegex();
            Match resultsCountMatch = decimalRegex.Match(userActivity.Text);
            if (resultsCountMatch.Success)
            {
                int resultsCount = -1;
                if (int.TryParse(resultsCountMatch.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out resultsCount) &&
                    (resultsCount > 0))
                {
                    configuration.SearchResultsCount = resultsCount;
                    ActionStates[activityKey] = CurrentActionState.Unknown;

                    BotAction settingsAction = ActionsRepository.GetAction(BotConstants.SystemSettingsActionCode);
                    responseTask = settingsAction.ProcessActivityAsync(userActivity);
                }
            }

            if (responseTask == null)
            {
                Activity replyActivity = userActivity.CreateReply(BotConstants.ResultsCountChangingFailed, "ru");
                ConnectorClient connector = ActionsRepository.Connectors.GetConnector(userActivity);
                responseTask = connector.Conversations.ReplyToActivityAsync(replyActivity);
            }

            return responseTask;
        }
        #endregion
    }
}