﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;

using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public static class PriceParser
    {
        #region Constants and fields

        private const double OneMillion = 1000000.0;

        private const double OneThousand = 1000.0;

        private const string MillionsHeader = " млн. руб.";

        private const string ThousandsHeader = " тыс. руб.";

        private const string UnitsHeader = " руб.";

        private static readonly char[] MillionSymbols; // м, и, л, о, н

        private static readonly char[] ThousandSymbols; // т, ы, с, я, ч
        #endregion

        #region Constructors

        static PriceParser()
        {
            MillionSymbols = new char[] { 'м', 'и', 'л', 'о', 'н' };
            ThousandSymbols = new char[] { 'т', 'ы', 'с', 'я', 'ч' };
        }
        #endregion

        #region Public class methods

        public static bool TryParseToValue(string activityText, string priceText, out double resultPrice, out string resultHeader)
        {
            if (string.IsNullOrEmpty(activityText))
            {
                throw new ArgumentNullException(nameof(activityText), "Activity text can't be null or empty.");
            }
            if (string.IsNullOrEmpty(priceText))
            {
                throw new ArgumentNullException(nameof(priceText), "Price text can't be null or empty.");
            }

            double price = 0.0D;

            if (double.TryParse(priceText, NumberStyles.Any, CultureInfo.InvariantCulture, out price) ||
                double.TryParse(priceText, NumberStyles.Any, CultureInfo.CurrentCulture, out price))
            {
                int millionIndex = activityText.IndexOfAny(MillionSymbols);
                int thousandIndex = activityText.IndexOfAny(ThousandSymbols);

                string header = null;

                if ((millionIndex == -1) && (thousandIndex == -1))
                {
                    if (price >= OneMillion)
                    {
                        header = GetHeader((price / OneMillion), MillionsHeader);
                    }
                    else if (price >= OneThousand)
                    {
                        header = GetHeader((price / OneThousand), ThousandsHeader);
                    }
                    else
                    {
                        header = GetHeader(price, UnitsHeader);
                    }
                }
                else
                {
                    if (millionIndex != -1)
                    {
                        header = GetHeader(price, MillionsHeader);
                        price *= OneMillion;
                    }
                    else if (thousandIndex != -1)
                    {
                        header = GetHeader(price, ThousandsHeader);
                        price *= OneThousand;
                    }
                    else
                    {
                        header = GetHeader(price, UnitsHeader);
                    }
                }

                resultHeader = header;
                resultPrice = price;
            }
            else
            {
                resultHeader = string.Empty;
                resultPrice = -1.0D;
            }

            return (price >= 0.0D);
        }

        public static bool TryParseToConfig(string activityText, string priceText, out JObject priceConfig)
        {
            if (string.IsNullOrEmpty(activityText))
            {
                throw new ArgumentNullException(nameof(activityText), "Activity text can't be null or empty.");
            }
            if (string.IsNullOrEmpty(priceText))
            {
                throw new ArgumentNullException(nameof(priceText), "Price text can't be null or empty.");
            }

            JObject parsingResult = null;

            double price = double.Parse(priceText, NumberStyles.Any, CultureInfo.InvariantCulture);
            if (price >= 0.0)
            {
                parsingResult = new JObject();

                int millionIndex = activityText.IndexOfAny(MillionSymbols);
                int thousandIndex = activityText.IndexOfAny(ThousandSymbols);

                string header = null;

                if ((millionIndex == -1) && (thousandIndex == -1))
                {
                    if (price >= OneMillion)
                    {
                        header = GetHeader((price / OneMillion), MillionsHeader);
                    }
                    else if (price >= OneThousand)
                    {
                        header = GetHeader((price / OneThousand), ThousandsHeader);
                    }
                    else
                    {
                        header = GetHeader(price, UnitsHeader);
                    }
                }
                else
                {
                    if (millionIndex != -1)
                    {
                        header = GetHeader(price, MillionsHeader);
                        price *= OneMillion;
                    }
                    else if (thousandIndex != -1)
                    {
                        header = GetHeader(price, ThousandsHeader);
                        price *= OneThousand;
                    }
                    else
                    {
                        header = GetHeader(price, UnitsHeader);
                    }
                }

                parsingResult["header"] = header;
                parsingResult["value"] = price;
            }

            priceConfig = parsingResult;

            return (price >= 0.0);
        }
        #endregion

        #region Private class methods

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetHeader(double value, string header)
        {
            return string.Concat(value.ToString("F0", CultureInfo.InvariantCulture), header);
        }
        #endregion
    }
}