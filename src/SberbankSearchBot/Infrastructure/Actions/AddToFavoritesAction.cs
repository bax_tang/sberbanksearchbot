﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class AddToFavoritesAction : BotAction
    {
        #region Constructors

        public AddToFavoritesAction() : base(BotConstants.AddToFavoritesActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            Activity replyActivity = ProcessUserActivity(userActivity);

            ConnectorClient connector = GetConnector(userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyActivity);
        }
        #endregion

        #region Private class methods

        private Activity ProcessUserActivity(KeyedActivity userActivity)
        {
            Activity replyActivity = userActivity.CreateReply(null, "ru");

            string activityKey = userActivity.GetActivityKey();
            string activityText = userActivity.Text;
            int spaceIndex = activityText.IndexOf(' ');
            string objectID = activityText.Substring(1 + spaceIndex);

            SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);

            CardAction[] cardButtons = new CardAction[3];

            cardButtons[0] = ActionSource.CreateBackAction("section " + configuration.SectionName);
            cardButtons[1] = ActionSource.CreateManageFavoritesAction(BotConstants.ManageFavoritesActionTitleAfterAdding);
            cardButtons[2] = ActionSource.CreateStartAction();

            HeroCard replyCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0]
            };

            bool added = FavoritesRepository.AddToFavorites(activityKey, objectID);
            if (added)
            {
                replyCard.Text = BotConstants.AddToFavoritesSucceeded;
            }
            else
            {
                replyCard.Text = BotConstants.AddToFavoritesFailed;
            }

            replyActivity.Attachments.Add(replyCard.ToAttachment());

            return replyActivity;
        }
        #endregion
    }
}