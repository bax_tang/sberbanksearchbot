﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class AddSubscriptionAction : BotAction
    {
        #region Constructors

        public AddSubscriptionAction() : base(BotConstants.AddSubscriptionActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            ConnectorClient connector = GetConnector(userActivity);

            Activity replyActivity = ProcessUserActivity(connector, userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyActivity);
        }
        #endregion

        #region Private class methods

        private Activity ProcessUserActivity(ConnectorClient connector, KeyedActivity userActivity)
        {
            Activity replyActivity = userActivity.CreateReply(null, "ru");

            string activityKey = userActivity.GetActivityKey();

            CardAction[] cardButtons = new CardAction[1];
            cardButtons[0] = ActionSource.CreateBackAction("start -h");

            HeroCard subscriptionCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0]
            };

            SearchConfiguration configuration = null;
            bool isRemoved = ConfigurationRepository.TryRemoveConfiguration(activityKey, out configuration);
            if (isRemoved)
            {
                SearchSubscription subscription = new SearchSubscription(connector, userActivity)
                {
                    ActivityKey = activityKey,
                    Configuration = configuration
                };

                SubscriptionsRepository.AddSubscription(activityKey, subscription);

                subscriptionCard.Text = Messages.GetMessage("AddSubscriptionSucceeded"); //BotConstants.AddSubscriptionSucceeded;
            }
            else
            {
                subscriptionCard.Text = Messages.GetMessage("AddSubscriptionFailed"); //BotConstants.AddSubscriptionFailed;
            }

            replyActivity.Attachments.Add(subscriptionCard.ToAttachment());

            return replyActivity;
        }
        #endregion
    }
}