﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class ManageSubscriptionsAction : BotAction
    {
        #region Constructors

        public ManageSubscriptionsAction() : base(BotConstants.ManageSubscriptionsActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            Activity replyActivity = ProcessUserActivity(userActivity);

            ConnectorClient connector = GetConnector(userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyActivity);
        }
        #endregion

        #region Private class methods

        private Activity ProcessUserActivity(KeyedActivity userActivity)
        {
            Activity replyActivity = userActivity.CreateReply(null, "ru");

            string activityKey = userActivity.GetActivityKey();
            var subscriptions = SubscriptionsRepository.GetSubscriptions(activityKey);

            if (subscriptions.Count > 0)
            {
                replyActivity.Text = BotConstants.CreatedSubscriptionsMessageText;

                for (int index = 0; index < subscriptions.Count; ++index)
                {
                    HeroCard subscriptionCard = HeroCardSource.CreateSubscriptionCard(index, subscriptions[index]);

                    replyActivity.Attachments.Add(subscriptionCard.ToAttachment());
                }
            }
            else
            {
                CardAction[] cardButtons = new CardAction[2];

                cardButtons[0] = ActionSource.CreateSearchAction();
                cardButtons[1] = ActionSource.CreateBackAction("start -h");

                HeroCard emptyCard = new HeroCard
                {
                    Buttons = cardButtons,
                    Images = new CardImage[0],
                    Text = BotConstants.NoSubscriptionsMessageText
                };

                replyActivity.Attachments.Add(emptyCard.ToAttachment());
            }

            return replyActivity;
        }
        #endregion
    }
}