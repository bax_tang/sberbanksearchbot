﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class ViewSubscriptionsAction : BotAction
    {
        #region Constructors

        public ViewSubscriptionsAction() : base(BotConstants.ViewSubscriptionsActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            Activity replyActivity = userActivity.CreateReply(null, "ru");

            string activityKey = userActivity.GetActivityKey();

            var subscriptions = SubscriptionsRepository.GetSubscriptions(activityKey);

            List<CardAction> cardButtons = new List<CardAction>(4);

            CardAction goBackAction = ActionSource.CreateBackAction("start -h");

            cardButtons.Add(goBackAction);

            if (subscriptions.Count > 0)
            {

            }
            else
            {
                replyActivity.Text = BotConstants.NoSubscriptionsMessageText;
            }

            HeroCard replyCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new List<CardImage>()
            };

            replyActivity.Attachments.Add(replyCard.ToAttachment());

            ConnectorClient connector = GetConnector(userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyActivity);
        }
        #endregion
    }
}