﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class KeywordsAction : BotAction
    {
        #region Constructors

        public KeywordsAction() : base(BotConstants.KeywordsActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            string activityKey = userActivity.GetActivityKey();

            Executor.SetActionState(activityKey, CurrentActionState.WaitForKeywords);

            Activity replyActivity = ProcessUserActivity(activityKey, userActivity);

            ConnectorClient connector = GetConnector(userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyActivity);
        }
        #endregion

        #region Private class methods

        private Activity ProcessUserActivity(string activityKey, KeyedActivity userActivity)
        {
            Activity replyActivity = userActivity.CreateReply(null, "ru");
            
            SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);
            
            CardAction[] cardButtons = new CardAction[1];
            cardButtons[0] = ActionSource.CreateBackAction(string.Concat("section ", configuration.SectionName));

            HeroCard replyCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0],
                Text = BotConstants.KeywordsActionText
            };

            replyActivity.Attachments.Add(replyCard.ToAttachment());

            return replyActivity;
        }
        #endregion
    }
}