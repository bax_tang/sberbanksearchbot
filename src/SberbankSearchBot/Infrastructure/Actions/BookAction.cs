﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class BookAction : BotAction
    {
        #region Constructors

        public BookAction() : base(BotConstants.BookActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            Activity replyMessage = userActivity.CreateReply(BotConstants.BookActionText, "ru");

            ConnectorClient connector = GetConnector(userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyMessage);
        }
        #endregion
    }
}