﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class ClearSubscriptionsAction : BotAction
    {
        #region Constructors

        public ClearSubscriptionsAction() : base(BotConstants.ClearSubscriptionsActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            Activity replyActivity = userActivity.CreateReply(null, "ru");

            string activityKey = userActivity.GetActivityKey();

            SubscriptionsRepository.ClearSubscriptions(activityKey);

            CardAction[] cardButtons = new CardAction[1];

            cardButtons[0] = ActionSource.CreateBackAction("start -h");

            HeroCard replyCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0],
                Text = BotConstants.ClearSubscriptionsCardText
            };

            replyActivity.Attachments.Add(replyCard.ToAttachment());

            ConnectorClient connector = GetConnector(userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyActivity);
        }
        #endregion
    }
}