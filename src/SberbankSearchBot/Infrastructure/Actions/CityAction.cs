﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class CityAction : BotAction
    {
        #region Constructors

        public CityAction() : base(BotConstants.CityActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            Activity replyActivity = ProcessUserActivity(userActivity);

            ConnectorClient connector = GetConnector(userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyActivity);
        }
        #endregion

        #region Private class methods

        private Activity ProcessUserActivity(KeyedActivity userActivity)
        {
            Activity replyActivity = userActivity.CreateReply(null, "ru");

            string activityKey = userActivity.GetActivityKey();
            string activityText = userActivity.Text;
            int setIndex = activityText.IndexOf("set");
            if (setIndex != -1)
            {
                ProcessCitySetting(activityKey, activityText, setIndex);
            }

            HeroCard replyCard = CreateCitiesCard(activityKey);

            replyActivity.Attachments.Add(replyCard.ToAttachment());

            return replyActivity;
        }

        private HeroCard CreateCitiesCard(string activityKey)
        {
            SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);

            IList<string> cities = DataRepository.GetCities(configuration.SectionName, configuration.RegionItem.RegionName);

            HashSet<string> citiesSet = configuration.CitiesItem.CitiesSet;

            List<CardAction> cardButtons = new List<CardAction>(2 + cities.Count);

            cardButtons.Add(ActionSource.CreateBackAction(string.Concat(BotConstants.SectionActionCode, " ", configuration.SectionName)));

            for (int index = 0; index < cities.Count; ++index)
            {
                string cityName = cities[index];
                bool isEnabled = citiesSet.Contains(cityName);

                cardButtons.Add(ActionSource.CreateCityAction(cityName, isEnabled));
            }

            cardButtons.Add(ActionSource.CreateDoSearchAction());

            HeroCard citiesCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0],
                Title = string.Format(BotConstants.CityActionTitleFormat, configuration.RegionItem.RegionName),
                Text = BotConstants.CityActionText
            };

            return citiesCard;
        }

        private void ProcessCitySetting(string activityKey, string activityText, int setIndex)
        {
            SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);

            string cityName = activityText.Substring(4 + setIndex);

            HashSet<string> citiesSet = configuration.CitiesItem.CitiesSet;

            if (citiesSet.Contains(cityName))
            {
                citiesSet.Remove(cityName);
            }
            else
            {
                citiesSet.Add(cityName);
            }
        }
        #endregion
    }
}