﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class RemoveSubscriptionAction : BotAction
    {
        #region Constructors

        public RemoveSubscriptionAction() : base(BotConstants.RemoveSubscriptionActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            Activity replyActivity = userActivity.CreateReply(null, "ru");

            CardAction[] cardButtons = new CardAction[1];
            cardButtons[0] = ActionSource.CreateBackAction("start -h");

            HeroCard replyCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0]
            };
            
            string activityKey = userActivity.GetActivityKey();

            int subscriptionIndex = -1;
            Regex decimalRegex = RegularExpressions.GetDecimalRegex();
            Match indexMatch = decimalRegex.Match(userActivity.Text);

            if (indexMatch.Success && int.TryParse(indexMatch.Value, out subscriptionIndex) && (subscriptionIndex != -1))
            {
                SubscriptionsRepository.RemoveSubscription(activityKey, subscriptionIndex);

                replyCard.Text = BotConstants.RemoveSubscriptionSucceeded;
            }
            else
            {
                replyCard.Text = BotConstants.RemoveSubscriptionFailed;
            }

            replyActivity.Attachments.Add(replyCard.ToAttachment());

            ConnectorClient connector = GetConnector(userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyActivity);
        }
        #endregion
    }
}