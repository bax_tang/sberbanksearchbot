﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SberbankSearchBot
{
    public class YearAction : BotAction
    {
        #region Constructors

        public YearAction() : base(BotConstants.YearActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            Activity replyActivity = ProcessUserActivity(userActivity);

            ConnectorClient connector = GetConnector(userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyActivity);
        }
        #endregion

        #region Private class methods

        private Activity ProcessUserActivity(KeyedActivity userActivity)
        {
            Activity replyActivity = userActivity.CreateReply(null, "ru");

            string activityKey = userActivity.GetActivityKey();
            string activityText = userActivity.Text;
            int setIndex = activityText.IndexOf("set");
            if (setIndex != -1)
            {
                ProcessYearSetting(activityKey, activityText, setIndex);
            }

            HeroCard yearsCard = CreateYearsCard(activityKey);

            replyActivity.Attachments.Add(yearsCard.ToAttachment());

            return replyActivity;
        }

        private HeroCard CreateYearsCard(string activityKey)
        {
            SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);
            string sectionName = configuration.SectionName;

            HashSet<int> yearsSet = configuration.IssueYearsItem.YearsSet;
            IList<int> yearsArray = DataRepository.GetTransportIssueYears();

            List<CardAction> cardButtons = new List<CardAction>(10);

            cardButtons.Add(ActionSource.CreateBackAction(string.Concat("section", " ", sectionName)));

            for (int index = 0; index < yearsArray.Count; ++index)
            {
                int year = yearsArray[index];
                bool isEnabled = yearsSet.Contains(year);

                cardButtons.Add(ActionSource.CreateYearAction(year, isEnabled));
            }

            HeroCard yearsCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0],
                Title = BotConstants.YearCardTitle,
                Text = BotConstants.YearCardText
            };

            return yearsCard;
        }

        private void ProcessYearSetting(string activityKey, string activityText, int setIndex)
        {
            SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);
            
            string yearText = activityText.Substring(4 + setIndex);

            int yearValue = -1;
            if (int.TryParse(yearText, NumberStyles.Any, CultureInfo.InvariantCulture, out yearValue))
            {
                HashSet<int> yearsSet = configuration.IssueYearsItem.YearsSet;

                if (yearsSet.Contains(yearValue))
                {
                    yearsSet.Remove(yearValue);
                }
                else
                {
                    yearsSet.Add(yearValue);
                }
            }
        }
        #endregion
    }
}