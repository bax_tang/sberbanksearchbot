﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class RemoveFromFavoritesAction : BotAction
    {
        #region Constructors

        public RemoveFromFavoritesAction() : base(BotConstants.RemoveFromFavoritesActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            Activity replyActivity = ProcessUserActivity(userActivity);

            ConnectorClient connector = GetConnector(userActivity);

            return connector.Conversations.ReplyToActivityAsync(replyActivity);
        }
        #endregion

        #region Private class methods

        private Activity ProcessUserActivity(KeyedActivity userActivity)
        {
            Activity replyActivity = userActivity.CreateReply(null, "ru");

            string activityKey = userActivity.GetActivityKey();
            string activityText = userActivity.Text;
            int spaceIndex = activityText.IndexOf(' ');
            string objectID = activityText.Substring(1 + spaceIndex);

            CardAction[] cardButtons = new CardAction[2];

            cardButtons[0] = ActionSource.CreateBackAction(BotConstants.ManageFavoritesActionCode);
            cardButtons[1] = ActionSource.CreateStartAction();

            HeroCard replyCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0]
            };

            bool removed = FavoritesRepository.RemoveFromFavorites(activityKey, objectID);
            if (removed)
            {
                replyCard.Text = BotConstants.RemoveFromFavoritesSucceeded;
            }
            else
            {
                replyCard.Text = BotConstants.RemoveFromFavoritesFailed;
            }

            replyActivity.Attachments.Add(replyCard.ToAttachment());

            return replyActivity;
        }
        #endregion
    }
}