﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public class RegionAction : BotAction
    {
        #region Constructors

        public RegionAction() : base(BotConstants.RegionActionCode) { }
        #endregion

        #region BotAction methods overriding

        public override Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity)
        {
            Task<ResourceResponse> responseTask = null;

            Activity replyActivity = null;
            bool result = ProcessUserActivity(userActivity, out replyActivity);
            if (result)
            {
                ConnectorClient connector = GetConnector(userActivity);

                responseTask = connector.Conversations.ReplyToActivityAsync(replyActivity);
            }
            else
            {
                SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(userActivity.GetActivityKey());

                BotAction sectionAction = ParentRepository.GetAction("section");
                userActivity.Text = string.Concat("section ", configuration.SectionName);

                responseTask = sectionAction.ProcessActivityAsync(userActivity);
            }

            return responseTask;
        }
        #endregion

        #region Private class methods

        private bool ProcessUserActivity(KeyedActivity userActivity, out Activity replyActivity)
        {
            replyActivity = null;

            string activityKey = userActivity.GetActivityKey();
            string activityText = userActivity.Text;

            int setIndex = activityText.IndexOf("set");
            int anyIndex = activityText.IndexOf("any");

            bool result = (setIndex == -1);
            if (result)
            {
                HeroCard replyCard = CreateRegionsCard(activityKey);

                Activity message = userActivity.CreateReply(null, "ru");
                message.Attachments.Add(replyCard.ToAttachment());

                replyActivity = message;
            }
            else
            {
                ProcessRegionSetting(activityKey, activityText, setIndex, anyIndex);
            }

            return result;
        }

        private void ProcessRegionSetting(string activityKey, string activityText, int setIndex, int anyIndex)
        {
            SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);

            if (anyIndex != -1)
            {
                configuration.RegionItem.RegionName = string.Empty;
                configuration.CitiesItem.Reset();
            }
            else
            {
                string regionName = activityText.Substring(4 + setIndex);
                configuration.RegionItem.RegionName = regionName;
                configuration.CitiesItem.Clear(true);
            }
        }

        private HeroCard CreateRegionsCard(string activityKey)
        {
            SearchConfiguration configuration = ConfigurationRepository.GetConfiguration(activityKey);

            List<CardAction> cardButtons = new List<CardAction>(10);

            cardButtons.Add(ActionSource.CreateBackAction(string.Concat("section ", configuration.SectionName)));
            cardButtons.Add(ActionSource.CreateRegionAnyAction());

            IList<string> regions = ParentRepository.ObjectsData.GetRegions(configuration.SectionName);
            for (int index = 0; index < regions.Count; ++index)
            {
                cardButtons.Add(ActionSource.CreateRegionAction(regions[index]));
            }

            cardButtons.Add(ActionSource.CreateDoSearchAction());

            HeroCard regionsCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0],
                Title = string.Format(BotConstants.RegionCardTitleFormat, configuration.SectionName),
                Text = BotConstants.RegionCardText
            };

            return regionsCard;
        }
        #endregion
    }
}