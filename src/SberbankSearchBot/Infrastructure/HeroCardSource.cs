﻿using System;
using System.Collections.Generic;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public static class HeroCardSource
    {
        #region Public class methods

        public static HeroCard CreateObjectCard(ObjectData objectData)
        {
            CardAction[] objectCardButtons = new CardAction[3];

            objectCardButtons[0] = ActionSource.CreateBookAction(objectData.ID);
            objectCardButtons[1] = ActionSource.CreateAddToFavoritesAction(objectData.ID);
            objectCardButtons[2] = ActionSource.CreateOpenUrlAction(objectData.URL);

            CardImage[] cardImages = null;
            string imageUrl = objectData.GetFirstImageUrl(); // objectData.GetRandomImageUrl();
            if (imageUrl != null)
            {
                CardImage image = new CardImage(imageUrl);
                cardImages = new[] { image };
            }
            else cardImages = new CardImage[0];

            HeroCard objectCard = new HeroCard
            {
                Buttons = objectCardButtons,
                Images = cardImages,
                Title = objectData.Name,
                Subtitle = objectData.GetPriceText(),
                Text = objectData.Description
            };

            return objectCard;
        }

        public static HeroCard CreateObjectCardForFavorites(ObjectData objectData)
        {
            CardAction[] objectCardButtons = new CardAction[3];

            objectCardButtons[0] = ActionSource.CreateBookAction(objectData.ID);
            objectCardButtons[1] = ActionSource.CreateRemoveFromFavoritesAction(objectData.ID);
            objectCardButtons[2] = ActionSource.CreateOpenUrlAction(objectData.URL);

            CardImage[] cardImages = null;
            string imageUrl = objectData.GetFirstImageUrl(); //objectData.GetRandomImageUrl();
            if (imageUrl != null)
            {
                CardImage image = new CardImage(imageUrl);
                cardImages = new[] { image };
            }
            else cardImages = new CardImage[0];

            HeroCard objectCard = new HeroCard
            {
                Buttons = objectCardButtons,
                Images = cardImages,
                Title = objectData.Name,
                Subtitle = objectData.GetPriceText(),
                Text = objectData.Description
            };

            return objectCard;
        }
        
        public static HeroCard CreateSubscriptionCard(int subscriptionIndex, SearchSubscription subscription)
        {
            CardAction[] cardButtons = new CardAction[2];

            cardButtons[0] = ActionSource.CreateBackAction("start -h");
            cardButtons[1] = ActionSource.CreateRemoveSubscriptionAction(subscriptionIndex);

            HeroCard subscriptionCard = new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0],
                Title = subscription.GetTitle(),
                Text = subscription.GetText()
            };

            return subscriptionCard;
        }

        public static HeroCard CreateSubscriptionCard(string sectionName, bool nextAvailable = false)
        {
            List<CardAction> subscriptionButtons = new List<CardAction>(4);

            subscriptionButtons.Add(ActionSource.CreateBackAction("section " + sectionName));
            subscriptionButtons.Add(ActionSource.CreateSearchAction("Новый поиск"));

            if (nextAvailable)
            {
                subscriptionButtons.Add(ActionSource.CreateRetrieveNextAction());
            }

            subscriptionButtons.Add(ActionSource.CreateAddSubscriptionAction());

            HeroCard subscriptionCard = new HeroCard
            {
                Buttons = subscriptionButtons,
                Images = new CardImage[0],
                Title = BotConstants.SubscriptionCardTitle
            };

            return subscriptionCard;
        }

        public static HeroCard CreateWarningCard()
        {
            CardAction[] cardButtons = new CardAction[1];

            cardButtons[0] = ActionSource.CreateSearchAction();

            return new HeroCard
            {
                Buttons = cardButtons,
                Images = new CardImage[0],
                Title = "OOPS",
                Text = "Что-то явно пошло не так"
            };
        }
        #endregion
    }
}