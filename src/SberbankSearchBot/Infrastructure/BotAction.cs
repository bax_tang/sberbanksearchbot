﻿using System;
using System.Threading.Tasks;

using Microsoft.Bot.Connector;

namespace SberbankSearchBot
{
    public abstract class BotAction
    {
        #region Constants and fields

        public readonly string ActionCode;

        protected BotActionsRepository ParentRepository = null;

        protected SearchConfigurationRepository ConfigurationRepository = null;

        protected ObjectsDataRepository DataRepository = null;

        protected ActionStatesExecutor Executor = null;

        protected UserFavoritesRepository FavoritesRepository = null;

        protected SearchSubscriptionsRepository SubscriptionsRepository = null;

        protected SystemConfigurationRepository SystemRepository = null;

        protected MessagesRepository Messages = null;
        #endregion

        #region Constructors

        protected internal BotAction(string actionCode)
        {
            if (string.IsNullOrEmpty(actionCode))
            {
                throw new ArgumentNullException(nameof(actionCode), "Action code can't be null or empty.");
            }

            ActionCode = actionCode;
        }
        #endregion

        #region Public class methods

        public void SetParentRepository(BotActionsRepository parentRepository)
        {
            if (parentRepository == null)
            {
                throw new ArgumentNullException(nameof(parentRepository), "Parent repository can't be null.");
            }

            if (ParentRepository != null)
            {
                throw new InvalidOperationException("Action already has a parent repository.");
            }

            ParentRepository = parentRepository;
            ConfigurationRepository = parentRepository.Configuration;
            DataRepository = parentRepository.ObjectsData;
            Executor = parentRepository.StatesExecutor;
            FavoritesRepository = parentRepository.Favorites;
            SubscriptionsRepository = parentRepository.Subscriptions;
            SystemRepository = parentRepository.SystemConfiguration;
            Messages = parentRepository.Messages;
        }

        public abstract Task<ResourceResponse> ProcessActivityAsync(KeyedActivity userActivity);
        #endregion

        #region Protected class methods

        protected ConnectorClient GetConnector(KeyedActivity userActivity)
        {
            BotActionsRepository parentRepository = ParentRepository;

            if (parentRepository == null)
            {
                throw new InvalidOperationException("Actions repository can't be null.");
            }

            return parentRepository.Connectors.GetConnector(userActivity);
        }
        #endregion
    }
}