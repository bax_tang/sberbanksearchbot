﻿using System;

using Autofac;
using Autofac.Core;

namespace SberbankSearchBot.Modules
{
    public sealed class MainModule : Module
    {
        #region Constructors

        public MainModule() : base() { }
        #endregion

        #region Module methods overriding

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterModule(new InfrastructureModule());

            builder.RegisterType<UpdatingService>().SingleInstance().OnActivated(HandleServiceActivation);
        }
        #endregion

        #region Private class methods

        private static void HandleServiceActivation(IActivatedEventArgs<UpdatingService> args)
        {

        }
        #endregion
    }
}