﻿using System;

using Autofac;
using NLog;
using Topshelf;
using Topshelf.Autofac;
using Topshelf.HostConfigurators;
using Topshelf.ServiceConfigurators;

namespace SberbankSearchBot
{
    using Modules;

    internal class EntryPoint
    {
        #region The entry point

        internal static int Main(string[] args)
        {
            int result = 0;

            try
            {
                TopshelfExitCode exitCode = HostFactory.Run(ConfigureHost);

                result = (int)exitCode;
            }
            catch (Exception exc)
            {
                result = exc.HResult;

                ILogger logger = LogManager.GetCurrentClassLogger();
                logger.Error(exc, exc.Message);
            }

            return result;
        }
        #endregion

        #region Private class methods

        private static IContainer CreateContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterModule(new MainModule());

            return builder.Build();
        }

        private static void ConfigureHost(HostConfigurator configurator)
        {
            IContainer container = CreateContainer();

            var configuration = container.Resolve<UpdatingServiceConfiguration>();

            configurator.UseAutofacContainer(container);
            configurator.UseNLog();
            configurator.SetServiceName(configuration.ServiceName);
            configurator.SetDisplayName(configuration.ServiceDisplayName);
            configurator.SetDescription(configuration.ServiceDescription);
            configurator.ApplyCommandLine();

            configurator.Service<UpdatingService>(ConfigureService);

            configurator.RunAsLocalSystem();
        }

        private static void ConfigureService(ServiceConfigurator<UpdatingService> configurator)
        {
            configurator.ConstructUsingAutofacContainer();

            configurator.WhenStarted(StartService);
            configurator.WhenStopped(StopService);
        }

        private static bool StartService(UpdatingService service, HostControl control)
        {
            return service.Start();
        }

        private static bool StopService(UpdatingService service, HostControl control)
        {
            return service.Stop();
        }
        #endregion
    }
}